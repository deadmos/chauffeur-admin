import { Component, OnInit } from '@angular/core';
import {MENUS} from '../networks-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {Router} from '@angular/router';
import {ActionsComponent} from '../../common/actions/actions.component';
import {NbThemeService} from '@nebular/theme';
import {AgDatatable} from '../../admin.extends';
@Component({
  selector: 'ngx-network-requests',
  templateUrl: './network-requests.component.html',
  styleUrls: ['./network-requests.component.scss']
})
export class NetworkRequestsComponent extends AgDatatable implements OnInit {
    public columnDefs;
    public defaultColDef;
    public rowData: {};
    public rowSelection;
    url = 'network-requests';
    menus = MENUS;
    breadcrumbs = [
      {
        title: 'Home',
        link: '/dashboard',
        icon: 'fa fa-home',
      },
      {
        title: 'Network Request List',
        icon: 'fa fa-user-secret',
      },
    ];
    headerData = {
      title: 'Network Request List',
      action: false,
    };
    constructor(
      protected http: HttpClient,
      protected admin: AdminService,
      protected router: Router,
      protected themeService: NbThemeService,
      ) {
      super();
      this.columnDefs = [
        {
          field: 'globalSearch',
          hide: true,
          initialHide: true,
          filter: 'agTextColumnFilter',
          filterParams: {
            newRowsAction: 'keep',
          },
        },
        { field: 'id', headerName: 'Ref', sortable: false, filter: false , maxWidth: 80,  minWidth: 60},
        {
          field: 'name',
          headerName: 'Name',
          sortable: true,
          filter: true,
          maxWidth: 250,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
          field: 'email',
          headerName: 'Email',
          sortable: true,
          filter: true,
          maxWidth: 250,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
          field: 'mobile',
          headerName: 'Phones',
          sortable: true,
          filter: true,
          maxWidth: 150,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
          field: 'address',
          headerName: 'Address',
          sortable: true,
          filter: true,
          maxWidth: 150,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
          field: 'company_name',
          headerName: 'Business Name',
          sortable: false,
          filter: false ,
          minWidth: 60,
          maxWidth: 200,
        },
        {
          field: 'company_address',
          headerName: 'Business Address',
          sortable: false,
          filter: false ,
          minWidth: 60,
          maxWidth: 200,
        },
        {
          field: 'tel',
          headerName: 'Business Phone No',
          sortable: false,
          filter: false ,
          minWidth: 60,
          maxWidth: 200,
        },
        {
          field: 'abn',
          headerName: 'ABN No.',
          sortable: false,
          filter: false ,
          minWidth: 60,
          maxWidth: 200,
        },
      ];
      this.defaultColDef = { resizable: true };
      this.rowSelection = 'multiple';
    }
  
    ngOnInit(): void {
  
    }

}
