import { Component, OnInit } from '@angular/core';
import {MENUS} from './networks-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {Router} from '@angular/router';
import {ActionsComponent} from '../common/actions/actions.component';
import {NbThemeService} from '@nebular/theme';
import {AgDatatable} from '../admin.extends';

@Component({
  selector: 'ngx-networks',
  templateUrl: './networks.component.html',
  styleUrls: ['./networks.component.scss'],
})
export class NetworksComponent extends AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  url = 'networks';
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Network List',
      icon: 'fa fa-user-secret',
    },
  ];
  headerData = {
    title: 'Network User List',
    action: true,
    actionLink: '/networks/create',
    actionTitle: 'Add New Network',
  };
  constructor(
    protected http: HttpClient,
    protected admin: AdminService,
    protected router: Router,
    protected themeService: NbThemeService,
    ) {
    super();
    this.columnDefs = [
      {
        field: 'globalSearch',
        hide: true,
        initialHide: true,
        filter: 'agTextColumnFilter',
        filterParams: {
          newRowsAction: 'keep',
        },
      },
      { field: 'id', headerName: 'Ref', sortable: false, filter: false , maxWidth: 80,  minWidth: 60},
      {
        field: 'user.name',
        headerName: 'Name',
        sortable: true,
        filter: true,
        minWidth: 100,
        filterParams: this.filterParams,
      },
      {
        field: 'user.email',
        headerName: 'Email',
        sortable: true,
        filter: true,
        minWidth: 100,
        filterParams: this.filterParams,
      },
      {
        field: 'user.mobile',
        headerName: 'Phones',
        sortable: true,
        filter: true,
        minWidth: 100,
        filterParams: this.filterParams,
      },
      {
        field: 'company_name',
        headerName: 'Company Name',
        sortable: false,
        filter: false ,
        minWidth: 60,
      },
      {
        field: 'website',
        headerName: 'Company Website',
        sortable: false,
        filter: false ,
        minWidth: 250,
      },
      { field: 'action', sortable: true, filter: false ,
        cellRendererFramework: ActionsComponent,
        displayType: 'dropdown',
        cellRendererParams: [
          {
            title: 'Edit Network',
            href: '/networks/edit',
            type: 'edit',
          },
          {
            title: 'Setting Network',
            href: '/networks/setting',
            type: 'settings-outline',
          },
          {
            title: 'Message Network',
            href: '/networks/message',
            type: 'email-outline',
          },
          {
            title: 'View Network',
            href: '/networks/detail',
            type: 'detail',
          },
          {
            title: 'Delete Network',
            href: 'networks/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete?',
              successMessage: 'Page deleted successfully',
            },
          },
        ],
      },
    ];
    this.defaultColDef = { resizable: true };
    this.rowSelection = 'multiple';
  }

  ngOnInit(): void {

  }

}
