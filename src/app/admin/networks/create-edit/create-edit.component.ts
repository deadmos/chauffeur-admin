import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-network-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss'],
})
export class CreateEditComponent implements OnInit {
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Network List',
      link: '/networks',
      icon: 'fa fa-user-secret',
    },
    {
      title: 'Create',
      icon: 'fa fa-plus',
    },
  ];
  headerData = {
    title: 'Network Add New',
  };
  network = {
    api_key: '',
    primary_commission: 5,
    api_allow_domain: '',
    api_url: '',
    name: '',
    email: '',
    mobile: '',
    company_name: '',
    website: '',
    logo: '',
  };
  submitting = false;
  generating = false;
  formStatus = false;

  constructor(private http: HttpClient,
              private admin: AdminService,
              private router: Router,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id') != null) {
      this.admin
        .get(`networks/` + this.route.snapshot.paramMap.get('id'))
        .subscribe((network: any) => {
          this.network = network.data.network;
          this.formStatus = true;
        });
    } else {
      this.network.logo = 'https://api.demo2project.com/images/logo.png';
      this.formStatus = true;
    }
  }

  notValid(name) {
    return this.admin.notValid(name);
  }

  valid(name) {
    return this.admin.valid(name);
  }

  getNumber(obj) {
    this.network.mobile = obj;
  }

  telInputObject(obj: any, phone) {
      obj.setNumber(phone);
  }
  getError(name) {
    if (name.control.getError('required') === true) {
      return 'Required';
    } else {
      return name.control.getError('message');
    }

  }

  submitForm(networkForm): void {
    this.submitting = true;
    const data = this.admin.formatFormData(networkForm);
    data.append('mobile', this.network.mobile);
    this.admin.showLoader();

    const curr = this.route.snapshot.paramMap.get('id');
    let url = '';
    if (curr != null) {
      data.append('_method', 'put');
      url = `networks/` + this.route.snapshot.paramMap.get('id');
    } else {
      url = `networks`;
    }
    this.admin
      .post(url, data, true)
      .subscribe((res: any) => {
          this.submitting = false;
          this.admin.hideLoader();
          this.admin.noty('Successfully Created Network Detail.');
          this.router.navigate(['networks']);
        },
        (error) => {
          this.submitting = false;
          this.admin.hideLoader();
          switch (error.status) {
            case 422:
              this.admin.noty('Validation Error', true);
              this.admin.showValidationError(networkForm, error.error);
              break;
            default :
              this.admin.noty('Server Error', true);
              break;
          }
        });
  }

  generateToken(): void {
    this.generating = true;
    this.admin
      .get(`networks/generate-api-token`)
      .subscribe((res: any) => {
        this.generating = false;
        this.network.api_key = res.data.key;
      });
  }

}
