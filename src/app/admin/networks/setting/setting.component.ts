import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'ngx-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent implements OnInit {
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Network List',
      link: '/networks',
      icon: 'fa fa-user-secret',
    },
    {
      title: 'Settings',
      icon: 'fa fa-cog',
    },
  ];
  headerData = {
    title: 'Network Settings',
  };
  network = {
    api_key: '',
    primary_commission: 5,
    api_allow_domain: '',
    api_url: '',
    name_of_the_bank:'',
    name_on_the_account:'',
    account_number:'',
    swift_code:'',
    bank_address:''
  };
  submitting = false;
  generating = false;
  bank_submitting = false;
  constructor(    private http: HttpClient,
                  private admin: AdminService,
                  private route: ActivatedRoute) {
  }

  ngOnInit(): void {

    this.http
      .get(
        this.admin.getFullUrl(`networks/` + this.route.snapshot.paramMap.get('id') ),
        {headers: this.admin.getHeader()},
      )
      .subscribe((network: any) => {
        this.network = network.data.network;
      });

  }
  notValid(name) {
    return this.admin.notValid(name);
  }

  valid(name) {
    return this.admin.valid(name);
  }

  getError(name) {
    if (name.control.getError('required') === true) {
      return 'Required';
    } else {
      return name.control.getError('message');
    }

  }
  submitForm(settingForm): void {
    this.submitting = true;
    this.admin
      .post(`networks/setting/` + this.route.snapshot.paramMap.get('id'), settingForm.value)
      .subscribe((res: any) => {
        this.submitting = false;
        this.admin.noty('Successfully Updated Network Settings.');
      },
        (error) => {
          this.submitting = false;
          this.admin.noty('Vlidation Error', true);
          switch (error.status) {
            case 422:
               this.admin.showValidationError(settingForm, error.error);
              break;
          }
        });
  }
  generateToken(): void {
    this.generating = true;
    this.admin
      .get(`networks/generate-api-token`)
      .subscribe((res: any) => {
        this.generating = false;
       this.network.api_key = res.data.key;
      });
  }
  submitBankForm(bankSettingForm): void {
    this.bank_submitting = true;
    this.admin
      .post(`networks/bank-setting/` + this.route.snapshot.paramMap.get('id'), bankSettingForm.value)
      .subscribe((res: any) => {
        this.bank_submitting = false;
        if(res.status){
            this.admin.noty('Successfully Updated Network Bank Info.');
        }else{
            this.admin.noty(res.message,true);
        }
        
    });
  }
}
