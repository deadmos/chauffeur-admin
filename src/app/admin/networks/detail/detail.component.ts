import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';

@Component({
  selector: 'ngx-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

    breadcrumbs = [
        {
          title: 'Home',
          link: '/dashboard',
          icon: 'fa fa-home',
        },
        {
          title: 'Network List',
          link: '/networks',
          icon: 'fa fa-user-secret',
        },
        {
          title: 'Detail',
          icon: 'fa fa-eye',
        },
      ];
      headerData = {
        title: 'Network Detail',
      };
      constructor(    private http: HttpClient,
        private admin: AdminService,
        private router: Router,
        private route: ActivatedRoute) {
            
        }
        network = {
            api_key: '',
            primary_commission: 5,
            api_allow_domain: '',
            api_url: '',
            name: '',
            email: '',
            mobile: '',
            company_name: '',
            website: '',
            name_of_the_bank:'',
            tel:'',
            name_on_the_account:'',
            account_number:'',
            swift_code:'',
            invoice_raised_type:''
          };

    ngOnInit(): void {
        this.admin
        .get(`networks/` + this.route.snapshot.paramMap.get('id') )
        .subscribe((network: any) => {
        this.network = network.data.network;
    });

}
}
