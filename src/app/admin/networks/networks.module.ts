import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NetworksRoutingModule} from './networks-routing.module';
import {NetworksComponent} from './networks.component';
import {
  NbButtonModule,
  NbCardModule,
  NbChatModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
  NbToggleModule,
} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import {AdminModule} from '../admin.module';
import {AgGridModule} from 'ag-grid-angular';
import { SettingComponent } from './setting/setting.component';
import {FormsModule} from '@angular/forms';
import { CreateEditComponent } from './create-edit/create-edit.component';
import {Ng2TelInputModule} from 'ng2-tel-input';
import { NetworkChatComponent } from './network-chat/network-chat.component';
import { DetailComponent } from './detail/detail.component';
import { NetworkRequestsComponent } from './network-requests/network-requests.component';


@NgModule({
  declarations: [
    NetworksComponent,
    SettingComponent,
    CreateEditComponent,
    NetworkChatComponent,
    DetailComponent,
    NetworkRequestsComponent,
  ],
  imports: [
    CommonModule,
    NetworksRoutingModule,
    NbCardModule,
    AdminThemeModule,
    NbSelectModule,
    NbToggleModule,
    AdminModule,
    AgGridModule,
    FormsModule,
    NbIconModule,
    NbFormFieldModule,
    NbInputModule,
    NbButtonModule,
    Ng2TelInputModule,
    NbChatModule,
  ],
})
export class NetworksModule {
}
