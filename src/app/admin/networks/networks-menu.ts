export const MENUS = [
  {
    id: 'network',
    title: 'Network Manager',
    icon: 'fa fa-user-secret',
    link: '/networks',
  },
  {
    id: 'request',
    title: 'Network New Requests',
    icon: 'fa fa-request',
    link: '/networks/requests',
  },
];
