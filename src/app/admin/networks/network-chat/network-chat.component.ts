import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';

@Component({
  selector: 'ngx-network-chat',
  templateUrl: './network-chat.component.html',
  styleUrls: ['./network-chat.component.scss']
})
export class NetworkChatComponent implements OnInit {

    breadcrumbs = [
        {
          title: 'Home',
          link: '/dashboard',
          icon: 'fa fa-home',
        },
        {
          title: 'Network List',
          link: '/networks',
          icon: 'fa fa-user-secret',
        },
        {
          title: 'message',
          icon: 'fa fa-envelope',
        },
      ];

      headerData = {
        title: 'Network Messages',
      };


  messages: any[];

  constructor(protected admin: AdminService,private route: ActivatedRoute) {
  
  }

  sendMessage(event: any) {

      this.admin.post('network-messages/'+ this.route.snapshot.paramMap.get('id'),{'message' :event.message}).subscribe((res:any) => {
          this.messages.push(res.data);
        });
   
  }

  ngOnInit(): void {
      this.admin.get('network-messages/'+ this.route.snapshot.paramMap.get('id')).subscribe((res:any) => {
          this.messages  = res.data;
        });
  }


}
