import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkChatComponent } from './network-chat.component';

describe('NetworkChatComponent', () => {
  let component: NetworkChatComponent;
  let fixture: ComponentFixture<NetworkChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetworkChatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
