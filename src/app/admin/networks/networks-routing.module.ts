import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NetworksComponent} from './networks.component';
import {SettingComponent} from './setting/setting.component';
import {CreateEditComponent} from './create-edit/create-edit.component';
import { NetworkChatComponent } from './network-chat/network-chat.component';
import { DetailComponent } from './detail/detail.component';
import { NetworkRequestsComponent } from './network-requests/network-requests.component';

const routes: Routes = [
  {
    path: '',
    component: NetworksComponent,
  },
  {
    path: 'requests',
    component: NetworkRequestsComponent,
  },
  {
    path: 'create',
    component: CreateEditComponent,
  },
  {
    path: 'edit/:id',
    component: CreateEditComponent,
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
  },
  {
    path: 'setting/:id',
    component: SettingComponent,
  },
  {
    path: 'message/:id',
    component: NetworkChatComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NetworksRoutingModule {
}
