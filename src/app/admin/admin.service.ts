import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs';
import * as Noty from 'noty';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable()
export class AdminService {
  env = environment;
  baseUrl = this.env.apiUrl;
  private messageSource = new BehaviorSubject('default message');
  getMessage = this.messageSource.asObservable();

  constructor(private http: HttpClient) {
  }

  setMessage(message: any) {
    this.messageSource.next(message);
  }

  isLogin() {
    let curUser = localStorage.getItem('currentUser');
    const curToken = localStorage.getItem('currentUserToken');

    if (curUser) {
      curUser = JSON.parse(curUser).data;
      return curUser !== undefined && curToken !== undefined && curUser['token'] !== undefined && curUser['role'] === 'admin';
    }
  }

  getFullUrl(url: any) {
    return this.baseUrl + 'admin/' + url;
  }

  notValid(name) {
   return name.invalid && (name.dirty || name.touched);
  }

  valid(name) {
    return name.valid;
  }

  get(url: any) {
    return this.http
      .get(
        this.getFullUrl(url),
        {headers: this.getHeader()},
      );
  }
  getFile(url: any) {
    this.http
      .get(
        this.getFullUrl(url),
        {headers: this.getHeader(), responseType: 'blob'},
      ).subscribe((response: any) => {
          const  dataType = response.type;
          const  binaryData = [];
          binaryData.push(response);
          const  downloadLink = document.createElement('a');
          downloadLink.target = '_blank';
          downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },
        (error) => {
          this.noty('Error While Downloading', true);
        });
  }

  post(url: any, data: any, isForm = false) {
    return this.http.post(this.getFullUrl(url), data, {headers: this.getHeader(isForm)});
  }
  noty(message, isError = false) {
    const type = !isError ? 'success' : 'error';
    new Noty({
      layout: 'topRight',
      timeout: 2000,
      theme: 'metroui',
      type: type,
      text: message,
    }).show();
  }
  showValidationError(form, error) {
    for (const key  of Object.keys(error.errors)) {
      const errors = error.errors[key];
      if (form.form.controls[key] !== undefined && errors.length > 0) {
        form.form.controls[key].setErrors({'message': errors[0]});
      }
    }
  }

  tableSize(params) {
    const gridWidth = document.getElementById('grid-wrapper').offsetWidth;
    const columnsToShow = [];
    const columnsToHide = [];
    let totalColsWidth = 0;
    const allColumns = params.columnApi.getAllColumns();
    for (let i = 0; i < allColumns.length; i++) {
      const column = allColumns[i];
      if (column.getColDef().hide !== true) {
        totalColsWidth += column.getMinWidth();
        if (totalColsWidth > gridWidth) {
          columnsToHide.push(column.colId);
        } else {
          columnsToShow.push(column.colId);
        }
      } else {
        columnsToHide.push(column.colId);
      }
    }
    params.columnApi.setColumnsVisible(columnsToShow, true);
    params.columnApi.setColumnsVisible(columnsToHide, false);
    setTimeout(() => {
      params.api.sizeColumnsToFit();
    }, 50);
  }

  tableSearch(gridApi, filterText) {

  }

  formatFormData(form) {
    const formData: FormData = new FormData();
    const data = form.value;
    Object.keys(data).forEach(function (value) {
      const formControl = form.controls[value];
      if (formControl.file !== undefined) {
        formData.append(value, formControl.file, formControl.file.name);
      } else {
        formData.append(value, formControl.value);
      }
    });
    return formData;

  }
  showLoader(text = 'Please Wait !') {
    Swal.fire({
      title: text,
      allowOutsideClick: false,
      showConfirmButton: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }
  hideLoader() {
    Swal.close();
  }

  getHeader(isForm = false) {
    const curToken = localStorage.getItem('currentUserToken');

    if (isForm) {
      return new HttpHeaders({
        'enctype': 'multipart/form-data',
        'Authorization': `Bearer ${curToken}`,
      });
    } else {
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${curToken}`,
      });
    }

  }
}
