export const MENUS = [
  {
    id: 'driver',
    title: 'Driver Reports',
    icon: 'fa fa-user-secret',
    link: '/reports',
  },
  {
    id: 'booking',
    title: 'Bookings Reports',
    icon: 'fa fa-calendar',
    link: '/reports/booking-reports',
  },
  {
    id: 'invoice',
    title: 'Invoice Reports',
    icon: 'fa fa-file',
    link: '/reports/invoice-reports',
  },
  {
    id: 'transaction',
    title: 'Transaction Reports',
    icon: 'fa fa-list',
    link: '/reports/transaction-reports',
  },
];
