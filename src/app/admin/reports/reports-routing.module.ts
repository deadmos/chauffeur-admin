import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookingReportCreateComponent } from './booking-report-create/booking-report-create.component';
import { BookingReportsComponent } from './booking-reports/booking-reports.component';
import { CustomerReportsComponent } from './customer-reports/customer-reports.component';
import { DriverCreateComponent } from './driver-create/driver-create.component';
import { InvoiceReportCreateComponent } from './invoice-report-create/invoice-report-create.component';
import { InvoiceReportsComponent } from './invoice-reports/invoice-reports.component';
import { PaymentReportsComponent } from './payment-reports/payment-reports.component';
import { ReportsComponent } from './reports.component';
import { TransactionReportCreateComponent } from './transaction-report-create/transaction-report-create.component';
import { TransactionReportsComponent } from './transaction-reports/transaction-reports.component';

const routes: Routes = [
    {
      path: '',
      component: ReportsComponent,
    },
    {
        path: 'create-driver-report',
        component: DriverCreateComponent,
    },
    {
        path: 'booking-reports',
        component: BookingReportsComponent,
    },
    {
        path: 'create-booking-report',
        component: BookingReportCreateComponent,
    },
    {
        path: 'invoice-reports',
        component: InvoiceReportsComponent,
    },
    {
        path: 'create-invoice-report',
        component: InvoiceReportCreateComponent,
    },
    {
        path: 'transaction-reports',
        component: TransactionReportsComponent,
    },
    {
        path: 'create-transaction-report',
        component: TransactionReportCreateComponent,
    },
    {
        path: 'customer-reports',
        component: CustomerReportsComponent,
    },
    {
        path: 'payment-reports',
        component: PaymentReportsComponent,
    },
   ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsRoutingModule { }
