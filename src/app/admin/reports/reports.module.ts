import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';
import { ReportsRoutingModule} from './reports-routing.module';
import { NbButtonModule, NbCardModule, NbDatepickerModule, NbFormFieldModule, NbIconModule, NbInputModule} from '@nebular/theme';
import { AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import { NbSelectModule, NbToggleModule } from '@nebular/theme';
import { AdminModule} from '../admin.module';
import { FormsModule} from "@angular/forms";
import { ReportsComponent } from './reports.component';
import { DriverCreateComponent } from './driver-create/driver-create.component';
import { BookingReportsComponent } from './booking-reports/booking-reports.component';
import { InvoiceReportsComponent } from './invoice-reports/invoice-reports.component';
import { TransactionReportsComponent } from './transaction-reports/transaction-reports.component';
import { CustomerReportsComponent } from './customer-reports/customer-reports.component';
import { PaymentReportsComponent } from './payment-reports/payment-reports.component';
import { BookingReportCreateComponent } from './booking-report-create/booking-report-create.component';
import { NbMomentDateModule } from '@nebular/moment';
import { InvoiceReportCreateComponent } from './invoice-report-create/invoice-report-create.component';
import { TransactionReportCreateComponent } from './transaction-report-create/transaction-report-create.component';


@NgModule({
  declarations: [ReportsComponent, DriverCreateComponent, BookingReportsComponent, InvoiceReportsComponent, TransactionReportsComponent, CustomerReportsComponent, PaymentReportsComponent, BookingReportCreateComponent, InvoiceReportCreateComponent, TransactionReportCreateComponent],
  imports: [
    CommonModule,
    AgGridModule,
    ReportsRoutingModule,
    NbCardModule,
    AdminThemeModule,
    NbSelectModule,
    NbToggleModule,
    AdminModule,
    NbInputModule,
    NbButtonModule,
    FormsModule,
    NbIconModule,
    NbFormFieldModule,
    NbInputModule,
    NbButtonModule,
    NbMomentDateModule,
    NbDatepickerModule,
    
  ],
})
export class ReportsModule { }
