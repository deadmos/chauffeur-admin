import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {AdminService} from '../../admin.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-invoice-report-create',
  templateUrl: './invoice-report-create.component.html',
  styleUrls: ['./invoice-report-create.component.scss'],
})
export class InvoiceReportCreateComponent implements OnInit {
  min: Date;
  max: Date;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Invoice Reports List',
      link: '/reports/invoice-reports',
      icon: 'fa fa-calendar',
    },
    {
      title: 'Create',
      icon: 'fa fa-plus',
    },
  ];
  headerData = {
    title: 'Add New Invoice Report',
  };
  report = {
    name: '',
    user_type: '',
    type: '',
    date_range: '',
  };
      submitting = false;
      constructor(    private http: HttpClient,
                      private admin: AdminService,
                      private router: Router,
                      private route: ActivatedRoute) {
      }

  ngOnInit(): void {
  }

  notValid(name) {
    return this.admin.notValid(name);
  }

  valid(name) {
    return this.admin.valid(name);
  }

  getError(name) {
    if (name.control.getError('required') === true) {
      return 'Required';
    } else {
      return name.control.getError('message');
    }

  }

  submitForm(settingForm): void {
    this.submitting = true;

    this.admin
      .post(`invoice-reports`, settingForm.value)
      .subscribe((res: any) => {
          this.submitting = false;
          if (res.status) {
            this.admin.noty('Successfully Created Invoice Report.');
            this.router.navigate(['reports/invoice-reports']);
          } else {
            this.admin.noty(res.message, true);
          }
        },
        (error) => {
          this.submitting = false;

          switch (error.status) {
            case 422:
                this.admin.noty('Validation Error', true);
                this.admin.showValidationError(settingForm, error.error);
                break;
          }
        });
  }

}
