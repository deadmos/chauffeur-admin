import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-driver-create',
  templateUrl: './driver-create.component.html',
  styleUrls: ['./driver-create.component.scss']
})
export class DriverCreateComponent implements OnInit {

    breadcrumbs = [
        {
          title: 'Home',
          link: '/dashboard',
          icon: 'fa fa-home',
        },
        {
          title: 'Driver Reports List',
          link: '/reports',
          icon: 'fa fa-user-secret',
        },
        {
          title: 'Create',
          icon: 'fa fa-plus',
        },
      ];
      headerData = {
        title: 'Add New Driver Report',
      };
      report = {
        name: '',
        user_type: '',
        type: '',
      };
      submitting = false;
      generating = false;
      constructor(    private http: HttpClient,
                      private admin: AdminService,
                      private router: Router,
                      private route: ActivatedRoute) {
      }

  ngOnInit(): void {
  }

  notValid(name) {
    return this.admin.notValid(name);
  }

  valid(name) {
    return this.admin.valid(name);
  }

  getError(name) {
    if (name.control.getError('required') === true) {
      return 'Required';
    } else {
      return name.control.getError('message');
    }

  }

  submitForm(settingForm): void {
    this.submitting = true;

    this.admin
      .post(`driver-reports`, settingForm.value)
      .subscribe((res: any) => {
          this.submitting = false;
          this.admin.noty('Successfully Created Driver Report.');
          this.router.navigate(['reports']);
        },
        (error) => {
          this.submitting = false;

          switch (error.status) {
            case 422:
                this.admin.noty('Vlidation Error', true);
                this.admin.showValidationError(settingForm, error.error);
                break;
          }
        });
  }

}
