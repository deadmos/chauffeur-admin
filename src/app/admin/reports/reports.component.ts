import { Component, OnInit } from '@angular/core';
import {MENUS} from './reports-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {Router} from '@angular/router';
import {ActionsComponent} from '../common/actions/actions.component';
import {NbThemeService} from '@nebular/theme';
import {AgDatatable} from '../admin.extends';
@Component({
  selector: 'ngx-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent extends AgDatatable implements OnInit {
    public columnDefs;
    public defaultColDef;
    public rowData: {};
    public rowSelection;
    url = 'driver-reports';
    menus = MENUS;
    breadcrumbs = [
      {
        title: 'Home',
        link: '/dashboard',
        icon: 'fa fa-home',
      },
      {
        title: 'Driver Reports',
        icon: 'fa fa-user-secret',
      },
    ];
    headerData = {
      title: 'Driver Report List',
      action: true,
      actionLink: '/reports/create-driver-report',
      actionTitle: 'Add New Driver Report',
    };
    constructor(
      protected http: HttpClient,
      protected admin: AdminService,
      protected router: Router,
      protected themeService: NbThemeService,
      ) {
      super();
      this.columnDefs = [
        {
          field: 'globalSearch',
          hide: true,
          initialHide: true,
          filter: 'agTextColumnFilter',
          filterParams: {
            newRowsAction: 'keep',
          },
        },
        { 
            field: 'id', 
            headerName: 'Id', 
            sortable: false, 
            filter: false , 
            maxWidth: 80,  
            minWidth: 60
        },
        {
          field: 'name',
          headerName: 'Name',
          sortable: true,
          filter: true,
          maxWidth: 250,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
          field: 'user_type',
          headerName: 'User Type',
          sortable: true,
          filter: true,
          maxWidth: 250,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
          field: 'type',
          headerName: 'Type',
          sortable: true,
          filter: true,
          maxWidth: 150,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        { field: 'action', sortable: true, filter: false ,
          cellRendererFramework: ActionsComponent,
          displayType: 'dropdown',
          cellRendererParams: [
            {
              title: 'Download Pdf',
              href: 'driver-reports/download-pdf/',
              type: 'download',
            },
            {
              title: 'Download Excel',
              href: 'driver-reports/download-excel/',
              type: 'download',
            },
            {
              title: 'Delete Report',
              href: 'driver-reports/',
              type: 'delete',
              confirm: {
                title: 'Are you sure want to delete?',
                successMessage: 'Report deleted successfully',
              },
            },
          ],
        },
      ];
      this.defaultColDef = { resizable: true };
      this.rowSelection = 'multiple';
    }
  
    ngOnInit(): void {
  
    }
  
  }
