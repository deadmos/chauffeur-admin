import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {AdminService} from '../../admin.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MENUS} from '../reports-menu';

@Component({
  selector: 'ngx-transaction-report-create',
  templateUrl: './transaction-report-create.component.html',
  styleUrls: ['./transaction-report-create.component.scss'],
})
export class TransactionReportCreateComponent implements OnInit {

  min: Date;
  max: Date;

  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Transaction Reports List',
      link: '/reports/transaction-reports',
      icon: 'fa fa-calendar',
    },
    {
      title: 'Create',
      icon: 'fa fa-plus',
    },
  ];
  headerData = {
    title: 'Add New Transaction Report',
  };
  menus = MENUS;
  report = {
    name: '',
    user_type: '',
    type: '',
    date_range: '',
  };
  submitting = false;
  generating = false;
  constructor(    private http: HttpClient,
                  private admin: AdminService,
                  private router: Router,
                  private route: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

  notValid(name) {
    return this.admin.notValid(name);
  }

  valid(name) {
    return this.admin.valid(name);
  }

  getError(name) {
    if (name.control.getError('required') === true) {
      return 'Required';
    } else {
      return name.control.getError('message');
    }

  }

  submitForm(settingForm): void {
    this.submitting = true;

    this.admin
      .post(`transaction-reports`, settingForm.value)
      .subscribe((res: any) => {
          this.submitting = false;
          if (res.status) {
            this.admin.noty('Successfully Created Transaction Report.');
            this.router.navigate(['reports/transaction-reports']);
          } else {
            this.admin.noty(res.message, true);
          }
        },
        (error) => {
          this.submitting = false;

          switch (error.status) {
            case 422:
              this.admin.noty('Validation Error', true);
              this.admin.showValidationError(settingForm, error.error);
              break;
          }
        });
  }

}
