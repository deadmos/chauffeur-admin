import { Component, OnInit } from '@angular/core';
import {MENUS} from '../reports-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {Router} from '@angular/router';
import {ActionsComponent} from '../../common/actions/actions.component';
import {NbThemeService} from '@nebular/theme';
import {AgDatatable} from '../../admin.extends';

@Component({
  selector: 'ngx-transaction-reports',
  templateUrl: './transaction-reports.component.html',
  styleUrls: ['./transaction-reports.component.scss'],
})
export class TransactionReportsComponent extends AgDatatable implements OnInit {
    public columnDefs;
    public defaultColDef;
    public rowData: {};
    public rowSelection;
    url = 'transaction-reports';
    menus = MENUS;
    breadcrumbs = [
      {
        title: 'Home',
        link: '/dashboard',
        icon: 'fa fa-home',
      },
      {
        title: 'Transaction Reports',
        icon: 'fa fa-user-secret',
      },
    ];
    headerData = {
      title: 'Transaction Report List',
      action: true,
      actionLink: '/reports/create-transaction-report',
      actionTitle: 'Add New transaction Report',
    };
    constructor(
      protected http: HttpClient,
      protected admin: AdminService,
      protected router: Router,
      protected themeService: NbThemeService,
      ) {
      super();
      this.columnDefs = [
        {
          field: 'globalSearch',
          hide: true,
          initialHide: true,
          filter: 'agTextColumnFilter',
          filterParams: {
            newRowsAction: 'keep',
          },
        },
        {
            field: 'id',
            headerName: 'Id',
            sortable: false,
            filter: false ,
            maxWidth: 80,
            minWidth: 60,
        },
        {
          field: 'name',
          headerName: 'Name',
          sortable: true,
          filter: true,
          maxWidth: 250,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
          field: 'type',
          headerName: 'Type',
          sortable: true,
          filter: true,
          maxWidth: 150,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        { field: 'action', sortable: true, filter: false ,
          cellRendererFramework: ActionsComponent,
          displayType: 'dropdown',
          cellRendererParams: [
            {
              title: 'Download Pdf',
              href: 'transaction-reports/download-pdf/',
              type: 'download',
            },
            {
              title: 'Download Excel',
              href: 'transaction-reports/download-excel/',
              type: 'download',
            },
            {
              title: 'Delete Report',
              href: 'transaction-reports/',
              type: 'delete',
              confirm: {
                title: 'Are you sure want to delete?',
                successMessage: 'Transaction Report Deleted Successfully',
              },
            },
          ],
        },
      ];
      this.defaultColDef = { resizable: true };
      this.rowSelection = 'multiple';
    }

    ngOnInit(): void {

    }

}
