import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingReportCreateComponent } from './booking-report-create.component';

describe('BookingReportCreateComponent', () => {
  let component: BookingReportCreateComponent;
  let fixture: ComponentFixture<BookingReportCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookingReportCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingReportCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
