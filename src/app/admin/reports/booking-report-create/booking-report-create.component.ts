import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {AdminService} from '../../admin.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-booking-report-create',
  templateUrl: './booking-report-create.component.html',
  styleUrls: ['./booking-report-create.component.scss']
})
export class BookingReportCreateComponent implements OnInit {

    min: Date;
    max: Date;

    breadcrumbs = [
        {
          title: 'Home',
          link: '/dashboard',
          icon: 'fa fa-home',
        },
        {
          title: 'Booking Reports List',
          link: '/reports/booking-reports',
          icon: 'fa fa-calendar',
        },
        {
          title: 'Create',
          icon: 'fa fa-plus',
        },
      ];
      headerData = {
        title: 'Add New Booking Report',
      };
      report = {
        name: '',
        user_type: '',
        type: '',
        date_range:''
      };
      submitting = false;
      generating = false;
      constructor(    private http: HttpClient,
                      private admin: AdminService,
                      private router: Router,
                      private route: ActivatedRoute) {
      }

  ngOnInit(): void {
  }

  notValid(name) {
    return this.admin.notValid(name);
  }

  valid(name) {
    return this.admin.valid(name);
  }

  getError(name) {
    if (name.control.getError('required') === true) {
      return 'Required';
    } else {
      return name.control.getError('message');
    }

  }

  submitForm(settingForm): void {
    this.submitting = true;

    this.admin
      .post(`booking-reports`, settingForm.value)
      .subscribe((res: any) => {
          this.submitting = false;
          this.admin.noty('Successfully Created Booking Report.');
          this.router.navigate(['reports/booking-reports']);
        },
        (error) => {
          this.submitting = false;

          switch (error.status) {
            case 422:
                this.admin.noty('Vlidation Error', true);
                this.admin.showValidationError(settingForm, error.error);
                break;
          }
        });
  }

}
