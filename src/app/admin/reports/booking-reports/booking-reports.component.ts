import { Component, OnInit } from '@angular/core';
import {MENUS} from '../reports-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {Router} from '@angular/router';
import {ActionsComponent} from '../../common/actions/actions.component';
import {NbThemeService} from '@nebular/theme';
import {AgDatatable} from '../../admin.extends';

@Component({
  selector: 'ngx-booking-reports',
  templateUrl: './booking-reports.component.html',
  styleUrls: ['./booking-reports.component.scss']
})
export class BookingReportsComponent extends AgDatatable implements OnInit {
    public columnDefs;
    public defaultColDef;
    public rowData: {};
    public rowSelection;
    url = 'booking-reports';
    menus = MENUS;
    breadcrumbs = [
      {
        title: 'Home',
        link: '/dashboard',
        icon: 'fa fa-home',
      },
      {
        title: 'Booking Reports',
        icon: 'fa fa-user-secret',
      },
    ];
    headerData = {
      title: 'Booking Report List',
      action: true,
      actionLink: '/reports/create-booking-report',
      actionTitle: 'Add New Booking Report',
    };
    constructor(
      protected http: HttpClient,
      protected admin: AdminService,
      protected router: Router,
      protected themeService: NbThemeService,
      ) {
      super();
      this.columnDefs = [
        {
          field: 'globalSearch',
          hide: true,
          initialHide: true,
          filter: 'agTextColumnFilter',
          filterParams: {
            newRowsAction: 'keep',
          },
        },
        { 
            field: 'id', 
            headerName: 'Id', 
            sortable: false, 
            filter: false , 
            maxWidth: 80,  
            minWidth: 60
        },
        {
          field: 'name',
          headerName: 'Name',
          sortable: true,
          filter: true,
          maxWidth: 250,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
          field: 'user_type',
          headerName: 'User Type',
          sortable: true,
          filter: true,
          maxWidth: 250,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
          field: 'type',
          headerName: 'Type',
          sortable: true,
          filter: true,
          maxWidth: 150,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        { field: 'action', sortable: true, filter: false ,
          cellRendererFramework: ActionsComponent,
          displayType: 'dropdown',
          cellRendererParams: [
            {
              title: 'Download Pdf',
              href: 'booking-reports/download-pdf/',
              type: 'download',
            },
            {
              title: 'Download Excel',
              href: 'booking-reports/download-excel/',
              type: 'download',
            },
            {
              title: 'Delete Report',
              href: 'booking-reports/',
              type: 'delete',
              confirm: {
                title: 'Are you sure want to delete?',
                successMessage: 'Booking Report deleted successfully',
              },
            },
          ],
        },
      ];
      this.defaultColDef = { resizable: true };
      this.rowSelection = 'multiple';
    }
  
    ngOnInit(): void {
  
    }
}
