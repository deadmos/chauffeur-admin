import { Component, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { AgDatatable } from '../admin.extends';
import { AdminService } from '../admin.service';

@Component({
  selector: 'ngx-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent extends AgDatatable implements OnInit {
    public columnDefs;
    public defaultColDef;
    public rowData: {};
    public rowSelection;
    menus = [];
    url = 'notifications-list';
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Notifications',
      icon: 'fa fa-file',
    },
  ];
  headerData = {
    title: 'Notifications',
    action: false,
  };
  constructor(
    protected admin: AdminService,
    protected themeService: NbThemeService,
  ) {
    super();
      this.columnDefs = [
        {
          field: 'globalSearch',
          hide: true,
          initialHide: true,
          filter: 'agTextColumnFilter',
          filterParams: {
            newRowsAction: 'keep',
          },
        },
        {
              field: 'data.notification_title',
              searchField: 'users.name',
              headerName: 'Name',
              sortable: true,
              filter: true,
              filterParams: this.filterParams,
              minWidth: 60,
              cellRenderer: function(params) {
                return params.value ? params.value : '';
            }
          },
      ];
      this.defaultColDef = { resizable: true };
      this.rowSelection = 'multiple';
  }

  ngOnInit(): void {
  }

}
