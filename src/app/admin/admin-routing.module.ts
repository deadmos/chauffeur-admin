import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminComponent} from './admin.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuardService} from '../auth/admin/auth-guard.service';
import {NotFoundComponent} from './miscellaneous/not-found/not-found.component';
import {FrontendLogsComponent} from './frontend-logs/frontend-logs.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {TransactionsComponent} from './transactions/transactions.component';
import {EarningsComponent} from './earnings/earnings.component';
import {DriverTransactionsComponent} from './driver-transactions/driver-transactions.component';
import {DriverPayoutComponent} from './transactions/driver-payout/driver-payout.component';
import {NetworkPayoutComponent} from './transactions/network-payout/network-payout.component';

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  canActivate: [AuthGuardService],
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'pages',
      loadChildren: () => import('./pages/pages.module')
        .then(m => m.PagesModule),
    },
    {
      path: 'networks',
      loadChildren: () => import('./networks/networks.module')
        .then(m => m.NetworksModule),
    },
    {
      path: 'drivers',
      loadChildren: () => import('./drivers/drivers.module')
        .then(m => m.DriversModule),
    },
    {
      path: 'bookings',
      loadChildren: () => import('./bookings/bookings.module')
        .then(m => m.BookingsModule),
    },
    {
      path: 'areas',
      loadChildren: () => import('./areas/areas.module')
        .then(m => m.AreasModule),
    },
    {
      path: 'seo',
      loadChildren: () => import('./seos/seos.module')
        .then(m => m.SeosModule),
    },
    {
      path: 'faqs',
      loadChildren: () => import('./faq/faq.module')
        .then(m => m.FaqModule),
    },
    {
      path: 'reports',
      loadChildren: () => import('./reports/reports.module')
        .then(m => m.ReportsModule),
    },
    {
      path: 'faq-categories',
      loadChildren: () => import('./faq-category/faq-category.module')
        .then(m => m.FaqCategoryModule),
    },
    {
      path: 'notifications',
      component: NotificationsComponent,
    },
    {
      path: 'frontend-logs',
      component: FrontendLogsComponent,
    },
    {
      path: 'settings',
      loadChildren: () => import('./settings/settings.module')
        .then(m => m.SettingsModule),
    },
    {
      path: 'fleets',
      loadChildren: () => import('./fleets/fleets.module')
        .then(m => m.FleetsModule),
    },
    {
      path: 'transactions',
      component: TransactionsComponent,
    },
    {
      path: 'driver-payouts',
      component: DriverPayoutComponent,
    },
    {
      path: 'my-payouts',
      component: NetworkPayoutComponent,
    },
    {
      path: 'driver-transactions',
      component: DriverTransactionsComponent,
    },
    {
      path: 'earnings',
      component: EarningsComponent,
    },
    {
      path: 'invoices',
      loadChildren: () => import('./invoices/invoices.module')
        .then(m => m.InvoicesModule),
    },
    {
      path: 'statements',
      loadChildren: () => import('./statements/statements.module')
        .then(m => m.StatementsModule),
    },
    {
      path: 'news-letters',
      loadChildren: () => import('./news-letters/news-letters.module')
        .then(m => m.NewsLettersModule),
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {
}
