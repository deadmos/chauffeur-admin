import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';
import { Params, Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NotyService } from 'app/admin/services/noty.service';

@Component({
    selector: 'ngx-create-edit',
    templateUrl: './create-edit.component.html',
    styleUrls: ['./create-edit.component.scss'],
})
export class CreateEditComponent implements OnInit {
    id: number;
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Faq List',
            link: '/faqs',
            icon: 'fa fa-question-circle',
        },
        {
            title: 'Details',
            icon: 'fa fa-file',
        },
    ];
    headerData = {
        title: 'Faq Detail',
    };
    faqResponse: any = { title: '', description: '', order: '', category_id: ''};
    categoryResponse: any;
    constructor(
        private admin: AdminService,
        private router: Router,
        private noty: NotyService,
        private activatedRoute: ActivatedRoute,
    ) {
        this.activatedRoute.params.subscribe((params: Params) => { this.id = params.id; });
        if (this.id) {
            this.admin.get(`faqs/${this.id}`).subscribe((responseData) => {
                this.faqResponse = responseData['data'];
            });
        } else {
            this.admin.get(`faq-categories`).subscribe((responseData) => {
                this.categoryResponse = responseData;
            });
        }
    }

    ngOnInit(): void {
    }

    selectCategoryId(id) {
        this.faqResponse.category_id = id;
    }
    showLoader() {
        Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading();
            },
        });
    }

    submitForm() {
        let postUrl;
        if (this.faqResponse.id !== undefined) {
            postUrl = `faqs/${this.faqResponse.id}/update`;
        } else {
            postUrl = `faqs/store`;
        }

        this.showLoader();
        this.admin.post(postUrl, this.faqResponse).subscribe({
            next: (res) => {
                const response: any = res;
                this.noty.success(response.message);
                Swal.close();
                this.router.navigate(['faqs']);
            },
            error: (error) => {
                Swal.close();
                this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
            },
        });
    }

  notValid(name) {
    return name.invalid && (name.dirty || name.touched);
  }
  valid(name) {
    return name.valid;
  }

}
