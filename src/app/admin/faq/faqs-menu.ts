export const MENUS = [
  {
    id: 'faq',
    title: 'Faq Manager',
    icon: 'fa fa-question-circle',
    link: '/faqs',
  },
  {
    id: 'faq_category',
    title: 'Faq Category Manager',
    icon: 'fa fa-list',
    link: '/faq-categories',
  },
];
