import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FaqComponent} from './faq.component';
import {CreateEditComponent} from './create-edit/create-edit.component';

const routes: Routes = [{

      path: '',
      component: FaqComponent,
    },
    {
        path: 'create',
        component: CreateEditComponent,
    },
    {
        path: 'edit/:id',
        component: CreateEditComponent,
    },
    {
        path: 'delete/:id',
        component: CreateEditComponent,
    }
   ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FaqRoutingModule { }
