import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {Router} from '@angular/router';
import {MENUS} from './faqs-menu';
import { ActionsComponent } from '../common/actions/actions.component';

@Component({
  selector: 'ngx-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})
export class FaqComponent implements OnInit {

  private gridApi;
  private gridColumnApi;

  public columnDefs;
  public defaultColDef;
  public rowData: {};

  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Faq List',
      icon: 'fa fa-file',
    },
  ];
  headerData = {
    title: 'Faq List',
    action: true,
    actionLink: '/faqs/create',
    actionTitle: 'Add New Faq',
  };



  constructor(private http: HttpClient, private admin: AdminService, private router: Router) {
    this.columnDefs = [
      { field: 'id', headerName: 'Id', sortable: false, filter: false , maxWidth: 40},
      { field: 'title', headerName: 'Title', sortable: false, filter: false, maxWidth: 400},
      { field: 'category.name', headerName: 'Category', sortable: false, filter: false, maxWidth: 400},
      { field: 'description', headerName: 'Description', sortable: false, filter: false, maxWidth: 400},
      { 
        field: 'action', sortable: true, filter: false,  maxWidth: 120,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'Edit Page',
            href: '/faqs/edit',
            type: 'edit',
          },
          {
            title: 'Delete Page',
            href: 'faqs/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete?',
              successMessage: 'FAQ deleted successfully',
            },
          },
        ],
      }

    ];
    this.defaultColDef = { resizable: true };
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }
  onGridSizeChanged(params) {
    const gridWidth = document.getElementById('grid-wrapper').offsetWidth;
    const columnsToShow = [];
    const columnsToHide = [];
    let totalColsWidth = 0;
    const allColumns = params.columnApi.getAllColumns();
    for (let i = 0; i < allColumns.length; i++) {
      const column = allColumns[i];
      totalColsWidth += column.getMinWidth();
      if (totalColsWidth > gridWidth) {
        columnsToHide.push(column.colId);
      } else {
        columnsToShow.push(column.colId);
      }
    }
    params.columnApi.setColumnsVisible(columnsToShow, true);
    params.columnApi.setColumnsVisible(columnsToHide, false);
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {

    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    const pageListUrl = this.admin.getFullUrl('faqs');
    const headers = this.admin.getHeader();
    this.http.get(pageListUrl,{ headers: headers })
    .subscribe((data) => {
        this.rowData = data;
      });
  }
  ngOnInit(): void {
  }

}
