import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {Router} from '@angular/router';
import {MENUS} from '../faq/faqs-menu';
import { ActionsComponent } from '../common/actions/actions.component';

@Component({
  selector: 'ngx-faq-category',
  templateUrl: './faq-category.component.html',
  styleUrls: ['./faq-category.component.scss'],
})
export class FaqCategoryComponent implements OnInit {

  private gridApi;
  private gridColumnApi;

  public columnDefs;
  public defaultColDef;
  public rowData: {};

  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Faq Category List',
      icon: 'fa fa-question-circle',
    },
  ];
  headerData = {
    title: 'Faq Category List',
    action: true,
    actionLink: '/faq-categories/create',
    actionTitle: 'Add New Faq Category',
  };

  constructor(private http: HttpClient, private admin: AdminService, private router: Router) {
    this.columnDefs = [
      { field: 'id', headerName: 'Id', sortable: false, filter: false , maxWidth: 60},
      { field: 'name', headerName: 'Title', sortable: false, filter: false , maxWidth: 600},
      { field: 'faqs_count', headerName: 'Faq Count', sortable: false, filter: false , maxWidth: 600},
      { 
        field: 'action', sortable: true, filter: false,  maxWidth: 120,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'Edit Page',
            href: '/faq-categories/edit',
            type: 'edit',
          },
          {
            title: 'Delete Page',
            href: 'faq-categories/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete?',
              successMessage: 'FAQ categories deleted successfully',
            },
          },
        ],
      }

    ];
    this.defaultColDef = { resizable: true };
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }
  onGridSizeChanged(params) {
    const gridWidth = document.getElementById('grid-wrapper').offsetWidth;
    const columnsToShow = [];
    const columnsToHide = [];
    let totalColsWidth = 0;
    const allColumns = params.columnApi.getAllColumns();
    for (let i = 0; i < allColumns.length; i++) {
      const column = allColumns[i];
      totalColsWidth += column.getMinWidth();
      if (totalColsWidth > gridWidth) {
        columnsToHide.push(column.colId);
      } else {
        columnsToShow.push(column.colId);
      }
    }
    params.columnApi.setColumnsVisible(columnsToShow, true);
    params.columnApi.setColumnsVisible(columnsToHide, false);
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {

    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    const pageListUrl = this.admin.getFullUrl('faq-categories');
    const headers = this.admin.getHeader();
    this.http.get(pageListUrl,{ headers: headers })
      .subscribe((data) => {
        this.rowData = data;
      });
  }
  ngOnInit(): void {
  }

}
