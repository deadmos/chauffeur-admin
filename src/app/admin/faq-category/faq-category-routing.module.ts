import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FaqCategoryComponent} from './faq-category.component';
import {CreateEditComponent} from './create-edit/create-edit.component';

const routes: Routes = [{

      path: '',
      component: FaqCategoryComponent,
    },
    {
        path: 'create',
        component: CreateEditComponent,
    },
    {
        path: 'edit/:id',
        component: CreateEditComponent,
    },
    {
        path: 'delete/:id',
        component: CreateEditComponent,
    }
   ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FaqCategoryRoutingModule { }
