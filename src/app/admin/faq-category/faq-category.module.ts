import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';
import { CreateEditComponent } from './create-edit/create-edit.component';
import {FaqCategoryRoutingModule} from './faq-category-routing.module';
import {NbButtonModule, NbCardModule, NbInputModule} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import { NbSelectModule, NbToggleModule } from '@nebular/theme';
import {AdminModule} from '../admin.module';
import {FormsModule} from "@angular/forms";
import { FaqCategoryComponent } from './faq-category.component';


@NgModule({
  declarations: [FaqCategoryComponent, CreateEditComponent],
  imports: [
    CommonModule,
    AgGridModule,
    FaqCategoryRoutingModule,
    NbCardModule,
    AdminThemeModule,
    NbSelectModule,
    NbToggleModule,
    AdminModule,
    NbInputModule,
    NbButtonModule,
    FormsModule,
  ],
})
export class FaqCategoryModule { }
