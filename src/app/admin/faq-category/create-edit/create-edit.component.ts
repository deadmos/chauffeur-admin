import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';
import { Params, Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NotyService } from 'app/admin/services/noty.service';

@Component({
  selector: 'ngx-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss'],
})
export class CreateEditComponent implements OnInit {
    id: number;
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Faq Category List',
            link: '/faq-categories',
            icon: 'fa fa-question-circle',
        },
        {
            title: 'Details',
            icon: 'fa fa-file',
        },
    ];
    headerData = {
        title: 'Faq Category Detail',
    };
    categoryResponse: any = { name: ''};
  constructor(
    private admin: AdminService,
    private router: Router,
    private noty: NotyService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.activatedRoute.params.subscribe((params: Params) => { this.id = params.id; });
    if (this.id) {
        this.admin.get(`faq-categories/${this.id}`).subscribe((responseData) => {
            this.categoryResponse = responseData;
        });
    }
  }

  ngOnInit(): void {
  }
  showLoader() {
    Swal.fire({
        title: 'Please Wait !',
        allowOutsideClick: false,
        showConfirmButton: false,
        willOpen: () => {
            Swal.showLoading();
        },
    });
}

submitForm() {
    let postUrl;
    if (this.categoryResponse.id !== undefined) {
        postUrl = `faq-categories/${this.categoryResponse.id}/update`;
    } else {
        postUrl = `faq-categories/store`;
    }

    this.showLoader();
    this.admin.post(postUrl, this.categoryResponse).subscribe({
        next: (res) => {
            const response: any = res;
            this.noty.success(response.message);
            Swal.close();
            this.router.navigate(['faq-categories']);
        },
        error: (error) => {
            Swal.close();
            this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
        },
    });
}

notValid(name) {
    if (name.constructor.name === 'NgModel') {
        return name.invalid && (name.dirty || name.touched);
    } else if (name.constructor.name === 'ImageInputComponent') {
        name = name.inputNgModel;
        if (name !== undefined) {
            return name.invalid && (name.dirty || name.touched);
        }
    }
    return true;
}

valid(name) {
    if (name.constructor.name === 'NgModel') {
        return name.valid;
    } else if (name.constructor.name === 'ImageInputComponent') {
        name = name.inputNgModel;
        if (name !== undefined) {
            return name.valid;
        }
    }
}

}
