import { Component, OnInit } from '@angular/core';
import {MENUS} from './news-letters-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {Router} from '@angular/router';
import {NbThemeService} from '@nebular/theme';
import {AgDatatable} from '../admin.extends';

@Component({
  selector: 'ngx-news-letters',
  templateUrl: './news-letters.component.html',
  styleUrls: ['./news-letters.component.scss']
})
export class NewsLettersComponent extends AgDatatable implements OnInit {
    public columnDefs;
    public defaultColDef;
    public rowData: {};
    public rowSelection;
    url = 'driver-news-letters';
    menus = MENUS;
    breadcrumbs = [
      {
        title: 'Home',
        link: '/dashboard',
        icon: 'fa fa-home',
      },
      {
        title: 'Driver News Letter List',
        icon: 'fa fa-user-secret',
      },
    ];
    headerData = {
      title: 'Driver News Letter List',
      action: true,
      actionLink: '/news-letters/driver/create',
      actionTitle: 'Add New News Letter',
    };
    constructor(
      protected http: HttpClient,
      protected admin: AdminService,
      protected router: Router,
      protected themeService: NbThemeService,
      ) {
      super();
      this.columnDefs = [
        {
          field: 'globalSearch',
          hide: true,
          initialHide: true,
          filter: 'agTextColumnFilter',
          filterParams: {
            newRowsAction: 'keep',
          },
        },
        { field: 'id', headerName: 'Ref', sortable: false, filter: false , maxWidth: 80,  minWidth: 60},
        {
          field: 'via',
          headerName: 'Via',
          sortable: true,
          filter: true,
          maxWidth: 150,
          minWidth: 100,
          filterParams: this.filterParams,
        },
        {
            field: 'driver.user.name',
            headerName: 'Driver Name',
            sortable: false,
            filter: false ,
            minWidth: 60,
            maxWidth: 200,
        },
        {
          field: 'subject',
          headerName: 'Subject',
          sortable: true,
          filter: true,
          maxWidth: 250,
          minWidth: 100,
          filterParams: this.filterParams,
          
        },
       
        {
          field: 'small_message',
          headerName: 'SMS Message',
          sortable: false,
          filter: false ,
          minWidth: 60,
          maxWidth: 200,
        },
        {
            field: 'message',
            headerName: 'Email Message',
            sortable: true,
            filter: true,
            maxWidth: 350,
            minWidth: 100,
            filterParams: this.filterParams,
            cellRenderer: function(params) {
                  return params.value ? params.value : '';
              }
          },
       
      ];
      this.defaultColDef = { resizable: true };
      this.rowSelection = 'multiple';
    }
    ngOnInit(): void {

    }
  
}
