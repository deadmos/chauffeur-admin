import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';
import {MENUS} from '../news-letters-menu';

@Component({
  selector: 'ngx-news-letters-create',
  templateUrl: './news-letters-create.component.html',
  styleUrls: ['./news-letters-create.component.scss']
})
export class NewsLettersCreateComponent implements OnInit {

    menus = MENUS;
    breadcrumbs = [
        {
          title: 'Home',
          link: '/dashboard',
          icon: 'fa fa-home',
        },
        {
          title: 'Driver News Letters List',
          link: '/news-letters/driver',
          icon: 'fa fa-envelope',
        },
        {
          title: 'Create',
          icon: 'fa fa-plus',
        },
      ];
      headerData = {
        title: 'Driver News Letter Add New',
      };


  news_letter = {
    subject: '',
    type: 'driver',
    via: 'email',
    message: '',
    small_message: '',
    drivers:[]
  };
  drivers = [];
  submitting = false;
  generating = false;
  constructor(    private http: HttpClient,
                  private admin: AdminService,
                  private router: Router,
                  private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.admin
      .get('drivers-list')
      .subscribe((res: any) => {
        this.drivers = res;
        console.log(this.drivers);
      });
  }
  notValid(name) {
    return this.admin.notValid(name);
  }

  valid(name) {
    return this.admin.valid(name);
  }
  setEditorValue(mce) {
    this.news_letter.message = mce;
  }

  getError(name) {
    if (name.control.getError('required') === true) {
      return 'Required';
    } else {
      return name.control.getError('message');
    }

  }
  submitForm(settingForm): void {
    this.submitting = true;

    this.admin
      .post(`driver-news-letters`, settingForm.value)
      .subscribe((res: any) => {
          this.submitting = false;
          this.router.navigate(['news-letters/driver']);
          this.admin.noty('Successfully Updated Network Settings.');
        },
        (error) => {
          this.submitting = false;
          switch (error.status) {
            case 422:
              this.admin.noty('Vlidation Error', true);
              this.admin.showValidationError(settingForm, error.error);
              break;
          }
        });
  }


}
