import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsLettersCreateComponent } from './news-letters-create.component';

describe('NewsLettersCreateComponent', () => {
  let component: NewsLettersCreateComponent;
  let fixture: ComponentFixture<NewsLettersCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsLettersCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsLettersCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
