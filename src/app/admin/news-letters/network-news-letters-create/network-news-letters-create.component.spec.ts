import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkNewsLettersCreateComponent } from './network-news-letters-create.component';

describe('NetworkNewsLettersCreateComponent', () => {
  let component: NetworkNewsLettersCreateComponent;
  let fixture: ComponentFixture<NetworkNewsLettersCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetworkNewsLettersCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkNewsLettersCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
