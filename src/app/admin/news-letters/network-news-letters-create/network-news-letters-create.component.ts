import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';
import {MENUS} from '../news-letters-menu';

@Component({
  selector: 'ngx-network-news-letters-create',
  templateUrl: './network-news-letters-create.component.html',
  styleUrls: ['./network-news-letters-create.component.scss']
})
export class NetworkNewsLettersCreateComponent implements OnInit {
    menus = MENUS;
    breadcrumbs = [
        {
          title: 'Home',
          link: '/dashboard',
          icon: 'fa fa-home',
        },
        {
          title: 'Network News Letters List',
          link: '/news-letters/network',
          icon: 'fa fa-envelope',
        },
        {
          title: 'Create',
          icon: 'fa fa-plus',
        },
      ];
      headerData = {
        title: 'Network News Letter Add New',
      };


  news_letter = {
    subject: '',
    type: 'network',
    via: 'email',
    message: '',
    small_message: '',
    networks:[]
  };
  networks = [];
  submitting = false;
  generating = false;
  constructor(    private http: HttpClient,
                  private admin: AdminService,
                  private router: Router,
                  private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.admin
      .get('networks-list')
      .subscribe((res: any) => {
        this.networks = res;
        console.log(this.networks);
      });
  }
  notValid(name) {
    return this.admin.notValid(name);
  }

  valid(name) {
    return this.admin.valid(name);
  }
  setEditorValue(mce) {
    this.news_letter.message = mce;
  }

  getError(name) {
    if (name.control.getError('required') === true) {
      return 'Required';
    } else {
      return name.control.getError('message');
    }

  }
  submitForm(settingForm): void {
    this.submitting = true;

    this.admin
      .post(`network-news-letters`, settingForm.value)
      .subscribe((res: any) => {
          this.submitting = false;
          this.router.navigate(['news-letters/network']);
          this.admin.noty('Successfully Updated Network Settings.');
        },
        (error) => {
          this.submitting = false;
          switch (error.status) {
            case 422:
              this.admin.noty('Vlidation Error', true);
              this.admin.showValidationError(settingForm, error.error);
              break;
          }
        });
  }

}
