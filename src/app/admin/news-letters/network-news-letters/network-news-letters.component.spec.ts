import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkNewsLettersComponent } from './network-news-letters.component';

describe('NetworkNewsLettersComponent', () => {
  let component: NetworkNewsLettersComponent;
  let fixture: ComponentFixture<NetworkNewsLettersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetworkNewsLettersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkNewsLettersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
