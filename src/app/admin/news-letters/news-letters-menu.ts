export const MENUS = [
  {
    id: 'driver',
    title: 'Driver News Letter',
    icon: 'fa fa-user-secret',
    link: '/news-letters/driver',
  },
  {
    id: 'network',
    title: 'Network News Letters',
    icon: 'fa fa-building',
    link: '/news-letters/network',
  },
];
