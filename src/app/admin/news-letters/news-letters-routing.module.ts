import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NetworkNewsLettersCreateComponent } from './network-news-letters-create/network-news-letters-create.component';
import { NetworkNewsLettersComponent } from './network-news-letters/network-news-letters.component';
import { NewsLettersCreateComponent } from './news-letters-create/news-letters-create.component';
import { NewsLettersComponent } from './news-letters.component';


const routes: Routes = [
    {
      path: 'driver',
      component: NewsLettersComponent,
    },
    {
      path: 'driver/create',
      component: NewsLettersCreateComponent,
    },
    {
      path: 'network/create',
      component: NetworkNewsLettersCreateComponent,
    },
    {
      path: 'network',
      component: NetworkNewsLettersComponent,
    },
   ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewsLettersRoutingModule { }
