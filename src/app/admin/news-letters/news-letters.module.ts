import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  NbButtonModule,
  NbCardModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import {AdminModule} from '../admin.module';
import {AgGridModule} from 'ag-grid-angular';
import {FormsModule} from '@angular/forms';
import { NewsLettersRoutingModule } from './news-letters-routing.module';
import { NewsLettersComponent } from './news-letters.component';
import { NetworkNewsLettersComponent } from './network-news-letters/network-news-letters.component';
import { NewsLettersCreateComponent } from './news-letters-create/news-letters-create.component';
import { NetworkNewsLettersCreateComponent } from './network-news-letters-create/network-news-letters-create.component';



@NgModule({
  declarations: [NewsLettersComponent, NetworkNewsLettersComponent, NewsLettersCreateComponent, NetworkNewsLettersCreateComponent],
  imports: [
    CommonModule,
    AgGridModule,
    NewsLettersRoutingModule,
    AdminThemeModule,
    AdminModule,
    NbInputModule,
    NbButtonModule,
    FormsModule,
    NbCardModule,
    NbFormFieldModule,
    NbIconModule,
    NbSelectModule,

  ],
})

export class NewsLettersModule {
}
