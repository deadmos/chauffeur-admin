import { Component, OnInit } from '@angular/core';
import { MENUS } from '../fleets-menu';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../../admin.service';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { NotyService } from 'app/admin/services/noty.service';

@Component({
  selector: 'ngx-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
  menus = MENUS;
  breadcrumbs = [
      {
          title: 'Home',
          link: '/dashboard',
          icon: 'fa fa-home',
      },
      {
          title: 'Fleet Document ',
          icon: 'fa fa-file',
      },
  ];
  areas = [];
  document_nodes:any = [];
  type: any = [];

  constructor(private http: HttpClient, private noty: NotyService, private admin: AdminService) {
  }

  ngOnInit(): void {
      this.admin.get('fleet-document-type')
      .subscribe((data:any) => {
              this.document_nodes = data.documentData;
          });
  }
  toggle(type) {
    this.type[type] = this.type[type] === 1 ? 0 : 1;
}
  showType(node: any) {
      this.type = node ? node : { name: '' };
  }
  showLoader() {
      Swal.fire({
          title: 'Please Wait !',
          allowOutsideClick: false,
          showConfirmButton: false,
          willOpen: () => {
              Swal.showLoading();
          },
      });
  }
  areaChange(event:any){
      this.admin.get(`fleets/document/${event.target.value}`)
      .subscribe((data) => {
        this.document_nodes = data['documentData'];
      });
  }

  addDocument() {
    this.showLoader();
    this.admin.post(`fleets/document/add`, this.type).subscribe({
        next: (res) => {
            let response: any = res;
            this.noty.success(response.message);
            Swal.close();
            this.type = response.data;
            this.document_nodes.push(response.data);
        },
        error: (error) => {
            Swal.close();
            this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
        },
    });
}
updateDocument() {
    this.showLoader();
    this.admin.post(`fleets/document/${this.type.id}/update`, this.type).subscribe({
        next: (res) => {
            let response: any = res;
            this.noty.success(response.message);
            Swal.close();
        },
        error: (error) => {
            Swal.close();
            this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
        },
    });

}
deleteDocument() {
    let _this = this;
    Swal.fire({
        title: "Are you sure want to delete?",
        showCancelButton: true,
        confirmButtonText: 'Yes',
    }).then((result) => {
        if (result.value) {
            this.showLoader();
            this.admin.post(`fleets/document/${this.type.id}/delete`, this.type).subscribe({
                next: (res) => {
                    let response: any = res;
                    this.noty.success(response.message);
                    Swal.close();
                    this.document_nodes = this.document_nodes.filter(obj => obj.id !== _this.type.id);
                    this.type = { name: '' };
                },
                error: (error) => {
                    Swal.close();
                    this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
                },
            });
        }
    });

}

}
