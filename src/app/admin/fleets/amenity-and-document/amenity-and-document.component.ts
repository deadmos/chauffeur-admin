import { Component, OnInit } from '@angular/core';
import { MENUS } from '../fleets-menu';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../../admin.service';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { NotyService } from 'app/admin/services/noty.service';

@Component({
    selector: 'ngx-amenity-and-document',
    templateUrl: './amenity-and-document.component.html',
    styleUrls: ['./amenity-and-document.component.scss'],
})
export class AmenityAndDocumentComponent implements OnInit {
    menus = MENUS;
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Fleet Amenity',
            icon: 'fa fa-list',
        },
    ];
    amenity_nodes = [];
    document_nodes = [];
    type: any = [];

    constructor(private http: HttpClient, private noty: NotyService, private admin: AdminService) {
    }

    ngOnInit(): void {
        this.http
            .get(
                this.admin.getFullUrl('fleets/amenity'),
                { headers: this.admin.getHeader() },
            )
            .subscribe((data) => {
                this.amenity_nodes = data['amenityData'];
            });
    }
    toggle(type) {
    }
    showType(node: any) {
        this.type = node ? node : { name: '' };
    }
    showLoader() {
        Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading();
            },
        });
    }
    addAmenity() {
        this.showLoader();
        this.admin.post(`fleets/amenity/add`, this.type).subscribe({
            next: (res) => {
                let response: any = res;
                this.noty.success(response.message);
                Swal.close();
                this.type = response.data;
                this.amenity_nodes.push(response.data);
            },
            error: (error) => {
                Swal.close();
                this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
            },
        });
    }

    updateAmenity() {
        this.showLoader();
        this.admin.post(`fleets/amenity/${this.type.id}/update`, this.type).subscribe({
            next: (res) => {
                let response: any = res;
                this.noty.success(response.message);
                Swal.close();
            },
            error: (error) => {
                Swal.close();
                this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
            },
        });

    }
    
    deleteAmenity() {
        let _this = this;
        Swal.fire({
            title: "Are you sure want to delete?",
            showCancelButton: true,
            confirmButtonText: 'Yes',
        }).then((result) => {
            if (result.value) {
                this.showLoader();
                this.admin.post(`fleets/amenity/${this.type.id}/delete`, this.type).subscribe({
                    next: (res) => {
                        let response: any = res;
                        this.noty.success(response.message);
                        Swal.close();
                        this.amenity_nodes = this.amenity_nodes.filter(obj => obj.id !== _this.type.id);
                        this.type = { name: '' };
                    },
                    error: (error) => {
                        Swal.close();
                        this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
                    },
                });
            }
        });

    }
}
