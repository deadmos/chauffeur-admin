import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FleetsRoutingModule } from './fleets-routing.module';
import { FleetsComponent } from './fleets.component';
import { AdminThemeModule } from '../../@themes/admin/admin-theme.module';
import { AgGridModule } from 'ag-grid-angular';
import { AdminModule } from '../admin.module';
import { ClassTypesComponent } from './class-types/class-types.component';
import { AmenityAndDocumentComponent } from './amenity-and-document/amenity-and-document.component';
import { MakersComponent } from './makers/makers.component';
import { FleetModelsComponent } from './fleet-models/fleet-models.component';
import { CreateEditComponent } from './makers/create-edit/create-edit.component';
import { CreateEditComponent as FleetCreateEditComponent } from './create-edit/create-edit.component';
import {NbButtonModule, NbCardModule, NbCheckboxModule, NbFormFieldModule, NbInputModule, NbSelectModule} from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { DocumentComponent } from './document/document.component';


@NgModule({
    declarations: [FleetsComponent, ClassTypesComponent, AmenityAndDocumentComponent, MakersComponent, FleetModelsComponent, CreateEditComponent, DocumentComponent, FleetCreateEditComponent],
  imports: [
    CommonModule,
    FleetsRoutingModule,
    AdminThemeModule,
    AgGridModule,
    AdminModule,
    NbCardModule,
    NbCheckboxModule,
    NbButtonModule,
    NbFormFieldModule,
    NbInputModule,
    FormsModule,
    NbSelectModule
  ],
})
export class FleetsModule { }
