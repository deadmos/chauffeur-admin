

export const MENUS = [
  {
    id: 'fleet',
    title: 'fleet Manager',
    icon: 'fa fa-bars',
    link: '/fleets',
  },
  {
    id: 'class',
    title: ' Fleet Class/Type',
    icon: 'fa fa-file',
    link: '/fleets/class-types',
  },
  {
    id: 'amenity',
    title: ' Amenities',
    icon: 'fa fa-list',
    link: '/fleets/amenity',
  },
  {
    id: 'document',
    title: ' Documents',
    icon: 'fa fa-file-alt',
    link: '/fleets/document',
  },
];
