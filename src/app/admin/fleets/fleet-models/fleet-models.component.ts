import { Component, OnInit } from '@angular/core';
import {MENUS} from '../fleets-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {Router} from '@angular/router';
import {ActionsComponent} from '../../common/actions/actions.component';

@Component({
  selector: 'ngx-fleet-models',
  templateUrl: './fleet-models.component.html',
  styleUrls: ['./fleet-models.component.scss'],
})
export class FleetModelsComponent implements OnInit {
  private gridApi;
  private gridColumnApi;

  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Fleet Model List',
      icon: 'fa fa-car',
    },
  ];
  headerData = {
    title: 'Fleet Model List',
    action: true,
    actionLink: '/fleets/create',
    actionTitle: 'Add New Fleet Model',
  };
  constructor(private http: HttpClient, private admin: AdminService, private router: Router) {
    this.columnDefs = [
      { field: 'id', headerName: 'Fleet Model Id', sortable: false, filter: false , maxWidth: 100,  minWidth: 60},
      {
        field: 'name',
        headerName: 'Fleet Model Name',
        sortable: true,
        filter: true,
        maxWidth: 200,
        minWidth: 100,
        filterParams: {
          filterOptions: ['contains', 'equals'],
          defaultOption: 'contains',
          suppressAndOrCondition : true,
        },
      },
      {
        field: 'maker.name',
        headerName: 'Model Name',
        sortable: false,
        filter: false ,
        maxWidth: 150,
        minWidth: 60},
      {
        field: 'fleet_class.name',
        headerName: 'Class',
        sortable: false,
        filter: false , maxWidth: 150,  minWidth: 60},
      {
        field: 'type.type',
        headerName: 'Type',
        sortable: false,
        filter: false ,
        maxWidth: 150,
        minWidth: 60},
      {
        field: 'luggage_capacity',
        headerName: 'Luggage',
        sortable: true, filter: false,
        maxWidth: 150,
      },
      {
        field: 'luggage_capacity',
        headerName: 'Passenger',
        sortable: true, filter: false,
        maxWidth: 150,
      },
      {
        field: 'child_seat_capacity',
        headerName: 'Child Seat',
        sortable: true, filter: false,
        maxWidth: 150,
      },
      { field: 'action', sortable: true, filter: false ,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'Edit Page',
            href: '/fleets/edit',
            type: 'edit',
          },
          {
            title: 'Delete Page',
            href: 'fleets/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete?',
              successMessage: 'Page deleted successfully',
            },
          },
        ],
      },
    ];
    this.defaultColDef = { resizable: true };
    this.rowSelection = 'multiple';
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }
  onGridSizeChanged(params) {
    const gridWidth = document.getElementById('grid-wrapper').offsetWidth;
    const columnsToShow = [];
    const columnsToHide = [];
    let totalColsWidth = 0;
    const allColumns = params.columnApi.getAllColumns();
    for (let i = 0; i < allColumns.length; i++) {
      const column = allColumns[i];
      totalColsWidth += column.getMinWidth();
      if (totalColsWidth > gridWidth) {
        columnsToHide.push(column.colId);
      } else {
        columnsToShow.push(column.colId);
      }
    }
    params.columnApi.setColumnsVisible(columnsToShow, true);
    params.columnApi.setColumnsVisible(columnsToHide, false);
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {

    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    const pageListUrl = this.admin.getFullUrl('fleet-models');
    const headers = this.admin.getHeader();
    this.http
      .get(
        pageListUrl,
        { headers: headers },
      )
      .subscribe((data) => {
        this.rowData = data;
      });
  }

  ngOnInit(): void {
  }

}
