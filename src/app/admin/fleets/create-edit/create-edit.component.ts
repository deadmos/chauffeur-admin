import { Component, OnInit } from '@angular/core';
import {AdminService} from '../../admin.service';

@Component({
  selector: 'ngx-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss'],
})
export class CreateEditComponent implements OnInit {

    breadcrumbs = [
        {
          title: 'Home',
          link: '/dashboard',
          icon: 'fa fa-home',
        },
        {
          title: 'Fleet List',
          link: '/fleets',
          icon: 'fa fa-car',
        },
        {
          title: 'Create',
          icon: 'fa fa-plus',
        },
      ];
      headerData = {
        title: 'Fleet Add New',
      };
      fleets: any = {};
      fleet: any = {};
      selectedType: any = {};
      selectedClass: any = {};
  responseDataFleets: any = {};
  responseDataFleet: any = {};
  currentAmenities: any = [];
  constructor(
    private admin: AdminService) { }

  fleetTypeChange(fleet_type_id: any) {
    this.selectedType = this.responseDataFleets.typeData.find(function (item, index) {
        return item.id === fleet_type_id;
    });
}

fleetClassChange(fleet_class_id: any) {
    this.selectedClass = this.responseDataFleets.classData.find(function (item, index) {
        return item.id === fleet_class_id;
    });
}

  ngOnInit(): void {
    const  _this = this;
    this.admin.get('fleets/class-type').subscribe((res: any) => {
      _this.responseDataFleets = res;
    });
  }

  submitForm(FleetForm): void {

  }

  submitFleetDetailForm(data: any): void {

  }

  toggle(type) {
    this.currentAmenities[type] = this.currentAmenities[type] === 1 ? 0 : 1;
  }
}
