import { Component, OnInit, TemplateRef } from '@angular/core';
import { MENUS } from '../fleets-menu';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../../admin.service';
import { NbDialogService } from '@nebular/theme';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NotyService } from 'app/admin/services/noty.service';

@Component({
    selector: 'ngx-class-types',
    templateUrl: './class-types.component.html',
    styleUrls: ['./class-types.component.scss'],
})
export class ClassTypesComponent implements OnInit {
    menus = MENUS;
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Fleet Class and Type List',
            icon: 'fa fa-car',
        },
    ];
    class_nodes = [];
    type_nodes = [];
    currentClass: any = [];
    currentType: any = [];
    dialogRef: any;
    dialogRef1: any;

    constructor(
      private dialogService: NbDialogService,
      private noty: NotyService,
      private http: HttpClient,
      private admin: AdminService) {
    }

    ngOnInit(): void {
        this.getClassType();
    }
    getClassType() {
        this.http.get(this.admin.getFullUrl('fleets/class-type'), { headers: this.admin.getHeader() })
            .subscribe((data) => {
                this.class_nodes = data['classData'];
                this.type_nodes = data['typeData'];
            });
    }

    showLoader() {
        Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading();
            },
        });
    }

  hideLoader() {
    Swal.close();
  }


  openClass(dialog: TemplateRef<any>, data: any = []) {
        this.currentClass = data;
        this.dialogRef = this.dialogService.open(dialog);
    }
    openType(dialog: TemplateRef<any>, data: any = []) {
        this.currentType = data;
        this.dialogRef1 = this.dialogService.open(dialog);
    }

    submitFleetClassForm(data: any) {
        let url = 'fleets/class/add';
        if (this.currentClass.id !== undefined) {
            url = `fleets/class/${this.currentClass.id}/update`;
        }
        this.showLoader();
        this.admin.post(url, data.value).subscribe({
            next: (res) => {
                const response: any = res;
                this.dialogRef.close();
                this.noty.success(response.message);
                Swal.close();
                if (this.currentClass.id === undefined) {
                    this.class_nodes.push(response.data);
                }
            },
            error: (error) => {
                Swal.close();
                this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
            },
        });
    }
    submitFleetTypeForm(data: any) {
        let url = 'fleets/type/add';
        if (this.currentType.id !== undefined) {
            url = `fleets/type/${this.currentType.id}/update`;
        }
        this.showLoader();
        this.admin.post(url, data.value).subscribe({
            next: (res) => {
                const response: any = res;
                this.dialogRef1.close();
                this.noty.success(response.message);
                Swal.close();
                if (this.currentType.id === undefined) {
                    this.type_nodes.push(response.data);
                }
            },
            error: (error) => {
                Swal.close();
                this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
            },
        });
    }
    delete(isClass = false) {
      const _this = this;
      Swal.fire({
        title: 'Delete?',
        showCancelButton: true,
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          let url = ``;
          if (isClass) {
            if (_this.currentClass.id !== undefined) {
              url = `fleets/class/${_this.currentClass.id}/delete`;
            }
          } else {
            if (_this.currentType.id !== undefined) {
              url = `fleets/type/${_this.currentType.id}/delete`;
            }
          }
          this.admin.post(url, {}).subscribe({
            next: (res) => {
              const response: any = res;
              _this.noty.success(response.message);
              Swal.close();
              if (isClass) {
                _this.dialogRef.close();
                if (_this.currentClass.id !== undefined) {
                  _this.class_nodes = _this.class_nodes.filter(item => item.id !== _this.currentClass.id);
                }
              } else {
                _this.dialogRef1.close();
                if (_this.currentType.id !== undefined) {
                  _this.type_nodes = _this.type_nodes.filter(item => item.id !== _this.currentType.id);
                }
              }
            },
            error: (error) => {
              Swal.close();
              _this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
            },
          });
          }
        });
    }
}
