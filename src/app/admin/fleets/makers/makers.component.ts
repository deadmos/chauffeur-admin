import { Component, OnInit } from '@angular/core';
import {MENUS} from '../fleets-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {Router} from '@angular/router';
import {ActionsComponent} from '../../common/actions/actions.component';

@Component({
  selector: 'ngx-makers',
  templateUrl: './makers.component.html',
  styleUrls: ['./makers.component.scss'],
})
export class MakersComponent implements OnInit {
  private gridApi;
  private gridColumnApi;

  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Fleet Maker List',
      icon: 'fa fa-building',
    },
  ];
  headerData = {
    title: 'Fleet Maker List',
    action: true,
    actionLink: '/fleets/makers/create',
    actionTitle: 'Add New Fleet Maker',
  };
  constructor(private http: HttpClient, private admin: AdminService, private router: Router) {
    this.columnDefs = [
      { field: 'id', headerName: 'Maker Id', sortable: false, filter: false , maxWidth: 100,  minWidth: 60},
      {
        field: 'name',
        headerName: 'Fleet Maker Name',
        sortable: true,
        filter: true,
        maxWidth: 200,
        minWidth: 100,
        filterParams: {
          filterOptions: ['contains', 'equals'],
          defaultOption: 'contains',
          suppressAndOrCondition : true,
        },
      },
      { field: 'action', sortable: true, filter: false ,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'Edit Maker',
            href: '/fleets/makers/edit',
            type: 'edit',
          },
          {
            title: 'Delete Maker',
            href: 'fleets/fleet-makers/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete?',
              successMessage: 'Page deleted successfully',
            },
          },
        ],
      },
    ];
    this.defaultColDef = { resizable: true };
    this.rowSelection = 'multiple';
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }
  onGridSizeChanged(params) {
    const gridWidth = document.getElementById('grid-wrapper').offsetWidth;
    const columnsToShow = [];
    const columnsToHide = [];
    let totalColsWidth = 0;
    const allColumns = params.columnApi.getAllColumns();
    for (let i = 0; i < allColumns.length; i++) {
      const column = allColumns[i];
      totalColsWidth += column.getMinWidth();
      if (totalColsWidth > gridWidth) {
        columnsToHide.push(column.colId);
      } else {
        columnsToShow.push(column.colId);
      }
    }
    params.columnApi.setColumnsVisible(columnsToShow, true);
    params.columnApi.setColumnsVisible(columnsToHide, false);
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {

    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    const pageListUrl = this.admin.getFullUrl('fleet-makers');
    const headers = this.admin.getHeader();
    this.http
      .get(
        pageListUrl,
        { headers: headers },
      )
      .subscribe((data) => {
        this.rowData = data;
      });
  }

  ngOnInit(): void {
  }
}
