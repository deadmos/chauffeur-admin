import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FleetsComponent} from './fleets.component';
import {ClassTypesComponent} from './class-types/class-types.component';
import {AmenityAndDocumentComponent} from './amenity-and-document/amenity-and-document.component';
import {MakersComponent} from './makers/makers.component';
import {FleetModelsComponent} from './fleet-models/fleet-models.component';
import {CreateEditComponent as MakerCreateEditComponent} from './makers/create-edit/create-edit.component';
import { DocumentComponent } from './document/document.component';
import { FleetDetailComponent } from '../drivers/fleet-detail/fleet-detail.component';
import { CreateEditComponent } from './create-edit/create-edit.component';

const routes: Routes = [{
  path: '',
  component: FleetsComponent,
  },
  {
    path: ':id/edit',
    component: FleetDetailComponent,
  },
  {
    path: 'create',
    component: CreateEditComponent,
  },
  {
    path: 'class-types',
    component: ClassTypesComponent,
  },
  {
    path: 'makers',
    component: MakersComponent,
  },
  {
    path: 'makers/create',
    component: MakerCreateEditComponent,
  },
  {
    path: 'makers/edit/:id',
    component: MakerCreateEditComponent,
  },
  {
    path: 'models',
    component: FleetModelsComponent,
  },
  {
    path: 'amenity',
    component: AmenityAndDocumentComponent,
  },
  {
    path: 'document',
    component: DocumentComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FleetsRoutingModule { }
