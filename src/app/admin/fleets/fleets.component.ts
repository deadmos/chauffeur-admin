import { Component, OnInit } from '@angular/core';
import { MENUS } from './fleets-menu';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';
import { ActionsComponent } from '../common/actions/actions.component';
import {AgDatatable} from '../admin.extends';
import {NbThemeService} from '@nebular/theme';
import {ToggleComponent} from '../common/toggle/toggle.component';

@Component({
    selector: 'ngx-fleets',
    templateUrl: './fleets.component.html',
    styleUrls: ['./fleets.component.scss'],
})
export class FleetsComponent extends AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  url = 'fleets';
  menus = MENUS;

  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Fleet List',
      icon: 'fa fa-car',
    },
  ];
  headerData = {
    title: 'Fleet List',
    action: false,
    actionLink: '/fleets/create',
    actionTitle: 'Add New Fleet',
  };
  constructor(
    protected http: HttpClient,
    protected admin: AdminService,
    protected router: Router,
    protected themeService: NbThemeService,
  ) {
    super();
    this.columnDefs = [
      {
        field: 'globalSearch',
        hide: true,
        initialHide: true,
        filter: 'agTextColumnFilter',
        filterParams: {
          newRowsAction: 'keep',
        },
      },
      {field: 'id', headerName: 'Id', sortable: false, filter: false, maxWidth: 80, minWidth: 50},
      {
        field: 'make_name', headerName: 'Maker', sortable: true, filter: true, maxWidth: 150, minWidth: 60,
        filterParams: this.filterParams,
      },
      {
        field: 'model_name', headerName: 'Model', sortable: true, filter: true, maxWidth: 150, minWidth: 60,
        filterParams: this.filterParams,
      },
      {
        field: 'fleet_class.name',
        searchField: 'fleet_classes.name',
        headerName: 'Class',
        sortable: true,
        filter: true,
        maxWidth: 200,
        minWidth: 150,
        filterParams: this.filterParams,
      },
      {
        field: 'type.type',
        searchField: 'fleet_types.type',
        headerName: 'Type',
        sortable: true,
        filter: true,
        maxWidth: 200,
        minWidth: 150,
        filterParams: this.filterParams,
      },
      {
        field: 'number',
        searchField: 'fleets.number',
        headerName: 'Vehicle No.',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'driver.user.name',
        searchField: 'users.name',
        headerName: 'Driver',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: '', headerName: 'Capacity', sortable: false, filter: false, maxWidth: 300, minWidth: 250,
        valueFormatter: params => params.data !== undefined ? this.capacityFormatter(params.data) : '',
      },
      {
        field: 'status', sortable: true, filter: false, maxWidth: 120,
        cellRendererFramework: ToggleComponent,
        cellRendererParams: {name: 'is_active', href: 'fleets/__placeholder__/change-status'},
      },
      {
        field: 'action', sortable: true, filter: false,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'Edit Fleet',
            href: '/fleets/:id/edit',
            hrefFormat: true,
            type: 'setting',
          },
          // {
          //     title: 'Delete Page',
          //     href: 'fleets/',
          //     type: 'delete',
          //     confirm: {
          //         title: 'Are you sure want to delete?',
          //         successMessage: 'Fleet deleted successfully',
          //     },
          // },
        ],
      },
    ];
    this.defaultColDef = {resizable: true};
    this.rowSelection = 'multiple';
    }

    capacityFormatter(data: any) {
        return `Pax: ${data.type.passenger_count} |  Lug: ${data.type.luggage_count} |  Suit: ${data.type.suitcase_count} |  Child: ${data.available_child_seats}`;
    }


    ngOnInit(): void {
    }

}
