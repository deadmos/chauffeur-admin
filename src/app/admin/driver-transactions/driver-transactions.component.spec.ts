import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverTransactionsComponent } from './driver-transactions.component';

describe('DriverTransactionsComponent', () => {
  let component: DriverTransactionsComponent;
  let fixture: ComponentFixture<DriverTransactionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverTransactionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
