import {Component, OnInit} from '@angular/core';
import {AgDatatable} from '../admin.extends';
import {MENUS} from '../transactions/transactions-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NbThemeService} from '@nebular/theme';

@Component({
  selector: 'ngx-driver-transactions',
  templateUrl: './driver-transactions.component.html',
  styleUrls: ['./driver-transactions.component.scss'],
})
export class DriverTransactionsComponent extends AgDatatable implements OnInit {

  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  url = 'driver-transactions';
  id: any;
  name: any;
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Transaction List',
      icon: 'fas fa-exchange-alt',
    },
  ];

  headerData = {
    title: 'Driver Transactions',
    action: false,
  };


  constructor(
    protected http: HttpClient,
    protected admin: AdminService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected themeService: NbThemeService,
  ) {
    super();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params.id;
      if (this.router.url.includes('invoices') && this.id) {
        this.url = `invoices/${this.id}/transactions`;
      } else if (this.router.url.includes('drivers') && this.id) {
        this.url = `driver/${this.id}/transactions`;
      }
    });
    this.columnDefs = [
      { field: 'id', headerName: 'Id', sortable: true, filter: false, maxWidth: 100, minWidth: 60 },
      {
        field: 'id',
        headerName: 'Transaction ID',
        sortable: true,
        filter: false,
        valueFormatter: params => params.data !== undefined ?  '#DTR-' + params.data.id : '',
      },
      { field: 'transfer_id', headerName: 'Stripe Transfer ID', sortable: true, filter: false },
      {
        field: 'driver.user.name',
        headerName: 'Driver',
        sortable: true,
        filter: true,
        filterParams: {
          filterOptions: ['contains', 'equals'],
          defaultOption: 'contains',
          suppressAndOrCondition: true,
        },
      },
      {
        field: 'invoice.reference_id',
        headerName: 'Invoice',
        sortable: true,
        filter: true,
        filterParams: {
          filterOptions: ['contains', 'equals'],
          defaultOption: 'contains',
          suppressAndOrCondition: true,
        },
      },
      {
        field: 'amount',
        headerName: 'Full Amount',
        valueFormatter: params => params.data !== undefined ? this.currencyFormatter(params.data.amount, '$') : '',
        sortable: true,
        filter: false,
      },
      {
        field: 'primary_network_commission',
        headerName: 'Generate At',
        valueFormatter: params => params.data !== undefined ? params.data.created_at_formatted  : '',
        sortable: true,
        filter: false,
      },
    ];
    this.defaultColDef = { resizable: true };
    this.rowSelection = 'multiple';
  }

  ngOnInit(): void {

  }
}
