import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'ngx-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent implements OnInit {
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Driver List',
      link: '/drivers',
      icon: 'fa fa-user-secret',
    },
    {
      title: 'Settings',
      icon: 'fa fa-cog',
    },
  ];
  headerData = {
    title: 'Driver Settings',
  };
  driver = {
    invoice_raised_type: 'daily',
    primary_commission: 5,
    invoice_weekday: '',
    invoice_day: '',
    name_of_the_bank:'',
    name_on_the_account:'',
    account_number:'',
    swift_code:'',
    bank_address:''
  };
  submitting = false;
  bank_submitting = false;
  verified = false;
  constructor(    private http: HttpClient,
                  private admin: AdminService,
                  private route: ActivatedRoute) {
  }

  ngOnInit(): void {

    this.http
      .get(
        this.admin.getFullUrl(`driver/` + this.route.snapshot.paramMap.get('id') ),
        {headers: this.admin.getHeader()},
      )
      .subscribe((driver: any) => {
        this.driver = driver.data.driver;
      });
  }
  notValid(name) {
    return this.admin.notValid(name);
  }

  valid(name) {
    return this.admin.valid(name);
  }
  submitForm(settingForm): void {
    this.submitting = true;
    this.admin
      .post(`driver/setting/` + this.route.snapshot.paramMap.get('id'), settingForm.value)
      .subscribe((res: any) => {
        this.submitting = false;
        this.admin.noty('Successfully Updated Driver Settings.');
    });
  }
  submitBankForm(bankSettingForm): void {
    this.bank_submitting = true;
    this.admin
      .post(`driver/bank-setting/` + this.route.snapshot.paramMap.get('id'), bankSettingForm.value)
      .subscribe((res: any) => {
        this.submitting = false;
        this.admin.noty('Successfully Updated Driver Bank Info.');
    });
  }

}
