import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverTermsComponent } from './driver-terms.component';

describe('DriverTermsComponent', () => {
  let component: DriverTermsComponent;
  let fixture: ComponentFixture<DriverTermsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverTermsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
