import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';
import { MENUS } from '../drivers-menu';

@Component({
  selector: 'ngx-driver-terms',
  templateUrl: './driver-terms.component.html',
  styleUrls: ['./driver-terms.component.scss']
})
export class DriverTermsComponent implements OnInit {

    menus = MENUS;
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
      
          {
            title: 'Terms & Policy',
            icon: 'fa fa-file',
          }
    ];
    headerData = {
        title: 'Driver Terms & Policy',
        action: false,
    };
    term = {
        heading: '',
        detail: '',
    };
    heading = '<p>title</p>';
    contentLoaded = false;

    constructor(protected admin: AdminService, protected router: Router) { 
       
    }

    ngOnInit(): void {
        this.admin
        .get('driver-terms')
        .subscribe((data:any) => {
          this.term = data;
          this.heading = '<p>heading</p>';
          this.contentLoaded = true;
        });
    }

  setEditorValue(data,type){
      this.term[type] = data;
  }

  submitDriverForm(form){
    const that = this;
    this.admin.post('driver-terms',this.term).subscribe({
        next:function(res:any){
            that.admin.noty(res.message);
        },
        error:function(error){
            //that.submitting = false;

            switch (error.status) {
              case 422:
                    that.admin.noty('Validation Error', true);
                    that.admin.showValidationError(form, error.error);
                break;
              default :
                  that.admin.noty('Server Error', true);
                break;
            }
        }
    })
  }


}
