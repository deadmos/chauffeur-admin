import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DriversComponent} from './drivers.component';
import {DocumentsComponent} from './documents/documents.component';
import {EditComponent} from './edit/edit.component';
import {FleetDetailComponent} from './fleet-detail/fleet-detail.component';
import {TransactionsComponent} from '../transactions/transactions.component';
import {InvoicesComponent} from '../invoices/invoices.component';
import {StatementsComponent} from '../statements/statements.component';
import {SettingComponent} from './setting/setting.component';
import { ChatComponent } from './chat/chat.component';
import { DriverCreateComponent } from './driver-create/driver-create.component';
import { DriverTermsComponent } from './driver-terms/driver-terms.component';

const routes: Routes = [
  {
    path: '',
    component: DriversComponent,
  },
  {
    path: 'document-types',
    component: DocumentsComponent,
  },
  {
    path: 'create',
    component: DriverCreateComponent,
  },
  {
    path: 'terms',
    component: DriverTermsComponent,
  },
  {
    path: 'edit/:id',
    component: EditComponent,
  },
  {
    path: 'setting/:id',
    component: SettingComponent,
  },
  {
    path: 'message/:id',
    component: ChatComponent,
  },
  {
    path: ':id/fleet-detail',
    component: FleetDetailComponent,
  },
  {
    path: ':id/transactions',
    component: TransactionsComponent,
  },
  {
    path: ':id/invoices',
    component: InvoicesComponent,
  },
  {
    path: ':id/statements',
    component: StatementsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DriversRoutingModule {
}
