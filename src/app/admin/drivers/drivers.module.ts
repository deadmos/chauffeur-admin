import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DriversRoutingModule } from './drivers-routing.module';
import { DriversComponent } from './drivers.component';
import {
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbFormFieldModule,
  NbInputModule,
  NbListModule,
  NbSelectModule,
  NbToggleModule,
  NbDatepickerModule,
  NbIconModule,
  NbChatModule} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import {AdminModule} from '../admin.module';
import {AgGridModule} from 'ag-grid-angular';
import { DocumentsComponent } from './documents/documents.component';
import { EditComponent } from './edit/edit.component';
import {FormsModule} from '@angular/forms';
import {NbMomentDateModule } from '@nebular/moment';
import { FleetDetailComponent } from './fleet-detail/fleet-detail.component';
import { SettingComponent } from './setting/setting.component';
import { ChatComponent } from './chat/chat.component';
import { DriverCreateComponent } from './driver-create/driver-create.component';
import { DriverTermsComponent } from './driver-terms/driver-terms.component';
import {MyDatePickerModule} from 'mydatepicker';
import {Ng2TelInputModule} from 'ng2-tel-input';

@NgModule({
  declarations: [
    DriversComponent,
    DocumentsComponent,
    EditComponent,
    FleetDetailComponent,
    SettingComponent,
    ChatComponent,
    DriverCreateComponent,
    DriverTermsComponent,
  ],
  imports: [
    CommonModule,
    DriversRoutingModule,
    AdminThemeModule,
    AdminModule,
    AgGridModule,
    NbCardModule,
    NbSelectModule,
    NbToggleModule,
    NbDatepickerModule,
    NbMomentDateModule,
    NbCheckboxModule,
    NbButtonModule,
    NbFormFieldModule,
    NbInputModule,
    NbListModule,
    FormsModule,
    NbIconModule,
    NbChatModule,
    MyDatePickerModule,
    Ng2TelInputModule,
  ],
})
export class DriversModule { }
