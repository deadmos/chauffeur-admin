import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';

@Component({
  selector: 'ngx-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {

    breadcrumbs = [
        {
          title: 'Home',
          link: '/dashboard',
          icon: 'fa fa-home',
        },
        {
          title: 'Driver List',
          link: '/drivers',
          icon: 'fa fa-user-secret',
        },
        {
          title: 'Messages',
          icon: 'fa fa-envelope',
        },
      ];
      headerData = {
        title: 'Driver Messages',
      };

    messages: any[];

    constructor(protected admin: AdminService,private route: ActivatedRoute) {
    
    }
  
    sendMessage(event: any) {
 
        this.admin.post('driver-messages/'+ this.route.snapshot.paramMap.get('id'),{'message' :event.message}).subscribe((res:any) => {
            this.messages.push(res.data);
          });
     
    }

    ngOnInit(): void {
        this.admin.get('driver-messages/'+ this.route.snapshot.paramMap.get('id')).subscribe((res:any) => {
            this.messages  = res.data;
          });
    }

}
