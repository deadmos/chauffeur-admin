import {Component, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NbDateService, NbDialogService} from '@nebular/theme';
import {AdminService} from 'app/admin/admin.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DatePipe} from '@angular/common';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {NotyService} from 'app/admin/services/noty.service';
import {HttpClient} from '@angular/common/http';
import { Moment} from 'moment';

@Component({
    selector: 'ngx-fleet-detail',
    templateUrl: './fleet-detail.component.html',
    styleUrls: ['./fleet-detail.component.scss'],
})
export class FleetDetailComponent implements OnInit {
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Driver List',
            link: '/drivers',
            icon: 'fa fa-user-secret',
        },
        {
            title: 'Fleet Detail',
            icon: 'fa fa-car',
        },
    ];
    driverId: any;
    dialogRef: any;
    fleet: any;
    responseDataFleets: any = {};
    responseDataFleet: any = {};
    selectedType: any;
    selectedClass: any;
    isDriverEdit: boolean = true;
    min: Moment;
    currentAmenities: any = [];
    uploadDocForm: FormGroup;
    constructor(
        private dialogService: NbDialogService,
        private admin: AdminService,
        private noty: NotyService,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private datePipe: DatePipe,
        private httpClient: HttpClient,
        private router: Router,
    ) {
        this.activatedRoute.params.subscribe((params: Params) => { this.driverId = params.id; });
        this.reload();
    }

    ngOnInit(): void {
        this.uploadDocForm = this.formBuilder.group({
            file: [''],
            extra_file: [''],
        });
    }

    reload() {
      if (this.router.url.includes('fleets')) {
        this.isDriverEdit = false;
        this.getFleet();
      } else {
        this.isDriverEdit = true;
        this.getDriverFleets();
      }
    }
    showLoader() {
        Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading();
            },
        });
    }

    convertDate(date: any) {
        return (new DatePipe('en-Us').transform(new Date(date), 'yyyy-MM-dd'));
    }
    onFileSelect(event, varName) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.uploadDocForm.get(varName).setValue(file);
        }
    }

    getFleet() {
        this.admin.get(`fleets/${this.driverId}`).subscribe((res) => {
            const response: any = res;
            this.responseDataFleets = response.data;
        });
    }
    getDriverFleets() {
        this.admin.get(`driver/${this.driverId}/fleets`).subscribe((res) => {
            const response: any = res;
            this.responseDataFleets = response.data;
        });
    }
    open(dialog: TemplateRef<any>, fleet: any) {

        this.responseDataFleet = fleet;
        this.responseDataFleet.amenities.forEach(element => {
            this.currentAmenities[element.id] = 1;
        });

        this.selectedType = this.responseDataFleets.types.filter(function (item, index) {
            return item.id == fleet.fleet_type_id;
        })[0];
        this.selectedClass = this.responseDataFleets.classes.filter(function (item, index) {
            return item.id == fleet.fleet_class_id;
        })[0];
      this.dialogRef =  this.dialogService.open(dialog);
    }

    openNewDialog(dialog: TemplateRef<any>) {
        this.responseDataFleet = {};
        this.currentAmenities = [];
        this.selectedType = { id: 0 };
        this.selectedClass = { id: 0 };
        this.dialogRef = this.dialogService.open(dialog, { context: 'this is some additional data passed to dialog' });
    }

    toggle(type) {
        this.currentAmenities[type] = this.currentAmenities[type] === 1 ? 0 : 1;
    }

    fleetTypeChange(fleet_type_id: any) {
        this.selectedType = this.responseDataFleets.types.filter(function (item, index) {
            return item.id == fleet_type_id;
        })[0];
    }
    fleetClassChange(fleet_class_id: any) {
        this.selectedClass = this.responseDataFleets.classes.filter(function (item, index) {
            return item.id == fleet_class_id;
        })[0];
    }

    showForm(doc_type_id: any, fleet: any): void {
        fleet.doc_status = true;
        fleet.cur_doc = doc_type_id;
        fleet.doc_type_value = {};

        const docs = this.responseDataFleets.doc_types.filter(function (item, index) {
            return item.id == doc_type_id;
        });
        const fleet_docs = fleet.fleet_docs.filter(function (item, index) {
            return item.document_type_id == doc_type_id;
        });

        if (docs.length > 0) {
            const doc = docs[0];
            if (fleet_docs.length > 0) {
                const fleet_doc = fleet_docs[0];
                fleet.doc_type = doc;
                fleet.doc_type_value = fleet_doc;
                fleet.doc_status = true;
            } else {
                fleet.doc_type = doc;
                fleet.doc_status = true;
            }
        } else {
            fleet.doc_type = {};
        }
        this.fleet = fleet;
    }
    submitDocumentForm(data: any, fleet: any): void {
        const formData = new FormData();
        if (this.uploadDocForm.get('file').value) {
            formData.append('file', this.uploadDocForm.get('file').value);
        }
        if (this.uploadDocForm.get('extra_file').value) {
            formData.append('extra_file', this.uploadDocForm.get('extra_file').value);
        }

        for (const index in data.value) {
            if (index == 'expire_date') {
                data.value[index] = this.convertDate(data.value[index]);
            }
            formData.append(index, data.value[index]);
        }

        const postUrl = this.admin.getFullUrl(`driver/fleets/${fleet.id}/document`);
        this.showLoader();
        this.httpClient.post<any>(postUrl, formData, { headers: this.admin.getHeader(true) }).subscribe({
            next: (res) => {
                const response = res;
                fleet.doc_type_value = res.data;
                Swal.close();
                this.noty.success(response.message);
            },
            error: (error) => {
                Swal.close();
                this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
            },
        });
    }

    submitFleetDetailForm(data: any): void {
        const formData = new FormData();

        for (const index in data.value) {
                formData.append(index, data.value[index]);
        }
        this.currentAmenities.forEach(function(element, key) {
            if (element) {
                formData.append('amenities[]', key);
            }
        });
        formData.append('fleet_type_id', this.selectedType.id);
        formData.append('fleet_class_id', this.selectedClass.id);
        let postUrl;
        if (this.responseDataFleet.id) {
            postUrl = this.admin.getFullUrl(`driver/${this.driverId}/fleets/${this.responseDataFleet.id}/detail`);
        } else {
            postUrl = this.admin.getFullUrl(`driver/${this.driverId}/fleets/detail/store`);
        }
        this.showLoader();
        this.httpClient.post<any>(postUrl, formData, { headers: this.admin.getHeader(true) }).subscribe({
            next: (response: any) => {
                this.noty.success(response.message);
                Swal.close();
              this.dialogRef.close();
                this.reload();
            },
            error: (error) => {
                Swal.close();
                this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
            },
        });
    }

    deleteFleet(fleet: any) {
        Swal.fire({
            title: 'Are you sure want to delete?',
            showCancelButton: true,
            confirmButtonText: 'Yes',
        }).then((result) => {
            const _this = this;
            if (result.value) {
                this.admin.get(`driver/fleets/${fleet.id}/delete`).subscribe({
                    next: (response: any) => {
                        this.noty.success(response.message);
                        Swal.close();
                        this.reload();
                    },
                    error: (error) => {
                        Swal.close();
                        this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
                    },
                });
            }
        });

    }
    setActiveFleet(fleet: any) {
        this.admin.get(`driver/${this.driverId}/fleets/${fleet.id}/set-active`).subscribe({
            next: (response: any) => {
                this.noty.success(response.message);
                Swal.close();
                this.responseDataFleets.fleets.forEach(element => {
                    element.is_active = 0;
                });
                fleet.is_active = 1;
            },
            error: (error) => {
                Swal.close();
                this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
            },
        });
    }
  toggleClick(event, toggleName: string) {
    event.preventDefault();
    let title, message;
    if (this.responseDataFleets.driver[toggleName]) {
      title = 'Driver Dis-Approval';
      message =
        'Your some Fleet Info is incorrect.';
    } else {
      title = 'Driver Approval';
      message =
        'Your Fleet and Fleet\'s Document are verified.';
    }

    Swal.fire({
      title: title,
      inputLabel: 'Message',
      input: 'textarea',
      inputAttributes: {
        autocapitalize: 'off',
      },
      showCancelButton: true,
      showDenyButton: true,
      confirmButtonText: 'Notify',
      confirmButtonColor: '#00a65a',
      denyButtonText: `Don't Notify`,
      denyButtonColor: '#f39c12',
      didOpen: function () {
        Swal.getPopup().querySelector('#swal2-input').value = message;
      },
    }).then((result) => {

      const requestData = {
        notify: result.isConfirmed ? true : false,
        type: 'fleet',
        status: !this.responseDataFleets.driver[toggleName],
        message: Swal.getPopup().querySelector('#swal2-input').value,
      };
      if (result.isConfirmed || result.isDenied) {
        this.showLoader();
        this.admin.post(`driver/approve/${this.driverId}`, requestData).subscribe({
          next: (response) => {
            Swal.close();
            this.responseDataFleets.driver[toggleName] = this.responseDataFleets.driver[toggleName] === 1 ? 0 : 1;
            this.noty.success('Driver Fleet Approval');
          },
          error: (error) => {
            Swal.close();
            this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
          },
        });

      }
    });
  }

  getCheckedStatus(toggleName: string) {
    return this.responseDataFleets.driver[toggleName] === 1;
  }

}
