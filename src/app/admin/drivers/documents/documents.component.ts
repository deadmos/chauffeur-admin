import { Component, OnInit } from '@angular/core';
import { MENUS } from '../drivers-menu';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../../admin.service';
import Swal from "sweetalert2/dist/sweetalert2.js";
import { NotyService } from 'app/admin/services/noty.service';

@Component({
    selector: 'ngx-documents',
    templateUrl: './documents.component.html',
    styleUrls: ['./documents.component.scss'],
})
export class DocumentsComponent implements OnInit {

    menus = MENUS;
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Driver List',
            icon: 'fa fa-user-secret',
        },
    ];
    type_nodes: any = [];
    type: any = {};
    constructor(private http: HttpClient, private noty: NotyService, private admin: AdminService) {
    }

    ngOnInit(): void {
        this.http
            .get(
                this.admin.getFullUrl('driver-document-type'),
                { headers: this.admin.getHeader() },
            )
            .subscribe((data) => {
                this.type_nodes = data;
            });
    }

    toggle(type) {
        this.type[type] = this.type[type] === 1 ? 0 : 1;
    }
    showType(node) {
        this.type = node ? node : { name: '' };
        console.log(this.type);
    }

    showLoader() {
        Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading();
            },
        });
    }
    addDocument() {
        this.showLoader();
        this.admin.post(`driver/document/add`, this.type).subscribe({
            next: (res) => {
                let response: any = res;
                this.noty.success(response.message);
                Swal.close();
                this.type = response.data;
                this.type_nodes.push(response.data);
            },
            error: (error) => {
                Swal.close();
                this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
            },
        });
    }
    updateDocument() {
        this.showLoader();
        this.admin.post(`driver/document/${this.type.id}/update`, this.type).subscribe({
            next: (res) => {
                let response: any = res;
                this.noty.success(response.message);
                Swal.close();
            },
            error: (error) => {
                Swal.close();
                this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
            },
        });

    }
    deleteDocument() {
        let _this = this;
        Swal.fire({
            title: "Are you sure want to delete?",
            showCancelButton: true,
            confirmButtonText: 'Yes',
        }).then((result) => {
            if (result.value) {
                this.showLoader();
                this.admin.post(`driver/document/${this.type.id}/delete`, this.type).subscribe({
                    next: (res) => {
                        let response: any = res;
                        this.noty.success(response.message);
                        Swal.close();
                        this.type_nodes = this.type_nodes.filter(obj => obj.id !== _this.type.id);
                        this.type = { name: '' };
                    },
                    error: (error) => {
                        Swal.close();
                        this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
                    },
                });
            }
        });

    }
}
