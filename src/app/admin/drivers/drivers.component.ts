import { Component, OnInit } from '@angular/core';
import { MENUS } from './drivers-menu';
import { AdminService } from '../admin.service';
import { ActionsComponent } from '../common/actions/actions.component';
import {AgDatatable} from '../admin.extends';
import {NbThemeService} from '@nebular/theme';
import {ToggleComponent} from '../common/toggle/toggle.component';

@Component({
    selector: 'ngx-drivers',
    templateUrl: './drivers.component.html',
    styleUrls: ['./drivers.component.scss'],
})
export class DriversComponent extends AgDatatable implements OnInit {

    public columnDefs;
    public defaultColDef;
    public rowData: {};
    public rowSelection;
    menus = MENUS;
    url = 'drivers';
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Driver List',
            icon: 'fa fa-user-secret',
        },
    ];
    headerData = {
        title: 'Driver User List',
        action: true,
        actionLink: '/drivers/create',
        actionTitle: 'Add New Driver',
    };
    constructor(
      protected admin: AdminService,
      protected themeService: NbThemeService,
    ) {
      super();
        this.columnDefs = [
          {
            field: 'globalSearch',
            hide: true,
            initialHide: true,
            filter: 'agTextColumnFilter',
            filterParams: {
              newRowsAction: 'keep',
            },
          },
            { field: 'id', headerName: 'Id', sortable: true, filter: false, maxWidth: 100, minWidth: 60 },
            {
                field: 'user.name',
                searchField: 'users.name',
                headerName: 'Name',
                sortable: true,
                filter: true,
                filterParams: this.filterParams,
                minWidth: 60,
            },
            {
                field: 'user.email',
                searchField: 'users.email',
                headerName: 'Email',
                sortable: true,
                filter: true,
                filterParams: this.filterParams,
                cellStyle: {'white-space': 'normal'},
            },
            {
                field: 'user.mobile',
                searchField: 'users.mobile',
                headerName: 'Phones',
                sortable: true,
                filter: true,
                filterParams: this.filterParams,
              minWidth: 60,
            },
            {
                field: 'user.user_name',
                searchField: 'users.user_name',
                headerName: 'Username',
                sortable: true,
                filter: true,
                filterParams: this.filterParams,
              minWidth: 60,
            },
            {
                field: 'location_name',
                searchField: 'location_name',
                headerName: 'Location',
                sortable: true,
                filter: true,
                filterParams: this.filterParams,
              minWidth: 60,
            },
            {
                headerName: 'Work Status',
                children: [
                    { headerName: 'Pending', field: 'incomplete_movements', sortable: true, maxWidth: 60 },
                    { headerName: 'Completed', field: 'completed_movements', sortable: true, maxWidth: 60 },
                    { headerName: 'Cancelled', field: 'cancelled_movements', sortable: true, maxWidth: 60 },
                ],
                sortable: true,
                filter: false,
                maxWidth: 200,
            },
            { field: 'status', sortable: true, filter: false ,  maxWidth: 120,
              cellRendererFramework: ToggleComponent,
              cellRendererParams: {name: 'is_active' , href: 'drivers/__placeholder__/change-active-status'},
            },
            { field: 'approval', sortable: true, filter: false ,  maxWidth: 120,
              cellRendererFramework: ToggleComponent,
              cellRendererParams: {name: 'is_approved' , href: 'drivers/__placeholder__/change-approval', 'confirm' : {
                  title: 'Are you sure want to change Approval Status?',
                  successMessage: 'Approval Changed successfully',
                }},
            },
            {
                field: 'action', sortable: true, filter: false, minWidth: 60,
                cellRendererFramework: ActionsComponent,
                displayType: 'dropdown',
                cellRendererParams: [
                    {
                        title: 'View / Edit Driver',
                        href: '/drivers/edit',
                        type: 'edit',
                    },
                    {
                        title: 'Setting Driver',
                        href: '/drivers/setting',
                        type: 'settings-outline',
                    },
                    {
                        title: 'Message Driver',
                        href: '/drivers/message',
                        type: 'email-outline',
                    },
                    {
                        title: 'Fleet Detail',
                        href: '/drivers/:id/fleet-detail',
                        hrefFormat: true,
                        icon: 'car-outline',
                        type: 'fleet',
                    },
                    {
                        title: 'View Transaction',
                        href: '/drivers/:id/transactions',
                        hrefFormat: true,
                        type: 'flip-2-outline',
                    },
                    {
                        title: 'View Invoice',
                        href: '/drivers/:id/invoices',
                        hrefFormat: true,
                        type: 'file-text-outline',
                    },
                    {
                        title: 'View Statement',
                        href: '/drivers/:id/statements',
                        hrefFormat: true,
                        type: 'file-add-outline',
                    },
                    // {
                    //     title: 'Delete Driver',
                    //     href: 'drivers/',
                    //     type: 'delete',
                    //     confirm: {
                    //         title: 'Are you sure want to delete?',
                    //         successMessage: 'Page deleted successfully',
                    //     },
                    // },
                ],
            },
        ];
        this.defaultColDef = { resizable: true };
        this.rowSelection = 'multiple';
    }


    ngOnInit(): void {
    }
}
