import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';
import { MENUS } from '../drivers-menu';
@Component({
  selector: 'ngx-driver-create',
  templateUrl: './driver-create.component.html',
  styleUrls: ['./driver-create.component.scss'],
})
export class DriverCreateComponent implements OnInit {

    @ViewChild('searchInput') searchInput: ElementRef;
    @ViewChild('autoSearch') autoSearch;
    menus = MENUS;
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Driver List',
            link: '/drivers',
            icon: 'fa fa-user-secret',
          },
          {
            title: 'Create',
            icon: 'fa fa-plus',
          },
    ];
    headerData = {
        title: 'Add New Driver',
        action: true,
        actionLink: '/drivers/create',
        actionTitle: 'Add New Driver',
    };
  driver = {
    name: '',
    email: '',
    mobile: '',
    dob: '',
    profile_image_path: '',
    profile_image: '',
    service_radius: '',
    service_location: '',
    service_location_lat: '',
    service_location_lng: '',
  };
    country;
    autocomplete;
    autoCompleteOptions = {
        types: ['geocode'],
        componentRestrictions: {country: 'us'},
    };
  constructor(protected admin: AdminService, protected router: Router) { }

  ngOnInit(): void {
    this.admin
    .get('map-config')
    .subscribe((data) => {
      this.country = data['country'];


      this.autoCompleteOptions.componentRestrictions.country = this.country.google_map_short_name;
      this.autocomplete = new google.maps.places.Autocomplete(
        this.searchInput.nativeElement,
        this.autoCompleteOptions);
      const that = this;
      this.autocomplete.addListener('place_changed', function () {
        const place = that.autocomplete.getPlace();
        that.driver.service_location = that.searchInput.nativeElement.value;
        that.driver.service_location_lat = place.geometry.location.lat();
        that.driver.service_location_lng = place.geometry.location.lng();
      });
    });
  }

  notValid(name) {
      return name.invalid && (name.dirty || name.touched);
  }

  valid(name) {
    return name.valid;
  }
  onDateChange(data) {
    this.driver.dob = data;
  }
  submitDriverForm(form: any) {
      const that = this;
        this.admin.post('drivers', this.driver).subscribe({
          next: function (res: any) {
            that.admin.noty(res.message);
            that.router.navigate(['drivers']);
          },
            error: function(error) {
                switch (error.status) {
                  case 422:
                        that.admin.noty('Validation Error', true);
                        that.admin.showValidationError(form, error.error);
                    break;
                  default :
                      that.admin.noty('Server Error', true);
                    break;
                }
            },
        });
  }
}
