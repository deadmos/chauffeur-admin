export const MENUS = [
  {
    id: 'driver',
    title: 'Driver Accounts',
    icon: 'fa fa-user-secret',
    link: '/drivers',
  },
  {
    id: 'document',
    title: 'Driver Document Types',
    icon: 'fa fa-list',
    link: '/drivers/document-types',
  },
  {
    id: 'terms',
    title: 'Driver Terms & Conditions',
    icon: 'fa fa-file',
    link: '/drivers/terms',
  },
];
