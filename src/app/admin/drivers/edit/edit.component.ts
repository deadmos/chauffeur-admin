import {Component, OnInit, ViewChild} from '@angular/core';
import {DatePipe} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {Params, Router, ActivatedRoute} from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {NotyService} from 'app/admin/services/noty.service';
import {IMyDpOptions} from 'mydatepicker';

@Component({
  selector: 'ngx-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {
  rowData: any;
  data: any = {is_profile_approved: 0, is_document_approved: 0};
  driverId: any;
  driverDetailResponse: any;
  statusResponse: any;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Driver List',
      link: '/drivers',
      icon: 'fa fa-user-secret',
    },
    {
      title: 'Details',
      icon: 'fa fa-file',
    },
  ];
  headerData = {
    title: 'Driver Detail',
  };
  min: Date;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
  };
  public date_model: any = '';


  constructor(
    private httpClient: HttpClient,
    private admin: AdminService,
    private router: Router,
    private noty: NotyService,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe,
  ) {
    this.min = new Date();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.driverId = params.id;
    });
    this.getDriverDetail();
  }

  public error: boolean = true;

  ngOnInit(): void {

  }

  getNumber(obj) {
    this.driverDetailResponse.data.driver.user.mobile = obj;
  }

  telInputObject(obj: any, phone) {
    obj.setNumber(phone);
  }

  getDriverDetail() {
    this.admin.get(`driver/${this.driverId}`).subscribe((responseData) => {
      this.driverDetailResponse = responseData;
      // this.phone.setNumber('+447733123456');
      this.data = {
        is_profile_approved: this.driverDetailResponse.data.driver.is_profile_approved,
        is_document_approved: this.driverDetailResponse.data.driver.is_document_approved,
      };
      this.myDatePickerOptions.dateFormat   = this.driverDetailResponse.data.dateConfig['date-format'];
    });
  }

  showLoader() {
    Swal.fire({
      title: 'Please Wait !',
      allowOutsideClick: false,
      showConfirmButton: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }
  viewFile(url) {
    this.admin.getFile(url);
  }

  convertDate(date: any) {
    if (!date) {
      return;
    }
    return (new DatePipe('en-Us').transform(new Date(date), 'yyyy-MM-dd 00:00:00'));
  }
  onFileSelect(files: FileList, index: number, file_type: string) {

    this.driverDetailResponse.data.documentTypes[index][file_type + '_input_name'] = files.item(0)['name'];
    this.driverDetailResponse.data.documentTypes[index]['document'][file_type] = files.item(0);
  }

  submitFormPersonal(data: any) {
    const that = this;
    const formattedData = this.admin.formatFormData(data);
    formattedData.append('mobile', this.driverDetailResponse.data.driver.user.mobile);

    this.showLoader();
    const postUrl = `driver/update/${this.driverId}`;
    this.admin.post(postUrl, formattedData, true).subscribe({
      next: (responseData) => {
        Swal.close();
        this.statusResponse = responseData;
        this.noty.success(this.statusResponse.message);
      },
      error: function (error) {
        Swal.close();
        switch (error.status) {
          case 422:
            that.admin.noty('Validation Error', true);
            that.admin.showValidationError(data, error.error);
            break;
          default :
            that.admin.noty('Server Error', true);
            break;
        }
      },
    });

  }

  submitFormDocument() {
    const documentData: any = [];
    const formData: FormData = new FormData();
    this.driverDetailResponse.data.documentTypes.forEach((element, index) => {
      const value: any = {};

      value.id = element.id;
      documentData[index] = value;
      if (typeof (element.document.file) !== 'undefined' && element.document.file) {
        value.file = element.document.file;
        documentData[index] = value;
      }
      if (typeof (element.document.extra_file) !== 'undefined' && element.document.extra_file) {
        value.extra_file = element.document.extra_file;
        documentData[index] = value;
      }
      if (element.is_contain_expire === 1) {
        value.expire_date = this.convertDate(element.document.expire_date);
        documentData[index] = value;
      }
      if (element.is_contain_name === 1) {
        value.name = element.document.name;
        documentData[index] = value;
      }
      if (element.is_contain_number === 1) {
        value.number = element.document.number;
        documentData[index] = value;
      }
    });
    documentData.forEach((element, index) => {
      for (const key in element) {
        if (key === 'file' || key === 'extra_file') {
          formData.append(`documents[${index}][${key}]`, element[key], element[key].name);
        } else {
          formData.append(`documents[${index}][${key}]`, element[key]);
        }
      }
    });

    this.showLoader();
    const postUrl = this.admin.getFullUrl(`driver/update-documents/${this.driverId}`);
    this.httpClient.post<any>(postUrl, formData, {headers: this.admin.getHeader(true)}).subscribe({
      next: (responseData) => {
        Swal.close();
        this.statusResponse = responseData;
        this.noty.success(this.statusResponse.message);
      },
      error: (error) => {
        Swal.close();
        this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
      },
    });
  }

  toggleClick(event, toggleName: string) {
    event.preventDefault();
    let title, message;
    if (this.data[toggleName]) {
      title = 'Driver Dis-Approval';
      message =
        'Your ' +
        (toggleName === 'is_profile_approved'
          ? 'personal detail'
          : 'document') +
        ' is incorrect.';
    } else {
      title = 'Driver Approval';
      message =
        'Your ' +
        (toggleName === 'is_profile_approved'
          ? 'personal detail'
          : 'document') +
        ' is verified.';
    }

    Swal.fire({
      title: title,
      inputLabel: 'Message',
      input: 'textarea',
      inputAttributes: {
        autocapitalize: 'off',
      },
      showCancelButton: true,
      showDenyButton: true,
      confirmButtonText: 'Notify',
      confirmButtonColor: '#00a65a',
      denyButtonText: `Don't Notify`,
      denyButtonColor: '#f39c12',
      didOpen: function () {
        Swal.getPopup().querySelector('#swal2-input').value = message;
      },
    }).then((result) => {

      const requestData = {
        notify: result.isConfirmed ? true : false,
        type: toggleName === 'is_profile_approved' ? 'personal' : 'document',
        status: !this.data[toggleName],
        message: Swal.getPopup().querySelector('#swal2-input').value,
      };
      if (result.isConfirmed || result.isDenied) {
        this.showLoader();
        this.admin.post(`driver/approve/${this.driverId}`, requestData).subscribe({
          next: (response) => {
            Swal.close();
            this.statusResponse = response;
            this.data[toggleName] = this.data[toggleName] === 1 ? 0 : 1;
            this.noty.success(this.statusResponse.message);
          },
          error: (error) => {
            Swal.close();
            this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
          },
        });

      }
    });
  }

  getCheckedStatus(toggleName: string) {
    return this.data[toggleName] === 1;
  }

  notValid(name) {
    return name.invalid && (name.dirty || name.touched);
  }

  valid(name) {
    return name.valid;
  }


  moment(expire_date: any) {
    return expire_date;
  }
}
