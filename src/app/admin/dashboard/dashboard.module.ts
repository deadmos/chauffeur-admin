import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import {NgxEchartsModule} from 'ngx-echarts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {ChartModule} from 'angular2-chartjs';
import {NbCardModule} from '@nebular/theme';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    AdminThemeModule,
    NgxEchartsModule,
    NgxChartsModule,
    ChartModule,
    NbCardModule,

  ],
})
export class DashboardModule { }
