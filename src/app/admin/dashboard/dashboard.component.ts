import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NbThemeService} from '@nebular/theme';
import { AdminService } from '../admin.service';


@Component({
  selector: 'ngx-admin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnDestroy {

  area_driver_results = [
  ];
  network_booking_results = [
  ];
  showLegend = true;
  showLabels = true;
  showXAxis = true;
  showYAxis = true;
  xAxisLabel = 'Networks';
  yAxisLabel = 'Bookings';
  colorScheme: any;
  themeSubscription: any;
  title: string;
  type: string;
  on = true;

  constructor(private theme: NbThemeService, private admin: AdminService) {
    this.admin.get('chart-data').subscribe(res => {
       this.area_driver_results = res['area'];
       this.network_booking_results = res['partner'];
      });
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

}
