import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { GeneralComponent } from './general/general.component';
import { EmailComponent } from './email/email.component';
import { SmsComponent } from './sms/sms.component';

import {NbButtonModule,NbCardModule, NbCheckboxModule, NbFormFieldModule, NbInputModule, NbListModule, NbSelectModule, NbToggleModule,NbDatepickerModule} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import {AdminModule} from '../admin.module';
import {AgGridModule} from 'ag-grid-angular';

import {FormsModule} from '@angular/forms';
import {NbMomentDateModule } from '@nebular/moment';


@NgModule({
  declarations: [SettingsComponent, GeneralComponent, EmailComponent, SmsComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    NbCardModule,
    AdminThemeModule,
    AdminModule,
    AgGridModule,
    NbSelectModule,
    NbToggleModule,
    NbDatepickerModule,
    NbMomentDateModule,
    NbCheckboxModule,
    NbButtonModule,
    NbFormFieldModule,
    NbInputModule,
    NbListModule,
    FormsModule
  ]
})
export class SettingsModule { }
