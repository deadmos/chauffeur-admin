import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmailComponent } from './email/email.component';
import { GeneralComponent } from './general/general.component';
import { SmsComponent } from './sms/sms.component';

const routes: Routes = [
    {

        path: 'admin',
        component: GeneralComponent,
    },
    {

        path: 'email',
        component: EmailComponent,
    },
    {

        path: 'sms',
        component: SmsComponent,
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
