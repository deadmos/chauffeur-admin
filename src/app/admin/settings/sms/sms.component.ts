import { Component, OnInit } from '@angular/core';
import { MENUS } from '../setting-menu';
@Component({
  selector: 'ngx-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.scss']
})
export class SmsComponent implements OnInit {

    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Sms Setting',
            icon: 'fa fa-envelope',
        },
    ];
    headerData = {
        title: 'Sms Setting',
        action: false,
    };
    menus = MENUS;
  constructor() { }

  ngOnInit(): void {
  }

}
