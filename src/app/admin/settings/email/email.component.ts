import { Component, OnInit } from '@angular/core';
import { MENUS } from '../setting-menu';

@Component({
  selector: 'ngx-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {

    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Email Setting',
            icon: 'fa fa-envelope',
        },
    ];
    headerData = {
        title: 'Email Setting',
        action: false,
    };
    menus = MENUS;
  constructor() { }

  ngOnInit(): void {
  }

}
