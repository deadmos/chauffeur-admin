export const MENUS = [
  {
    id: 'admin',
    title: 'Admin Setting',
    icon: 'fa fa-cog',
    link: '/settings/admin',
  },
  {
    id: 'email',
    title: 'Email Setting',
    icon: 'fa fa-envelope',
    link: '/settings/email',
  },
  {
    id: 'sms',
    title: 'SMS Setting',
    icon: 'fa fa-envelope',
    link: '/settings/sms',
  },
];
