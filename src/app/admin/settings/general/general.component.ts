import { Component, OnInit } from '@angular/core';
import { MENUS } from '../setting-menu';
@Component({
  selector: 'ngx-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {

    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'General Setting',
            icon: 'fa fa-cog',
        },
    ];
    headerData = {
        title: 'General Setting',
        action: false,
    };
    menus = MENUS;
  constructor() { }

  ngOnInit(): void {
  }

}
