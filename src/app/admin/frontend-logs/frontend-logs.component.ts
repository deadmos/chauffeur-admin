import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-frontend-logs',
  templateUrl: './frontend-logs.component.html',
  styleUrls: ['./frontend-logs.component.scss'],
})
export class FrontendLogsComponent implements OnInit {
  menus = [];
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Frontend Logs',
      icon: 'fa fa-history',
    },
  ];
  headerData = {
    title: 'Frontend Logs',
    action: false,
  };
  constructor() { }

  ngOnInit(): void {
  }

}
