import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingsRoutingModule } from './bookings-routing.module';
import { BookingsComponent } from './bookings.component';
import {AgGridModule} from 'ag-grid-angular';
import {NbCardModule, NbSelectModule, NbToggleModule} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import {AdminModule} from '../admin.module';


@NgModule({
  declarations: [BookingsComponent],
  imports: [
    CommonModule,
    BookingsRoutingModule,
    AgGridModule,
    NbCardModule,
    AdminThemeModule,
    NbSelectModule,
    NbToggleModule,
    AdminModule,
  ],
})
export class BookingsModule { }
