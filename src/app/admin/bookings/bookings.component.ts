import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';

@Component({
    selector: 'ngx-bookings',
    templateUrl: './bookings.component.html',
    styleUrls: ['./bookings.component.scss'],
})
export class BookingsComponent implements OnInit {
    private gridApi;
    private gridColumnApi;

    public columnDefs;
    public defaultColDef;
    public rowData: {};
    public rowSelection;
    menus = [];
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Bookings List',
            icon: 'fa fa-file',
        },
    ];
    headerData = {
        title: 'Bookings List',
        action: false,
    };
    constructor(private http: HttpClient, private admin: AdminService, private router: Router) {
        this.columnDefs = [
            { field: 'id', headerName: 'Id', sortable: true, filter: false, maxWidth:60},
            { field: 'booking_reference_id', headerName: 'B-Ref', sortable: false, filter: false, maxWidth:60 },
            { field: 'reference_id', headerName: 'M-Ref', sortable: false, filter: false , maxWidth:60},
            { field: 'pickup.name', headerName: 'From', sortable: false, filter: false ,minWidth:150},
            { field: 'dropoff.name', headerName: 'To', sortable: false, filter: false ,minWidth:150},
            {
                field: 'start',
                headerName: 'Date/Time',
                valueFormatter: params => this.dateFormatter(params.data.start),
                sortable: true,
                filter: false,
                maxWidth:100
            },
            {
                field: 'driver_fare',
                headerName: 'Fare',
                valueFormatter: params => this.currencyFormatter(params.data.driver_fare, '$'),
                sortable: true,
                filter: false,
                maxWidth:60
            },
            { field: 'status', headerName: 'Status', sortable: false, filter: false, maxWidth:60},

        ];
        this.defaultColDef = { resizable: true };
        this.rowSelection = 'multiple';
    }
    zeroPad (num){
        return (num < 10 ? '0' + num : num);
    };
    currencyFormatter(currency, sign) {
        currency=currency>0?currency:'0.00';
        return sign + `${currency}`;
    }
    dateFormatter(date) {
        date=new Date(date);
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        return `${this.zeroPad(day)}/${this.zeroPad(month)}/${year}`;
    }

    onFirstDataRendered(params) {
        params.api.sizeColumnsToFit();
    }
    onGridSizeChanged(params) {
        const gridWidth = document.getElementById('grid-wrapper').offsetWidth;
        const columnsToShow = [];
        const columnsToHide = [];
        let totalColsWidth = 0;
        const allColumns = params.columnApi.getAllColumns();
        for (let i = 0; i < allColumns.length; i++) {
            const column = allColumns[i];
            totalColsWidth += column.getMinWidth();
            if (totalColsWidth > gridWidth) {
                columnsToHide.push(column.colId);
            } else {
                columnsToShow.push(column.colId);
            }
        }
        params.columnApi.setColumnsVisible(columnsToShow, true);
        params.columnApi.setColumnsVisible(columnsToHide, false);
        params.api.sizeColumnsToFit();
    }

    onGridReady(params) {

        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        const pageListUrl = this.admin.getFullUrl('bookings');
        const headers = this.admin.getHeader();
        this.http.get(pageListUrl, { headers: headers }).subscribe((data) => {
            this.rowData = data;
        });
        this.rowData = [];
    }
    ngOnInit(): void {
    }
}
