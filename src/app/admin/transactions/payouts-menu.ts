export const MENUS = [
  {
    id: 'primary',
    title: 'Driver Payouts',
    icon: 'fa fa-user-secret',
    link: '/driver-payouts',
  },
  {
    id: 'driver',
    title: 'My Payouts',
    icon: 'fa fa-building',
    link: '/my-payouts',
  },
];
