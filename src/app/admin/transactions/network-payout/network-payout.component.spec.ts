import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkPayoutComponent } from './network-payout.component';

describe('NetworkPayoutComponent', () => {
  let component: NetworkPayoutComponent;
  let fixture: ComponentFixture<NetworkPayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetworkPayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkPayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
