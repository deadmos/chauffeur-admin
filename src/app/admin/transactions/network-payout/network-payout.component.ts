import {Component, OnInit} from '@angular/core';
import {AgDatatable} from '../../admin.extends';
import {MENUS} from '../payouts-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NbThemeService} from '@nebular/theme';

@Component({
  selector: 'ngx-network-payout',
  templateUrl: './network-payout.component.html',
  styleUrls: ['./network-payout.component.scss'],
})
export class NetworkPayoutComponent extends AgDatatable implements OnInit {

  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  url = 'network-payout';
  curr_network: any = '';
  id: any;
  name: any;
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'My Payout',
      icon: 'fas fa-exchange-alt',
    },
  ];
  headerData = {
    title: 'My Payouts',
    action: false,
  };


  constructor(
    protected http: HttpClient,
    protected admin: AdminService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected themeService: NbThemeService,
  ) {
    super();
    this.activatedRoute.params.subscribe((params: Params) => {

    });
    this.columnDefs = [
      {field: 'key', headerName: 'Id', sortable: true, filter: false, maxWidth: 100, minWidth: 60},
      {field: 'id', headerName: 'Stripe Payout Id', sortable: true, filter: false, minWidth: 210},
      {field: 'bank_info', headerName: 'External Account', sortable: true, filter: false, minWidth: 210},
      {field: 'description', headerName: 'Description', sortable: true, filter: false},
      {field: 'created_formatted', headerName: 'Initiated At', sortable: true, filter: false, minWidth: 210},
      {field: 'arrival_formatted', headerName: 'Est. Arrival', sortable: true, filter: false, minWidth: 210},
      {field: 'amount', headerName: 'Amount', sortable: true, filter: false},
      {field: 'currency', headerName: 'Currency', sortable: true, filter: false},
      {
        field: 'status',
        headerName: 'Status',
        sortable: true,
        cellRenderer: function (params) {
          if (params.data !== undefined) {
            if (params.data.status === 'paid') {
              return '<span class="badge badge-success">' + params.data.status + '</span>';
            } else {
              return '<span class="badge badge-warning">' + params.data.status + '</span>';
            }
          }
        },
        filter: false,
      },
    ];
    this.defaultColDef = {resizable: true};
    this.rowSelection = 'multiple';
  }

  data = [];

  ngOnInit(): void {
    this.admin.get('ajax-networks').subscribe((res: any) => {
      this.data = res;
    });
  }

  filterNetwork() {
    if (this.curr_network !== '') {
      this.agExtras = [{name: 'network_id', value: this.curr_network}];
      this.reFetch();
    }

  }

}
