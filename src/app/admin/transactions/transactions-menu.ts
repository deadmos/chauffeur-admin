export const MENUS = [
  {
    id: 'primary',
    title: 'Primary Transaction',
    icon: 'fa fa-building',
    link: '/transactions',
  },
  {
    id: 'driver',
    title: 'Driver Transaction',
    icon: 'fa fa-user-secret',
    link: '/driver-transactions',
  },
];
