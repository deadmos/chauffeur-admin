import { Component, OnInit } from '@angular/core';
import { MENUS } from './transactions-menu';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../admin.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AgDatatable } from '../admin.extends';
import { NbDialogService, NbThemeService } from '@nebular/theme';

@Component({
    selector: 'ngx-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent extends AgDatatable implements OnInit {

  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  url = 'transactions';
    id: any;
    name: any;
    menus = MENUS;
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Transaction List',
            icon: 'fas fa-exchange-alt',
        },
    ];
    headerData = {
        title: 'Transactions',
        action: false,
    };
    commission = {
        list : [],
        network_list : [],
        primany_list : [],
    };

    constructor(
        protected http: HttpClient,
        protected admin: AdminService,
        protected activatedRoute: ActivatedRoute,
        protected router: Router,
        protected dialogService: NbDialogService,
        protected themeService: NbThemeService,
    ) {
        super();
        this.activatedRoute.params.subscribe((params: Params) => {
            this.id = params.id;
            if (this.router.url.includes('invoices') && this.id) {
                this.url = `invoices/${this.id}/transactions`;
            } else if (this.router.url.includes('drivers') && this.id) {
                this.url = `driver/${this.id}/transactions`;
            }
        });
        this.columnDefs = [
            { field: 'id', headerName: 'Id', sortable: true, filter: false, maxWidth: 100, minWidth: 60 },
            { field: 'reference_id', headerName: 'Transaction ID', sortable: true, filter: false },
            { field: 'booking_reference_id', headerName: 'Booking ID', sortable: true, filter: false },
            { field: 'movement_reference_id', headerName: 'Movement ID', sortable: true, filter: false },
            {
                field: 'driver.user.name',
                headerName: 'Driver',
                sortable: true,
                filter: true,
                filterParams: {
                    filterOptions: ['contains', 'equals'],
                    defaultOption: 'contains',
                    suppressAndOrCondition: true,
                },
            },
            {
                field: 'full_amount',
                headerName: 'Full Amount',
                valueFormatter: params => params.data !== undefined ? this.currencyFormatter(params.data.full_amount, '$') : '',
                sortable: true,
                filter: false,
            },
            {
                field: 'network_amount',
                headerName: 'Network Amount',
                valueFormatter: params => params.data !== undefined ? this.currencyFormatter(params.data.network_amount, '$') : '',
                sortable: true,
                filter: false,
            },
            {
                field: 'driver_amount',
                headerName: 'Driver Amount',
                valueFormatter: params => params.data !== undefined ? this.currencyFormatter(params.data.driver_amount, '$') : '',
                sortable: true,
                filter: false,
            },
            {
                field: 'primary_driver_commission',
                headerName: 'Driver Comm.',
                valueFormatter: params => params.data !== undefined ?
                  this.currencyFormatter(params.data.primary_driver_commission, '$') : '',
                sortable: true,
                filter: false,
            },
            {
                field: 'primary_network_commission',
                headerName: 'Network Comm.',
                valueFormatter: params => params.data !== undefined ?
                  this.currencyFormatter(params.data.primary_network_commission, '$') : '',
                sortable: true,
                filter: false,
            },
        ];
        this.defaultColDef = { resizable: true };
        this.rowSelection = 'multiple';
    }

    rowClicked(data, dialog) {
        this.admin.get('transactions/' + data.data.id).subscribe({
            next: (res: any) => {
                this.commission = res;
                this.dialogService.open(dialog);
            },
            error: (error) => {

            },
        });
    }

    ngOnInit(): void {

    }

}
