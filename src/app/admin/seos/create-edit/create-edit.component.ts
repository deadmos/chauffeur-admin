import { Component, OnInit } from '@angular/core';
import { AdminService } from "../../admin.service";
import { Params, Router, ActivatedRoute } from "@angular/router";
import Swal from "sweetalert2/dist/sweetalert2.js";
import { NotyService } from "app/admin/services/noty.service";

@Component({
    selector: 'ngx-create-edit',
    templateUrl: './create-edit.component.html',
    styleUrls: ['./create-edit.component.scss']
})
export class CreateEditComponent implements OnInit {
    id: number;
    breadcrumbs = [
        {
            title: "Home",
            link: "/dashboard",
            icon: "fa fa-home",
        },
        {
            title: "Seo List",
            link: "/seo",
            icon: "fa fa-search",
        },
        {
            title: "Details",
            icon: "fa fa-file",
        },
    ];
    headerData = {
        title: "SEO Detail",
    };
    seoResponse: any = { title: '', url: '', meta_title: '', meta_keywords: '', meta_description: '' };
    constructor(
        private admin: AdminService,
        private router: Router,
        private noty: NotyService,
        private activatedRoute: ActivatedRoute,
    ) {
        this.activatedRoute.params.subscribe((params: Params) => { this.id = params.id });
        if (this.id){
            this.admin.get(`seo/${this.id}`).subscribe((responseData) => {
                this.seoResponse = responseData['data'];
            });
        }
    }

    ngOnInit(): void {
    }

    showLoader() {
        Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading();
            },
        });
    }

    submitForm() {
        let postUrl;
        if(this.seoResponse.id != undefined){
            postUrl= `seo/${this.seoResponse.id}/update`;
        }else{
            postUrl= `seo/store`;
        }

        this.showLoader();
        this.admin.post(postUrl, this.seoResponse).subscribe({
            next: (res) => {
                let response: any = res;
                this.noty.success(response.message);
                Swal.close();
            },
            error: (error) => {
                Swal.close();
                this.noty.error("Server Error Status:" + error.status + "<br>" + error.message);
            },
        });
    }

  notValid(name) {
    return name.invalid && (name.dirty || name.touched);
  }
  valid(name) {
    return name.valid;
  }

}
