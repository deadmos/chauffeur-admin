import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {Router} from '@angular/router';
import { ActionsComponent } from '../common/actions/actions.component';

@Component({
  selector: 'ngx-seos',
  templateUrl: './seos.component.html',
  styleUrls: ['./seos.component.scss'],
})
export class SeosComponent implements OnInit {

  private gridApi;
  private gridColumnApi;

  public columnDefs;
  public defaultColDef;
  public rowData: {};

  menus = [];
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Seo List',
      icon: 'fa fa-search',
    },
  ];
  headerData = {
    title: 'Seo List',
    action: true,
    actionLink: '/seo/create',
    actionTitle: 'Add New Seo',
  };



  constructor(private http: HttpClient, private admin: AdminService, private router: Router) {
    this.columnDefs = [
      { field: 'id', headerName: 'Id', sortable: false, filter: false , maxWidth: 40},
      { field: 'url', headerName: 'Page Url', sortable: false, filter: false , maxWidth: 400},
      { field: 'title', headerName: 'Title', sortable: false, filter: false , maxWidth: 200},
      { field: 'meta_title', headerName: 'Meta Title', sortable: false, filter: false},
      { field: 'meta_keywords', headerName: 'Meta Keywords', sortable: false, filter: false},
      { field: 'meta_description', headerName: 'Meta Description', sortable: false, filter: false},
      { 
        field: 'action', sortable: true, filter: false,  maxWidth: 120,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'Edit Page',
            href: '/seo/edit',
            type: 'edit',
          },
          {
            title: 'Delete Page',
            href: 'seo/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete?',
              successMessage: 'Seo deleted successfully',
            },
          },
        ],
      }
    ];
    this.defaultColDef = { resizable: true };
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }
  onGridSizeChanged(params) {
    const gridWidth = document.getElementById('grid-wrapper').offsetWidth;
    const columnsToShow = [];
    const columnsToHide = [];
    let totalColsWidth = 0;
    const allColumns = params.columnApi.getAllColumns();
    for (let i = 0; i < allColumns.length; i++) {
      const column = allColumns[i];
      totalColsWidth += column.getMinWidth();
      if (totalColsWidth > gridWidth) {
        columnsToHide.push(column.colId);
      } else {
        columnsToShow.push(column.colId);
      }
    }
    params.columnApi.setColumnsVisible(columnsToShow, true);
    params.columnApi.setColumnsVisible(columnsToHide, false);
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {

    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    const pageListUrl = this.admin.getFullUrl('seo');
    const headers = this.admin.getHeader();
    this.http.get(pageListUrl,{ headers: headers })
      .subscribe((data) => {
        this.rowData = data;
      });
    this.rowData = [];
  }
  ngOnInit(): void {
  }
}
