import {IDatasource, IGetRowsParams} from 'ag-grid-community';

export class AgDatatable {
  protected gridApi;
  protected gridColumnApi;
  protected admin;
  protected themeService;
  paginationCount = 20;
  url = '';
  agExtras = [];
  filterText = '';
  ag_class = 'ag-theme-balham-dark';
  paginationNumber = '20';
  filterParams = {
    filterOptions: ['contains', 'equals'],
    defaultOption: 'contains',
    suppressAndOrCondition: true,
  };
  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      const _this = this;
      let url = this.url + '?length=' + this.paginationCount;
      url += '&page=' + (this.gridApi.paginationProxy.getCurrentPage() + 1 );
      if (params.sortModel.length > 0) {
        let sortBy = params.sortModel[0].colId;
        const colDef = this.gridApi.getColumnDef(sortBy);
        if (colDef !== null) {
          if (colDef.searchField !== undefined) {
            sortBy = colDef.searchField;
          }
        }
        url += '&sortBy=' + sortBy + '&sortType=' + params.sortModel[0].sort;
      }
      if (Object.keys(params.filterModel).length > 0) {
        Object.keys(params.filterModel).forEach(function(key, index) {
          const filterData = params.filterModel[key];
          const colDef = _this.gridApi.getColumnDef(key);
          if (colDef !== null) {
            if (colDef.searchField !== undefined) {
              key = colDef.searchField;
            }
          }
          url += '&searchKey[' + index + ']=' + key + '&searchType[' + index + ']=' +  filterData.type;
          url +=  '&search[' + index + ']=' + filterData.filter  ;
        });
      }
      if (this.agExtras.length > 0) {
        this.agExtras.forEach(function (extra) {
          url += '&' + extra.name + '=' + extra.value;
        });
      }
      this.admin.get(url).subscribe(response => {
        params.successCallback(response['data'], response['total']);
        if (response['status'] === false && response['message'] !== undefined ) {
          this.admin.noty(response['message'], true);
        }
      });
    },
  };

  onFirstDataRendered(params): void {
    setTimeout(() => {
      params.api.sizeColumnsToFit();
    }, 50);

  }

  onPageSizeChanged() {
    this.paginationCount = Number(this.paginationNumber);
    this.gridApi.gridOptionsWrapper.setProperty('cacheBlockSize', this.paginationCount);
    this.gridApi.setDatasource(this.dataSource);
  }

  reFetch() {
    this.gridApi.setDatasource(this.dataSource);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.setDatasource(this.dataSource);
    this.themeService.onThemeChange()
      .subscribe((theme: any) => {
        if (theme.name === 'dark') {
          this.ag_class = 'ag-theme-balham-dark';
        } else {
          this.ag_class = 'ag-theme-balham';
        }
      });
  }

  onGridSizeChanged(params) {
    this.admin.tableSize(params);
  }

  onFilterTextBoxChanged() {
    let filters = this.gridApi.getFilterModel();
    if (Object.keys(filters).length > 0) {
      if (filters.globalSearch !== undefined) {
        filters.globalSearch.filter = this.filterText;
      } else {
        filters.globalSearch = {filter: this.filterText};
      }
    } else {
      filters = {'globalSearch': {filter: this.filterText}};
    }
    this.gridApi.setFilterModel(filters);
  }

  currencyFormatter(currency, sign) {
    return sign + Number(currency).toFixed(2);
  }

  statusFormatter(status) {
    if (status === 'paid') {
      return '<span class="badge badge-success">' + status + '</span>';
    } else {
      return '<span class="badge badge-warning">' + status + '</span>';
    }
  }
}

