import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminThemeModule } from '../@themes/admin/admin-theme.module';
import { AdminComponent } from './admin.component';
import { AgGridModule} from 'ag-grid-angular';
import { NbActionsModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
  NbToggleModule,
  NbTooltipModule} from '@nebular/theme';
import { ToggleComponent } from './common/toggle/toggle.component';
import { ActionsComponent} from './common/actions/actions.component';
import { DashboardModule} from './dashboard/dashboard.module';
import { ImageInputComponent } from './common/image-input/image-input.component';
import { AngularCropperjsModule} from 'angular-cropperjs';
import { TreeListComponent} from './common/tree-list/tree-list.component';
import { DndModule} from 'ng2-dnd';
import { FormsModule} from '@angular/forms';
import { TreeFormListComponent} from './common/tree-form-list/tree-form-list.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { FrontendLogsComponent } from './frontend-logs/frontend-logs.component';
import { ValidationErrorComponent } from './common/validation-error/validation-error.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { SvgComponent } from './common/svg/svg.component';
import { Ng2TelInputModule} from 'ng2-tel-input';
import { EarningsComponent } from './earnings/earnings.component';
import { TableHeaderComponent } from './common/table-header/table-header.component';
import { DriverTransactionsComponent } from './driver-transactions/driver-transactions.component';
import { DriverPayoutComponent } from './transactions/driver-payout/driver-payout.component';
import { NetworkPayoutComponent } from './transactions/network-payout/network-payout.component';
import {NgSelect2Module} from 'ng-select2';
import { DownloadComponent } from './common/download/download.component';
import {SecurePipe} from '../secure.pipe';


@NgModule({
  declarations: [
    AdminComponent,
    ToggleComponent,
    ActionsComponent,
    ImageInputComponent,
    TreeListComponent,
    TreeFormListComponent,
    NotificationsComponent,
    FrontendLogsComponent,
    ValidationErrorComponent,
    TransactionsComponent,
    SvgComponent,
    EarningsComponent,
    TableHeaderComponent,
    DriverTransactionsComponent,
    DriverPayoutComponent,
    NetworkPayoutComponent,
    DownloadComponent,
    SecurePipe,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    AdminThemeModule,
    DashboardModule,
    AgGridModule,
    NbSelectModule,
    NbCardModule,
    NbToggleModule,
    NbActionsModule,
    NbTooltipModule,
    AngularCropperjsModule,
    DndModule,
    FormsModule,
    Ng2TelInputModule,
    NbInputModule,
    NbIconModule,
    NgSelect2Module,
  ],
  exports: [
    ImageInputComponent,
    TreeListComponent,
    TreeFormListComponent,
    ValidationErrorComponent,
    TableHeaderComponent,
    DownloadComponent,
  ],
})


export class AdminModule { }
