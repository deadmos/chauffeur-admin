import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ngx-tree-form-list',
  templateUrl: './tree-form-list.component.html',
  styleUrls: ['./tree-form-list.component.scss'],
})
export class TreeFormListComponent implements OnInit {
  @Input() nodes: any = [];
  @Input() current_node: any = false;
  @Input() shownForm: boolean = false;
  @Output() nodeClicked = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit(): void {
  }

  showForm(node = null): void {
    this.shownForm = true;
    // if (node !== null) {
    this.nodeClicked.emit(node);
    // }
  }

}
