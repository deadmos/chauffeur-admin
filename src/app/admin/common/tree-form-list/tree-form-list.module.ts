import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DndModule} from 'ng2-dnd';
import {AdminModule} from '../../admin.module';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DndModule,
    AdminModule,
    FormsModule,
  ],
})
export class TreeFormListModule { }
