import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'ngx-table-header',
  templateUrl: './table-header.component.html',
  styleUrls: ['./table-header.component.scss'],
})
export class TableHeaderComponent implements OnInit {
  @Input() paginationNumber: any = 20;
  @Input() filterText: string = '';
  @Output() filterTextChange = new EventEmitter<string>();
  @Output() paginationNumberChange = new EventEmitter<any>();
  @Output() paginationChanged = new EventEmitter();
  @Output() searchChanged = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  onPageSizeChanged() {
    this.paginationNumberChange.emit(this.paginationNumber);
    this.paginationChanged.emit();
  }

  onFilterTextChanged() {
    this.filterTextChange.emit(this.filterText);
    this.searchChanged.emit();
  }

}
