import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../../admin.service';
import {ImageSnippet} from '../../../models/image-snippet';

@Component({
    selector: 'ngx-image-input',
    templateUrl: './image-input.component.html',
    styleUrls: ['./image-input.component.scss'],
})


export class ImageInputComponent implements OnInit {
    @Input() imageUrl: any;
    @Input() aspectRatioX: number = 1;
    @Input() aspectRatioY: number = 1;
    @ViewChild('inputNgModel') inputNgModel;
    @ViewChild('cropper') cropper;
    dialogRef;
    selectedFile: ImageSnippet;
    required = true;
    cropperRef;
    input;
    cropped_image = '/assets/images/default.png';
    cropped_data;
    button_title = 'Upload Image';

    config = {
        aspectRatio: this.aspectRatioX / this.aspectRatioY,
        viewMode: 2,
        scalable: false,
        background: false,
        zoomable: false,
    };

    constructor(private dialogService: NbDialogService, private http: HttpClient) {

    }
    cropChanged(crop) {
        this.cropped_image = crop.cropper.getCroppedCanvas().toDataURL('image/png');
        this.cropped_data = JSON.stringify(crop.cropper.getData());

    }
    open(dialog: TemplateRef<any>) {
        this.dialogRef = this.dialogService.open(dialog, { context: 'this is some additional data passed to dialog' });
    }

    openCropper(event = null, input = null) {
        if (event !== null) {
            this.readThis(event.target, this.cropper, input);
            this.button_title = event.target.value.split('\\')[event.target.value.split('\\').length - 1];
        } else {
            this.cropperRef = this.dialogService.open(this.cropper);
        }
    }
    closeCropper() {
        this.cropperRef.close();
    }
    readThis(inputValue: any, cropper, input): void {
        const myReader: FileReader = new FileReader();
        const _this = this;
        myReader.onloadend = function (e) {
            _this.imageUrl  = input.control.base64 = e.target.result;
            input.control.file = inputValue.files[0];
            _this.cropperRef = _this.dialogService.open(cropper);
        };
        myReader.readAsDataURL(inputValue.files[0]);
    }

  ngOnInit(): void {
    this.config.aspectRatio = this.aspectRatioX / this.aspectRatioY;

    if(this.imageUrl !== undefined){
        this.cropped_image = this.imageUrl;
    }
  }

    notValid(name) {
        return name.invalid && (name.dirty || name.touched);
    }
    valid(name) {
        return name.valid;
    }
}
