import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DndModule} from 'ng2-dnd';
import {AdminModule} from '../../admin.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DndModule,
    AdminModule,
  ],
})
export class TreeListModule { }
