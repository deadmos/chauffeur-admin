import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-tree-list',
  templateUrl: './tree-list.component.html',
  styleUrls: ['./tree-list.component.scss'],
})
export class TreeListComponent implements OnInit {
  @Input() nodes: any = [];
  dragOperation: boolean = true;

  // nodes = [
  //   {
  //     id: 1,
  //     name: 'test1111',
  //     children : [
  //       {
  //         id : 11,
  //         name : 'test11',
  //       },
  //       {
  //         id : 12,
  //         name : 'test12',
  //       },
  //     ],
  //   },
  //   {
  //     id: 2,
  //     name: 'test22222',
  //     children : [
  //       {
  //         id : 21,
  //         name : 'test21',
  //       },
  //       {
  //         id : 22,
  //         name : 'test22',
  //       },
  //     ],
  //   },
  // ];
  ngOnInit(): void {
  }
}


