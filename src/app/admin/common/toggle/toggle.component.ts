import { Component, OnDestroy } from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {NotyService} from '../../services/noty.service';


@Component({
  selector: 'ngx-admin-toggle',
  template: `<nb-toggle status="success" [checked]="getCheckedStatus()"  (click)="toggleClick($event)"></nb-toggle>`,
})
export class ToggleComponent implements ICellRendererAngularComp, OnDestroy {
  private params: any;
  private data: any;
  toggleDetail: any;
  confirm = {
    title: 'Are you sure want to change status?',
    successMessage: 'Status changed successfully',
  };
  constructor(
    private router: Router,
    private http: HttpClient,
    private admin: AdminService,
    private noty: NotyService,
  ) {
  }

  agInit(params: any): void {
    this.params = params;
    this.data = this.params.data;
    this.toggleDetail = this.params.colDef.cellRendererParams;
    if (this.toggleDetail.confirm !== undefined) {
      this.confirm = this.toggleDetail.confirm;
    }
  }

  toggleClick(event) {
    event.preventDefault();
    Swal.fire({
      title: this.confirm.title,
      showCancelButton: true,
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        const pageListUrl = this.admin.getFullUrl(this.params.href.replace('__placeholder__', this.data.id));
        const headers = this.admin.getHeader();
        this.http
          .post(
            pageListUrl, null,
            { headers: headers },
          ).subscribe({
          next: data => {
            this.data[this.toggleDetail.name] = this.data[this.toggleDetail.name] === 1 ? 0 : 1;
            this.noty.success(this.confirm.successMessage);
          },
          error: error => {
            this.noty.error('Server Error Status:' + error.status);
          },
        });
      }
    });

  }
  getCheckedStatus() {
    return this.data !== undefined ? this.data[this.toggleDetail.name] === 1 : false;
  }

  ngOnDestroy() {
    // no need to remove the button click handler
    // https://stackoverflow.com/questions/49083993/does-angular-automatically-remove-template-event-listeners
  }

  refresh(params: any): boolean {
    return false;
  }
}
