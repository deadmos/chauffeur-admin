import {Component, OnDestroy} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import * as  Noty from 'noty';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';


@Component({
  selector: 'ngx-admin-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss'],
})
export class ActionsComponent implements ICellRendererAngularComp, OnDestroy {
  private params: any;
  public displayType: string;
  data;
  confirm = {
    title: 'Are you sure want to delete?',
    successMessage: 'Deleted successfully',
  };
  actions: [];

  constructor(private router: Router, private http: HttpClient, private admin: AdminService) {
  }

  agInit(params: any): void {
    this.params = params;
    this.actions = this.params.colDef.cellRendererParams;
    this.displayType = this.params.colDef.displayType !== undefined ? this.params.colDef.displayType : 'row';
    this.data = this.params.data;
  }

  getActionIcon(type) {
    let iconName = '';
    switch (type) {
      case 'edit':
        iconName = 'edit-2-outline';
        break;
      case 'detail':
        iconName = 'eye-outline';
        break;
      case 'download':
        iconName = 'download';
        break;
      case 'fleet':
        iconName = 'car-outline';
        break;
      case 'delete':
        iconName = 'trash-2-outline';
        break;
      case 'setting':
        iconName = 'settings';
        break;
      default:
        iconName = type;
        break;
    }

    return iconName;
  }

  navigate(action: any) {
    const confirm = action.confirm !== undefined ? action.confirm : this.confirm;
    const hrefFormat = action.hrefFormat !== undefined ? action.hrefFormat : false;
    const hrefLink = action.hrefLink !== undefined ? action.hrefLink : false;
    if (hrefLink) {
      window.open(this.admin.getFullUrl(action.href + this.data.id), '_blank');
    }
    if (action.type === 'delete') {
      Swal.fire({
        title: confirm.title,
        showCancelButton: true,
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          const pageListUrl = this.admin.getFullUrl(action.href + this.data.id);
          const headers = this.admin.getHeader();
          this.http.delete(pageListUrl, {headers: headers})
            .subscribe((data) => {
              this.params.api.refreshInfiniteCache();
              new Noty({
                layout: 'topRight',
                timeout: 2000,
                theme: 'metroui',
                type: 'success',
                text: confirm.successMessage,
              }).show();
            } , (error) => {
                this.admin.noty('Error While Deleting', true);
        });
        }
      });
    } else if (action.actionEvent !== undefined && action.actionEvent === 'update') {
      Swal.fire({
        title: confirm.title,
        showCancelButton: true,
        confirmButtonText: 'Yes',
      }).then((result) => {
        const _this = this;
        if (result.value) {
          const updateUrl = this.admin.getFullUrl(action.href + this.data.id);
          const headers = this.admin.getHeader();
          this.http.post(updateUrl, {}, {headers: headers})
            .subscribe((res) => {
              this.params.api.refreshInfiniteCache();
              new Noty({
                layout: 'topRight',
                timeout: 2000,
                theme: 'metroui',
                type: 'success',
                text: confirm.successMessage,
              }).show();
            });
        }
      });
    } else if (action.type === 'download') {
        const headers = this.admin.getHeader();
        this.http.get(this.admin.getFullUrl(action.href + this.data.id), {
            headers: headers,
            responseType: 'blob',
        }).subscribe((response: any) => {
            const  dataType = response.type;
            const  binaryData = [];
            binaryData.push(response);
            const  downloadLink = document.createElement('a');
            downloadLink.target = '_blank';
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
            document.body.appendChild(downloadLink);
            downloadLink.click();
        },
        (error) => {
                this.admin.noty('Error While Downloading', true);
        });
    } else {
      if (hrefFormat) {
        action.href = action.href.replace(':id', this.data.id);
        this.router.navigate([action.href]);
      } else {
        this.router.navigate([action.href, this.data.id]);
      }
    }

  }

  ngOnDestroy() {
    // no need to remove the button click handler
    // https://stackoverflow.com/questions/49083993/does-angular-automatically-remove-template-event-listeners
  }

  refresh(params: any): boolean {
    return false;
  }
}
