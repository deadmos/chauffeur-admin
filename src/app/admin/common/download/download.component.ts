import {Component, Input, OnInit, TemplateRef} from '@angular/core';
import {AdminService} from '../../admin.service';
import {HttpClient} from '@angular/common/http';
import {NbDialogService} from '@nebular/theme';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'ngx-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss'],
})
export class DownloadComponent implements OnInit {
  @Input() url: string;
  @Input() fullUrl: boolean = true;
  dialogRef: any;
  imageUrl: any;
  image_url: any;
  constructor(
    private admin: AdminService,
    private http: HttpClient,
    private dialogService: NbDialogService,
    private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.image_url = this.fullUrl ?   this.url : this.admin.getFullUrl(this.url);
  }

  download(event, dialog: TemplateRef<any>) {
    this.dialogRef = dialog;
    event.preventDefault();
    const headers = this.admin.getHeader();
    this.http.get(this.fullUrl ?   this.url : this.admin.getFullUrl(this.url), {
      headers: headers,
      responseType: 'blob',
    }).subscribe((response: any) => {
        const dataType = response.type;
        if (dataType === 'image/jpeg' || dataType === 'image/png' || dataType === 'image/jpg' || dataType === 'image/webp') {
          this.openNewDialog(dialog, response);
        } else {
          const binaryData = [];
          binaryData.push(response);
          const downloadLink = document.createElement('a');
          downloadLink.target = '_blank';
          downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
          document.body.appendChild(downloadLink);
          downloadLink.click();
        }

      },
      (error) => {
        this.admin.noty('Error While Downloading', true);
      });
  }
  openNewDialog(dialog: TemplateRef<any>, data) {
    const unsafeImageUrl = URL.createObjectURL(data);
    this.imageUrl = this.sanitizer.bypassSecurityTrustUrl(unsafeImageUrl);
    this.dialogRef = this.dialogService.open(dialog, { context: 'this is some additional data passed to dialog' });
  }

}
