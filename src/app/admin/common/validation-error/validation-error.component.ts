import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ngx-validation-error',
  templateUrl: './validation-error.component.html',
  styleUrls: ['./validation-error.component.scss'],
})
export class ValidationErrorComponent implements OnInit {
  @Input() notValid: boolean;
  @Input() valid: boolean;
  @Input() validationError: 'Validation Error';

  constructor() { }

  ngOnInit(): void {
  }

}
