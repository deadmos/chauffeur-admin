export const MENUS = [
  {
    id: 'driver',
    title: 'Driver Invoices',
    icon: 'fa fa-user-secret',
    link: '/invoices/driver',
  },
  {
    id: 'primary',
    title: 'Primary Invoices',
    icon: 'fa fa-building',
    link: '/invoices/primary',
  },
];
