import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesComponent } from './invoices.component';
import {NbButtonModule,NbCardModule, NbCheckboxModule, NbFormFieldModule, NbInputModule, NbListModule, NbSelectModule, NbToggleModule,NbDatepickerModule} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import {AdminModule} from '../admin.module';
import {AgGridModule} from 'ag-grid-angular';

import {FormsModule} from '@angular/forms';
import {NbMomentDateModule } from '@nebular/moment';
import { NetworkInvoiceComponent } from './network-invoice/network-invoice.component';
import {NgSelect2Module} from "ng-select2";

@NgModule({
  declarations: [InvoicesComponent, NetworkInvoiceComponent],
  imports: [
    CommonModule,
    InvoicesRoutingModule,
    NbCardModule,
    AdminThemeModule,
    AdminModule,
    AgGridModule,
    NbSelectModule,
    NbToggleModule,
    NbDatepickerModule,
    NbMomentDateModule,
    NbCheckboxModule,
    NbButtonModule,
    NbFormFieldModule,
    NbInputModule,
    NbListModule,
    FormsModule,
    NgSelect2Module
  ],
})
export class InvoicesModule { }
