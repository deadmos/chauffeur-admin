import {Component, OnInit} from '@angular/core';
import {MENUS} from './invoices-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {ActivatedRoute, Params} from '@angular/router';
import {ActionsComponent} from '../common/actions/actions.component';
import {AgDatatable} from '../admin.extends';
import {NbThemeService} from '@nebular/theme';

@Component({
  selector: 'ngx-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
})
export class InvoicesComponent extends AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  driverId: any;
  name: any;
  curr_driver: any = '';
  menus = MENUS;
  url = 'driver-invoices';
  agExtras = [];
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Invoice List',
      icon: 'fas fa-file-alt',
    },
  ];
  headerData = {
    title: 'Invoices',
    action: false,
  };

  constructor(
    private http: HttpClient,
    protected admin: AdminService,
    protected themeService: NbThemeService,
    private activatedRoute: ActivatedRoute,
  ) {
    super();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.driverId = params.id;
    });
    this.columnDefs = [
      {
        field: 'globalSearch',
        hide: true,
        initialHide: true,
        filter: 'agTextColumnFilter',
        filterParams: {
          newRowsAction: 'keep',
        },
      },
      {field: 'id', headerName: 'Id', sortable: true, filter: false, maxWidth: 100, minWidth: 60},
      {field: 'reference_id', headerName: 'Invoice ID', sortable: true, filter: false},
      {field: 'start', headerName: 'From Date', sortable: true, filter: false},
      {field: 'end', headerName: 'To Date', sortable: true, filter: false},
      {
        field: 'driver.user.name',
        headerName: 'Driver',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'paid_status',
        headerName: 'Status',
        valueFormatter: params => params.data !== undefined ? this.paymentStatusFormatter(params.data.paid_at) : '',
        sortable: true,
        filter: false,
      },
      {
        field: 'transactions_sum',
        headerName: 'Amount',
        valueFormatter: params => params.data !== undefined ?
          this.currencyFormatter(params.data.transactions_sum, '$') : '',
        sortable: true,
        filter: false,
      },
      {
        field: 'action', sortable: true, filter: false, minWidth: 150,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'View Transaction',
            href: '/invoices/:id/transactions/',
            hrefFormat: true,
            type: 'detail',
          },
          {
            title: 'Mark as Paid',
            href: 'invoices/mark-as-paid/',
            type: 'checkmark-circle-outline',
            confirm: {
              title: 'Are you sure want to mark as paid?',
              successMessage: 'Marked as paid successfully',
            },
            actionEvent: 'update',
            actionEventUpdate: {
              field: 'paid_at',
            },
          },
        ],
      },
    ];
    this.defaultColDef = {resizable: true};
    this.rowSelection = 'multiple';
  }

  paymentStatusFormatter(paid_at) {
    let status = 'Unpaid';
    if (paid_at) {
      status = 'Paid';
    }
    return status;
  }
  data = [];

  ngOnInit(): void {
    this.admin.get('ajax-drivers').subscribe((res: any) => {
      this.data = res;
    });
  }
  filterDriver() {
    if (this.curr_driver !== '') {
      this.agExtras = [{name: 'driver_id', value: this.curr_driver}];
      this.reFetch();
    }

  }


}
