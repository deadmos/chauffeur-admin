import { Component, OnInit } from '@angular/core';
import {AgDatatable} from '../../admin.extends';
import {MENUS} from '../invoices-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {NbThemeService} from '@nebular/theme';
import {ActivatedRoute, Params} from '@angular/router';
import {ActionsComponent} from '../../common/actions/actions.component';

@Component({
  selector: 'ngx-network-invoice',
  templateUrl: './network-invoice.component.html',
  styleUrls: ['./network-invoice.component.scss'],
})
export class NetworkInvoiceComponent extends AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  driverId: any;
  name: any;
  menus = MENUS;
  url = 'primary-invoices';
  agExtras = [];
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Primary Invoice List',
      icon: 'fas fa-file-alt',
    },
  ];
  headerData = {
    title: 'Primary Invoices',
    action: false,
  };

  constructor(
    private http: HttpClient,
    protected admin: AdminService,
    protected themeService: NbThemeService,
    private activatedRoute: ActivatedRoute,
  ) {
    super();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.driverId = params.id;
    });
    this.columnDefs = [
      {
        field: 'globalSearch',
        hide: true,
        initialHide: true,
        filter: 'agTextColumnFilter',
        filterParams: {
          newRowsAction: 'keep',
        },
      },
      {field: 'id', headerName: 'Id', sortable: true, filter: false, maxWidth: 100, minWidth: 60},
      {field: 'reference_id', headerName: 'Invoice ID', sortable: true, filter: false},
      {
        field: 'partner.company_name',
        headerName: 'Network',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'movement.reference_id',
        headerName: 'Movement',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'paid_status',
        headerName: 'Status',
        valueFormatter: params => params.data !== undefined ? this.paymentStatusFormatter(params.data.is_paid) : '',
        sortable: true,
        filter: false,
      },
      {
        field: 'transactions_sum',
        headerName: 'Amount',
        valueFormatter: params => params.data !== undefined ?
          this.currencyFormatter(params.data.amount, '$') : '',
        sortable: true,
        filter: false,
      },
      {
        field: 'action', sortable: true, filter: false, minWidth: 150,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'View Pdf',
            href: 'primary-invoices/',
            hrefFormat: true,
            type: 'download',
          },
        ],
      },
    ];
    this.defaultColDef = {resizable: true};
    this.rowSelection = 'multiple';
  }

  paymentStatusFormatter(sattus) {
    let status = 'Unpaid';
    if (sattus === 1) {
      status = 'Paid';
    }
    return status;
  }
  data = [];

  ngOnInit(): void {

  }
}
