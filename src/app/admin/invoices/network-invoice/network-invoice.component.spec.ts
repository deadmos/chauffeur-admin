import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkInvoiceComponent } from './network-invoice.component';

describe('NetworkInvoiceComponent', () => {
  let component: NetworkInvoiceComponent;
  let fixture: ComponentFixture<NetworkInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetworkInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
