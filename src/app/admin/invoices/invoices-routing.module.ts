import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoicesComponent } from './invoices.component';
import { TransactionsComponent } from '../transactions/transactions.component';
import {NetworkInvoiceComponent} from './network-invoice/network-invoice.component';

const routes: Routes = [
    {
        path: 'driver',
        component: InvoicesComponent,
    },
    {
        path: 'primary',
        component: NetworkInvoiceComponent,
    },
    {
        path: ':id/transactions',
        component: TransactionsComponent,
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class InvoicesRoutingModule { }
