import {Component, OnInit} from '@angular/core';
import {MENUS} from '../transactions/transactions-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NbThemeService} from '@nebular/theme';
import {AgDatatable} from '../admin.extends';

@Component({
  selector: 'ngx-earnings',
  templateUrl: './earnings.component.html',
  styleUrls: ['./earnings.component.scss'],
})
export class EarningsComponent extends  AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  id: any;
  name: any;
  menus = MENUS;
  url = 'transactions';
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Earning List',
      icon: 'fas fa-money',
    },
  ];
  headerData = {
    title: 'Earnings',
    action: true,
    actionLink: '/networks/create',
    actionTitle: 'Add New Network',
  };
  constructor(
    private http: HttpClient,
    protected admin: AdminService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    protected themeService: NbThemeService,
  ) {
    super();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params.id;
    });
    this.columnDefs = [
      {
        field: 'globalSearch',
        hide: true,
        initialHide: true,
        filter: 'agTextColumnFilter',
        filterParams: {
          newRowsAction: 'keep',
        },
      },
      {field: 'id', headerName: 'Id', sortable: true, filter: false, maxWidth: 100, minWidth: 60},
      {
        field: 'reference_id',
        headerName: 'Transaction ID',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'booking_reference_id',
        headerName: 'Booking ID',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'movement_reference_id',
        headerName: 'Movement ID',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'driver.user.name',
        searchField: 'driver_users.name',
        headerName: 'Driver',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'network.company_name',
        headerName: 'Network',
        searchField: 'partners.company_name',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'primary_driver_commission',
        headerName: 'Commission',
        valueFormatter: params => {
          if (params.data !== undefined) {
            return this.commissionFormatter(params.data);
          }
        },
        sortable: true,
        filter: false,
      },
      {
        field: 'full_amount',
        headerName: 'Full Amount',
        valueFormatter: params => {
          if (params.data !== undefined) {
            return  this.currencyFormatter(params.data.full_amount, '$');
          }
        },
        sortable: true,
        filter: false,
      },
      {
        field: 'driver_amount',
        headerName: 'Driver Amount',
        valueFormatter: params => {
          if (params.data !== undefined) {
            return  this.currencyFormatter(params.data.driver_amount, '$');
          }
        },
        sortable: true,
        filter: false,
      },
      {
        field: 'network_amount',
        headerName: 'Network Amount',
        valueFormatter: params => {
          if (params.data !== undefined) {
            return  this.currencyFormatter(params.data.network_amount, '$');
          }
        },
        sortable: true,
        filter: false,
      },
    ];
    this.defaultColDef = {resizable: true};
    this.rowSelection = 'multiple';
  }

  commissionFormatter(data) {
    return 'D: $' + data.primary_driver_commission + ', ' + 'N: $' + data.primary_network_commission;
  }

  ngOnInit(): void {

  }
}
