import {Component, OnInit, ViewChild} from '@angular/core';
import {MENUS} from '../areas-menu';
import {AdminService} from '../../admin.service';
import {MapService} from '../map.service';
import * as turf from '@turf/turf';
@Component({
  selector: 'ngx-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {
  menus = MENUS;
  @ViewChild('map') map;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Test',
      icon: 'fa fa-map',
    },
  ];
  headerData = {
    title: 'Test',
    action: false,
  };
  options = {
    center: <any>new google.maps.LatLng(
      24,
      12),
    zoom: <any>8,
    disableDefaultUI: true,
  };
currRoute;
  routePathPoly;
  carMarker;
  totalDistance = 0;
  completedDistance = 0;
  remainingDistance = 0;
  totalDuration = 3900;
  remainingDuration = 0;
  totalPercentage: any = 0;
  distToLine: any = 0;

  constructor(    private admin: AdminService , private mapService: MapService) { }

  ngOnInit(): void {
    this.admin
      .get('map-config')
      .subscribe((data) => {
         // this.options = this.mapService.formatCountryOption(data['country']);
        this.plotRoute(data['route']);
      });

  }
  plotRoute(route) {
      const Coordinates = [];
      const bounds = new google.maps.LatLngBounds();
       route.overview_path.forEach(function(value) {
           Coordinates.push({lat: value.lat, lng: value.lng});
           bounds.extend(new google.maps.LatLng(value.lat, value.lng));
       });
       const driver_icon = {
         path:
           'M29.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759   c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z    M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713   v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336   h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805',
         fillColor: '#000',
         fillOpacity: 1,
         scale: 0.5,
         strokeColor: '#bbb',
         strokeWeight: 0.3,
         anchor: new google.maps.Point(23, 0),
       };
      const flightPath = new google.maps.Polyline({
        path: Coordinates,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2,
      });
    const icon_url =   'https://vip.test/assets/partner/dist/img/start.svg';
    const icon = {
      url: icon_url,
      size: new google.maps.Size(35, 35),
      scaledSize: new google.maps.Size(35, 35),
    };
    const marker = new google.maps.Marker({
      map: this.map.googleMap,
      icon: icon,
      title: route.start,
      position: new google.maps.LatLng(route.overview_path[1], route.overview_path[0]),
      draggable: true,
      animation: google.maps.Animation.DROP,
    });
    const geocoder = new google.maps.Geocoder;
    const points = [];
    route.overview_path.forEach(function (point) {
      points.push([point.lng, point.lat]);
    });
    const line = turf.lineString(points);
    this.totalDistance = turf.length(line, {units: 'miles'});
    const _this = this;
    google.maps.event.addListener(marker, 'dragend', function() {
      geocoder.geocode({'location': marker.getPosition()}, function(results, status) {
        if (status === 'OK') {
          marker.setTitle(results[0].formatted_address);
          const pt = turf.point([marker.getPosition().lng(), marker.getPosition().lat()]);
          const snapped = turf.pointOnLine(line, pt, {units: 'miles'});
          const splitter = turf.point(snapped.geometry.coordinates);
          const full_split = turf.lineSplit(line, splitter);
          _this.completedDistance = turf.length(full_split.features[0], {units: 'miles'});
          _this.remainingDistance = turf.length(full_split.features[1], {units: 'miles'});
          _this.totalPercentage = ((_this.completedDistance / _this.totalDistance) * 100).toFixed(2);
          _this.remainingDuration = (_this.totalDuration / 100) * (100 - _this.totalPercentage);
          _this.distToLine = snapped.properties.dist.toFixed(2);
          const routePath = [];
          let split;
          if (_this.currRoute === undefined) {
             split = turf.lineSplit(line, splitter);
            split.features[0].geometry.coordinates.forEach(function (value) {
              routePath.push({lat: value[1], lng: value[0]});
            });
            _this.currRoute = split.features[1];
          } else {
             split = turf.lineSplit(turf.lineString(points), splitter);
            if (split.features.length > 1) {
              split.features[0].geometry.coordinates.forEach(function (value) {
                routePath.push({lat: value[1], lng: value[0]});
              });
              _this.currRoute = split.features[1];
            }
          }
          if (split.features.length > 1) {
            _this.carMarker.setMap(null);
            if (_this.routePathPoly !== undefined) {
              _this.routePathPoly.setMap(null);
            }
            const infowindow = new google.maps.InfoWindow({

            });
            _this.routePathPoly = new google.maps.Polyline({
              path: routePath,
              geodesic: true,
              strokeColor: '#FF0000',
              strokeOpacity: 0.0,
              strokeWeight: 2,
              icons: [
                {
                  icon: driver_icon,
                  offset: '0%',
                },
              ],

            });
            infowindow.open(_this.map.googleMap, marker);
            _this.routePathPoly.setMap(_this.map.googleMap);
            _this.animateCircle(_this.routePathPoly);
          }
        }
      });
    });
      flightPath.setMap(this.map.googleMap);
      this.carMarker =  new google.maps.Marker({
      map: this.map.googleMap,
      icon: driver_icon,
      title: 'test',
      position: new google.maps.LatLng(route.overview_path[1], route.overview_path[0]),
    });
      this.map.googleMap.fitBounds(bounds);

    }
  animateCircle(line) {
    let count = 0;
    const intervalId = window.setInterval(() => {
      count = (count + 1) % 100;
      const icons = line.get('icons');
      icons[0].offset = count / 1 + '%';
      line.set('icons', icons);
      if (count === 99) {
        window.clearInterval(intervalId);
      }
    }, 40);
  }
   secondsToHms(d) {
    d = Number(d);
    const h = Math.floor(d / 3600);
    const m = Math.floor(d % 3600 / 60);
    const s = Math.floor(d % 3600 % 60);

    const hDisplay = h > 0 ? h + (h === 1 ? ' hour, ' : ' hours, ') : '';
    const mDisplay = m > 0 ? m + (m === 1 ? ' minute, ' : ' minutes, ') : '';
    const sDisplay = s > 0 ? s + (s === 1 ? ' second' : ' seconds') : '';
    return hDisplay + mDisplay + sDisplay;
  }


}
