
import {Component, ViewChild, OnInit, ElementRef, ChangeDetectorRef} from '@angular/core';
import {MENUS} from '../areas-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import Wkt from 'wicket/wicket.js';
import 'wicket/wicket-gmap3.js';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {NotyService} from '../../services/noty.service';
import {Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'ngx-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss'],
})

export class CreateEditComponent implements OnInit {
  @ViewChild('searchInput') searchInput: ElementRef;
  @ViewChild('map') map;
  @ViewChild('autoSearch') autoSearch;
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Area List',
      link: '/areas',
      icon: 'fa fa-map',
    },
    {
      title: 'Area Create',
      icon: 'fa fa-plus',
    },
  ];
  headerData = {
    title: 'Area Create',
    action: false,
  };
  options = {
    center: <any>new google.maps.LatLng(
      24,
      12),
    zoom: <any>8,
    disableDefaultUI: true,
  };
  area: any =  {
    name: '',
    routes: '',
    boundingbox: '',
    autoSearch: true,
  };
  found_polygons = [];
  plotted_polygons = [];
  created_polygons = [];
  current_plotted = 0;
  newPolyCounts = 0;
  country;
  editId;
  editMode = false;
  autocomplete;
  autoCompleteOptions = {
    types: ['geocode'],
    componentRestrictions: {country: 'us'},
  };
  polyOption = {
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#3b3862',
    fillOpacity: 0.55,
    editable: false,
  };
  drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.POLYGON,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [google.maps.drawing.OverlayType.POLYGON],
    },
    polygonOptions: {
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#3b3862',
      fillOpacity: 0.55,
      editable: true,
      draggable: true,

    },
    markerOptions: {
      draggable: true,
    },

  });
  url = 'https://nominatim.openstreetmap.org/search.php?q=_placeholder_&polygon_geojson=1&format=json';

  constructor(
    private http: HttpClient,
    private admin: AdminService,
    private noty: NotyService,
    private router: Router,
    private a_router: ActivatedRoute,
    private cdr: ChangeDetectorRef) {
    if (this.router.url === '/areas/create') {
      this.editMode = false;
    } else {
      this.editMode = true;
      this.editId = this.a_router.snapshot.params.id;
    }
  }

  ngOnInit(): void {
    if (this.editMode) {
      this.headerData.title = 'Area Edit';
      this.breadcrumbs[2] = {title: 'Area Edit', icon: 'fa fa-pen'};
    } else {
      this.headerData.title = 'Area Create';
    }
    this.http
      .get(
        this.admin.getFullUrl('map-config'),
        {headers: this.admin.getHeader()},
      )
      .subscribe((data) => {
        this.country = data['country'];
        this.country.center = new google.maps.LatLng(
          this.country.lat_lng.coordinates[1],
          this.country.lat_lng.coordinates[0]);
        this.country.zoom = this.country.zoom_level;
        this.country.disableDefaultUI = true;
        this.options = this.country;
        this.autoCompleteOptions.componentRestrictions.country = this.country.google_map_short_name;
        this.autocomplete = new google.maps.places.Autocomplete(
          this.searchInput.nativeElement,
          this.autoCompleteOptions);
        const that = this;
        this.autocomplete.addListener('place_changed', function () {
          const place = that.autocomplete.getPlace();
          that.area.name = place.formatted_address;
          if (!place.geometry) {
            return;
          }
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            that.map.fitBounds(place.geometry.viewport);
          } else {
            that.options.center = place.geometry.location;
            that.options.zoom = 17;  // Why 17? Because it looks good.
          }
          that.clearAllPolygons();
          if (!that.autoSearch.checked) {
            that.found_polygons = [];
            that.cdr.detectChanges();
            return false;
          }
          that.getPolygons(that.searchInput.nativeElement.value);
        });
      });
    if (this.editMode) {
      this.http
        .get(
          this.admin.getFullUrl('areas/' + this.editId + '/edit'),
          {headers: this.admin.getHeader()},
        )
        .subscribe((data) => {
          this.area = data;
          this.found_polygons.push(data);
          this.plotPolygon(data, this.formatPolygon(data));
          this.setBoundToMap(data);
        });
    }
  }

  clearAllPolygons() {
    this.plotted_polygons.forEach(function (value, key) {
      value.setMap(null);
    });
    this.plotted_polygons = [];
  }

  init_draw_tools() {
    if (this.drawingManager.getDrawingMode() === null) {
      this.drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
      this.drawingManager.setOptions({
        drawingControl: true,
      });
    } else {
      this.drawingManager.setMap(this.map.googleMap);
      const that = this;
      google.maps.event.addListener(this.drawingManager, 'polygoncomplete', function (polygon) {
        polygon.bob = Math.random().toString(36).substring(7);
        const contentString = '<div class="info-content">' +
          '<button type="button" id="delete-drown-' + polygon.bob + '" data-index="' +
          polygon.bob + '" >Remove</button>' +
          '</div>';
        const infowindow = new google.maps.InfoWindow({
          content: contentString,
        });
        google.maps.event.addListenerOnce(infowindow, 'domready', () => {
          document.getElementById('delete-drown-' + polygon.bob).addEventListener('click', () => {
            infowindow.close();
            polygon.setMap(null);
            that.removeCreatedPolygon(polygon.bob);
          });
        });
        that.created_polygons.push(polygon);
        google.maps.event.addListener(polygon, 'click', function (event) {
          infowindow.setPosition(event.latLng);
          infowindow.open(that.map.googleMap);
        });
        google.maps.event.addListener(polygon, 'dragend', function (event) {
        });

      });
      google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function (event) {
        that.drawingManager.setDrawingMode(null);
        that.drawingManager.setOptions({
          drawingControl: false,
        });
      });
    }

  }

  getPolygons(value) {
    this.http
      .get(this.url.replace('_placeholder_', value))
      .subscribe((data: any) => {
        const results = data.filter(function (ele, index) {
          return ele.osm_type === 'relation';
        });
        this.found_polygons = results;
        if (results.length === 0) {
          return;
        }
        this.found_polygons = results;
        this.cdr.detectChanges();
        this.plotPolygon(results[0], this.formatPolygon(results[0]));
        this.setBoundToMap(results[0]);
      });
  }

  formatPolygon(rawPoly) {
    const wktObj = new Wkt.Wkt();
    if (rawPoly.geojson !== undefined) {
      return wktObj.read(JSON.stringify(rawPoly.geojson));
    }
    return false;
  }

  removeCreatedPolygon(polyId) {
    this.created_polygons = this.created_polygons.filter(function (value) {
      return value.bob !== polyId;
    });
  }

  saveArea() {

    if (this.plotted_polygons.length > 0 || this.created_polygons.length > 0) {
      this.populatePoly();
      if (this.editMode) {
        this.http
          .put(
            this.admin.getFullUrl('areas/' + this.editId),
            this.area,
            {headers: this.admin.getHeader()},
          )
          .subscribe((data: any) => {
            this.noty.success('Area Updated Successfully');
            this.router.navigate(['areas']);
          });
      } else {
        this.http
          .post(
            this.admin.getFullUrl('areas'),
            this.area,
            {headers: this.admin.getHeader()},
          )
          .subscribe((data: any) => {
            this.noty.success('Area Stored Successfully');
            this.router.navigate(['areas']);
          });
      }

    } else {
      Swal.fire(
        'Polygon Issue',
        'No Polygon found in Map.. Please Draw or Search for a Polygon',
      );
    }

  }

  notValid(name) {
    return name.invalid && (name.dirty || name.touched);
  }

  valid(name) {
    return name.valid;
  }

  populatePoly() {
    const wkt = new Wkt.Wkt();
    this.plotted_polygons.forEach(function (value, key) {
      if (key === 0) {
        wkt.fromObject(value);
      } else {
        const wkt2 = new Wkt.Wkt();
        wkt.merge(wkt2.fromObject(value));
      }
    });
    this.area.routes = wkt.write();
    this.area.boundingbox = this.getMapBoundBox();
  }

  setBoundToMap(poly) {

    const southWest = new google.maps.LatLng(poly.boundingbox[0], poly.boundingbox[2]);
    const northEast = new google.maps.LatLng(poly.boundingbox[1], poly.boundingbox[3]);
    this.map.fitBounds(new google.maps.LatLngBounds(southWest, northEast));
  }

  plotToMap(poly) {
    if (this.current_plotted !== poly.key) {
      this.clearAllPolygons();
      this.current_plotted = poly.key;
      this.plotPolygon(poly.value, this.formatPolygon(poly.value));
      this.setBoundToMap(poly.value);
    }

  }

  getMapBoundBox() {
    this.map.googleMap.setZoom(this.map.googleMap.getZoom() + 1);
    const bound = this.map.googleMap.getBounds();
    this.map.googleMap.setZoom(this.map.googleMap.getZoom() - 1);
    const ne = bound.getNorthEast();
    const sw = bound.getSouthWest();
    return '["' + sw.lat() + '","' + ne.lat() + '","' + sw.lng() + '","' + ne.lng() + '"]';
  }

  plotPolygon(object, polygonWkt, editable = false) {
    const that = this;
    if (object.geojson.type === 'Polygon') {
      const triangleCoords = [];
      object.geojson.coordinates[0].forEach(function (value5, key5) {
        triangleCoords.push({lng: value5[0], lat: value5[1]});
      });
      const polygon = new google.maps.Polygon({
        paths: triangleCoords,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#3b3862',
        fillOpacity: 0.55,
        editable: false,
      });
      that.plotted_polygons.push(polygon);
      polygon.setMap(this.map.googleMap);
    } else {
      const polyArray = polygonWkt.toObject(this.polyOption);
      polyArray.forEach(function (value22, key) {
        that.plotted_polygons.push(value22);
        value22.setMap(that.map.googleMap);
      });
    }
  }

}
