import { Component, OnInit } from '@angular/core';
import { MENUS } from '../areas-menu';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';
import { ActionsComponent } from 'app/admin/common/actions/actions.component';
import {AgDatatable} from '../../admin.extends';
import {NbThemeService} from '@nebular/theme';

@Component({
    selector: 'ngx-location-types',
    templateUrl: './location-types.component.html',
    styleUrls: ['./location-types.component.scss'],
})
export class LocationTypesComponent extends AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  menus = MENUS;
  url = 'location-type';
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Location Type List',
            icon: 'fa fa-map',
        },
    ];
    headerData = {
        title: 'Location Type List',
        action: true,
        actionLink: '/areas/location-types/create',
        actionTitle: 'Add New Location Type',
    };
    constructor(
      protected http: HttpClient,
      protected admin: AdminService,
      protected themeService: NbThemeService,
      protected router: Router) {
      super();
        this.columnDefs = [
          {
            field: 'globalSearch',
            hide: true,
            initialHide: true,
            filter: 'agTextColumnFilter',
            filterParams: {
              newRowsAction: 'keep',
            },
          },
            {
              field: 'id',
              sortable: true,
              filter: false,
              maxWidth: 60 ,
            },
            {
                field: 'title',
                sortable: true,
                filter: true,
                filterParams: {
                    filterOptions: ['contains', 'equals'],
                    defaultOption: 'contains',
                    suppressAndOrCondition: true,
                },
                minWidth: 400,
            },
            {
                field: 'type',
                sortable: true,
                filter: true,
                filterParams: {
                    filterOptions: ['contains', 'equals'],
                    defaultOption: 'contains',
                    suppressAndOrCondition: true,
                },
            },
            {
                field: 'locations_count',
                headerName: 'Locations',
                sortable: true,
            },
            {
                field: 'action', sortable: true, filter: false, maxWidth: 150,
                cellRendererFramework: ActionsComponent,
                cellRendererParams: [
                    {
                        title: 'Edit Page',
                        href: '/areas/location-types/edit',
                        type: 'edit',
                    },
                    {
                        title: 'Delete Page',
                        href: 'location-type/',
                        type: 'delete',
                        confirm: {
                            title: 'Are you sure want to delete?',
                            successMessage: 'Location type deleted successfully',
                        },
                    },
                ],
            },
        ];
        this.defaultColDef = { resizable: true };
        this.rowSelection = 'multiple';
    }

    ngOnInit(): void {
    }

}
