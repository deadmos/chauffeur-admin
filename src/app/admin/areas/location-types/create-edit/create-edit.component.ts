import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NotyService } from 'app/admin/services/noty.service';

@Component({
    selector: 'ngx-create-edit',
    templateUrl: './create-edit.component.html',
    styleUrls: ['./create-edit.component.scss'],
})
export class CreateEditComponent implements OnInit {
    id: number;
    menus = [];
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Location Type List',
            link: '/areas/location-types',
            icon: 'fa fa-map',
        },
        {
            title: 'Detail',
            icon: 'fa fa-file',
        },
    ];
    locationTypeData: any = { title: ''};
    types: any = [];
    constructor(
        private activatedRoute: ActivatedRoute,
        private noty: NotyService,
        private router: Router,
        private admin: AdminService) {
        this.activatedRoute.params.subscribe((params: Params) => { this.id = params.id; });
        if (this.id) {
            this.admin.get(`location-type/${this.id}`).subscribe((responseData) => {
                this.locationTypeData = responseData['data']['location_type'];
                this.types = responseData['data']['types'];
            });
        } else {
            this.admin.get(`location-type-config`).subscribe((responseData) => {
                this.types = responseData['data'];
            });
        }
    }
    ngOnInit(): void {
    }
    typeChange(type) {
        this.locationTypeData.type = type;
    }
    showLoader() {
        Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading();
            },
        });
    }
    submitForm() {
        let url = 'location-type';
        if (this.locationTypeData.id !== undefined) {
            url = `location-type/${this.locationTypeData.id}`;
            this.locationTypeData._method = 'put';
        } else {
            this.locationTypeData._method = 'post';
        }

        this.showLoader();
        this.admin.post(url, this.locationTypeData).subscribe({
            next: (res) => {
                const response: any = res;
                this.noty.success(response.message);
                Swal.close();
                this.router.navigate(['areas/location-types']);
            },
            error: (error) => {
                Swal.close();
                this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
            },
        });

    }

    notValid(name) {
      return name.invalid && (name.dirty || name.touched);
    }
    valid(name) {
      return name.valid;
    }

}
