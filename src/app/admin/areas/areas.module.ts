import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreasRoutingModule } from './areas-routing.module';
import { AreasComponent } from './areas.component';
import {AgGridModule} from 'ag-grid-angular';
import {NbButtonModule, NbCardModule, NbCheckboxModule, NbFormFieldModule, NbInputModule, NbListModule, NbSelectModule, NbToggleModule} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import {AdminModule} from '../admin.module';
import { LocationsComponent } from './locations/locations.component';
import { LocationTypesComponent } from './location-types/location-types.component';
import { CreateEditComponent } from './create-edit/create-edit.component';
import { CreateEditComponent  as LocationTypeCreateEditComponent} from './location-types/create-edit/create-edit.component';
import { CreateEditComponent  as LocationCreateEditComponent} from './locations/create-edit/create-edit.component';
import {GoogleMapsModule} from '@angular/google-maps';
import {FormsModule} from '@angular/forms';
import { TestComponent } from './test/test.component';
import { DriverZoneComponent } from './driver-zone/driver-zone.component';
import { ZoneCreateEditComponent } from './driver-zone/zone-create-edit/zone-create-edit.component';


@NgModule({
  declarations: [
    AreasComponent,
    LocationsComponent,
    LocationCreateEditComponent,
    LocationTypesComponent,
    LocationTypeCreateEditComponent,
    CreateEditComponent,
    TestComponent,
    DriverZoneComponent,
    ZoneCreateEditComponent],
  imports: [
    CommonModule,
    AreasRoutingModule,
    AgGridModule,
    NbCardModule,
    AdminThemeModule,
    NbSelectModule,
    NbToggleModule,
    NbButtonModule,
    AdminModule,
    NbInputModule,
    NbCheckboxModule,
    GoogleMapsModule,
    NbListModule,
    FormsModule,
    NbFormFieldModule,
  ],
})
export class AreasModule { }
