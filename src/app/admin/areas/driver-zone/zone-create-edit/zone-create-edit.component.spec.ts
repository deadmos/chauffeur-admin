import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneCreateEditComponent } from './zone-create-edit.component';

describe('ZoneCreateEditComponent', () => {
  let component: ZoneCreateEditComponent;
  let fixture: ComponentFixture<ZoneCreateEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZoneCreateEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
