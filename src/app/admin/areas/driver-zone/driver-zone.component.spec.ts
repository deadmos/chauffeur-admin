import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverZoneComponent } from './driver-zone.component';

describe('DriverZoneComponent', () => {
  let component: DriverZoneComponent;
  let fixture: ComponentFixture<DriverZoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverZoneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
