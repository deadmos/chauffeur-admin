import { Component, OnInit } from '@angular/core';
import {AgDatatable} from '../../admin.extends';
import {MENUS} from '../areas-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {Router} from '@angular/router';
import {NbThemeService} from '@nebular/theme';
import {ActionsComponent} from '../../common/actions/actions.component';

@Component({
  selector: 'ngx-driver-zone',
  templateUrl: './driver-zone.component.html',
  styleUrls: ['./driver-zone.component.scss'],
})
export class DriverZoneComponent extends AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  menus = MENUS;
  url = 'zones';
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Driver Zone List',
      icon: 'fa fa-map',
    },
  ];
  headerData = {
    title: 'Driver Zone List',
    action: true,
    actionLink: '/areas/zones/create',
    actionTitle: 'Add New Driver Zone',
  };
  constructor(
    protected http: HttpClient,
    protected admin: AdminService,
    protected router: Router,
    protected themeService: NbThemeService,
  ) {
    super();
    this.columnDefs = [
      {
        field: 'globalSearch',
        hide: true,
        initialHide: true,
        filter: 'agTextColumnFilter',
        filterParams: {
          newRowsAction: 'keep',
        },
      },
      { field: 'id', sortable: true, filter: false, maxWidth: 60 },
      {
        field: 'name',
        headerName: 'Zones',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
        minWidth: 250,
      },
      {
        field: 'area.name',
        headerName: 'Area',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
      },
      {
        field: 'action', sortable: true, filter: false, maxWidth: 150,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'Edit Zone',
            href: '/areas/zones/edit',
            type: 'edit',
          },
          {
            title: 'Delete Zone',
            href: 'zones/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete?',
              successMessage: 'Driver Zone deleted successfully',
            },
          },
        ],
      },
    ];
    this.defaultColDef = { resizable: true };
    this.rowSelection = 'multiple';
  }
  ngOnInit(): void {
  }

}
