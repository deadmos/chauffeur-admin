import { Component, OnInit } from '@angular/core';
import { MENUS } from '../areas-menu';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';
import { ActionsComponent } from 'app/admin/common/actions/actions.component';
import {AgDatatable} from '../../admin.extends';
import {NbThemeService} from '@nebular/theme';

@Component({
    selector: 'ngx-locations',
    templateUrl: './locations.component.html',
    styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent extends AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  menus = MENUS;
  url = 'location';
    breadcrumbs = [
        {
            title: 'Home',
            link: '/dashboard',
            icon: 'fa fa-home',
        },
        {
            title: 'Location List',
            icon: 'fa fa-map',
        },
    ];
    headerData = {
        title: 'Location List',
        action: true,
        actionLink: '/areas/locations/create',
        actionTitle: 'Add New Location',
    };
    constructor(
      protected http: HttpClient,
      protected admin: AdminService,
      protected router: Router,
      protected themeService: NbThemeService,
) {
  super();
  this.columnDefs = [
    {
      field: 'globalSearch',
      hide: true,
      initialHide: true,
      filter: 'agTextColumnFilter',
      filterParams: {
        newRowsAction: 'keep',
      },
    },
            { field: 'id', sortable: true, filter: false, maxWidth: 60 },
            {
                field: 'name',
                headerName: 'Locations',
                sortable: true,
                filter: true,
                filterParams: this.filterParams,
                minWidth: 250,
            },
            {
              field: 'area.name',
              headerName: 'Area',
              sortable: true,
              filter: true,
              filterParams: this.filterParams,
            },
            {
              field: 'location_type.title',
              headerName: 'Type',
              sortable: true,
              filter: true,
              filterParams: this.filterParams,
            },
            {
                field: 'action', sortable: true, filter: false, maxWidth: 150,
                cellRendererFramework: ActionsComponent,
                cellRendererParams: [
                    {
                        title: 'Edit Page',
                        href: '/areas/locations/edit',
                        type: 'edit',
                    },
                    {
                        title: 'Delete Page',
                        href: 'location/',
                        type: 'delete',
                        confirm: {
                            title: 'Are you sure want to delete?',
                            successMessage: 'Location deleted successfully',
                        },
                    },
                ],
            },
        ];
        this.defaultColDef = { resizable: true };
        this.rowSelection = 'multiple';
    }
    ngOnInit(): void {
    }

}
