import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../../admin.service';
import Wkt from 'wicket/wicket.js';
import 'wicket/wicket-gmap3.js';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {NotyService} from '../../../services/noty.service';

@Component({
  selector: 'ngx-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss'],
})
export class CreateEditComponent implements OnInit {

  @ViewChild('searchInput') searchInput: ElementRef;
  @ViewChild('map') map;
  @ViewChild('autoSearch') autoSearch;

  id: number;
  menus = [];
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Location List',
      link: '/areas/locations',
      icon: 'fa fa-map',
    },
    {
      title: 'Detail',
      icon: 'fa fa-file',
    },
  ];
  headerData = {
    title: 'Location Create',
    action: false,
  };
  options = {
    center: <any>new google.maps.LatLng(
      24,
      12),
    zoom: <any>8,
    disableDefaultUI: true,
  };
  area: any = {
    name: '',
    routes: '',
    boundingbox: '',
    autoSearch: true,
  };
  found_polygons = [];
  plotted_polygons = [];
  plotted_area_polygons = [];
  created_polygons = [];
  current_plotted = 0;
  newPolyCounts = 0;
  country;
  editId;
  editMode = false;
  locationDataStatus = false;
  autocomplete;
  autoCompleteOptions = {
    types: ['establishment'],
    componentRestrictions: {country: 'us'},
  };
  polyOption = {
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#3b3862',
    fillOpacity: 0.55,
    editable: false,
  };
  drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.POLYGON,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [google.maps.drawing.OverlayType.POLYGON],
    },
    polygonOptions: {
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#3b3862',
      fillOpacity: 0.55,
      editable: true,
      draggable: true,

    },
    markerOptions: {
      draggable: true,
    },

  });
  url = 'https://nominatim.openstreetmap.org/search.php?q=_placeholder_&polygon_geojson=1&format=json';
  locationData: any = {title: '', area_id: '', location_type_id: '', lat: '', lng: ''};
  types: any = [];
  areas: any = [];

  constructor(
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private noty: NotyService,
    private admin: AdminService,
    private cdr: ChangeDetectorRef,
  ) {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params.id;
    });
    if (this.id) {
      this.editId = this.id;
      this.editMode = true;
      this.admin.get(`location/${this.id}/edit`).subscribe((responseData) => {
        this.locationData = responseData['data']['location'];
        this.country = responseData['data']['country'];
        this.areas = responseData['data']['areas'];
        this.formatData(true);
        this.areaChange(this.locationData.area_id);
        this.plotPolygon(responseData['data']['location'], this.formatPolygon(responseData['data']['location']));
        this.setBoundToMap(responseData['data']['location']);

      });
    } else {
      this.editMode = false;
      this.admin.get(`zones/create`).subscribe((responseData: any) => {
        this.areas = responseData.data['areas'];
        this.country = responseData.data['country'];
        this.locationDataStatus = true;
        this.formatData();

      });
    }
  }

  ngOnInit(): void {
    // if (this.editMode) {
    //     this.headerData.title = 'Area Edit';
    //     this.breadcrumbs[2] = {title: 'Area Edit', icon: 'fa fa-pen'};
    //   } else {
    //     this.headerData.title = 'Area Create';
    //   }
    //   this.admin
    //     .get('locations/create'
    //     )
    //     .subscribe((data) => {

    //     });
    //   if (this.editMode) {
    //     this.http
    //       .get(
    //         this.admin.getFullUrl('areas/' + this.editId + '/edit'),
    //         {headers: this.admin.getHeader()},
    //       )
    //       .subscribe((data) => {
    //         this.area = data;
    //         this.found_polygons.push(data);
    //         this.plotPolygon(data, this.formatPolygon(data));
    //         this.setBoundToMap(data);
    //       });
    //   }
  }

  formatData(mapOption = false) {
    this.country.center = new google.maps.LatLng(
      this.country.lat_lng.coordinates[1],
      this.country.lat_lng.coordinates[0]);
    this.country.zoom = this.country.zoom_level;
    this.country.disableDefaultUI = true;
    if (mapOption === false) {
      this.options = this.country;
    }
    this.autoCompleteOptions.componentRestrictions.country = this.country.google_map_short_name;
    this.autocomplete = new google.maps.places.Autocomplete(
      this.searchInput.nativeElement,
      this.autoCompleteOptions);
    const that = this;
    this.autocomplete.addListener('place_changed', function () {
      const place = that.autocomplete.getPlace();
      that.locationData.name = place.name;
      that.locationData.lat = place.geometry.location.lat();
      that.locationData.lng = place.geometry.location.lng();
      if (!place.geometry) {
        return;
      }
      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        that.map.fitBounds(place.geometry.viewport);
      } else {
        that.options.center = place.geometry.location;
        that.options.zoom = 17;  // Why 17? Because it looks good.
      }
      that.clearAllPolygons();
      if (!that.autoSearch.checked) {
        that.found_polygons = [];
        that.cdr.detectChanges();
        return false;
      }
      that.getPolygons(place.name);
    });
  }

  typeChange(type) {
    this.locationData.type = type;
  }

  areaChange(type) {
    const area = this.areas.find(function (value) {
      return value.id === type;
    });
    if (area !== undefined) {
      this.area = area;
      this.clearAreaPloygon();
      this.plotPolygon(area, this.formatPolygon(area), true);
      this.setBoundToMap(area);
    }
  }

  showLoader() {
    Swal.fire({
      title: 'Please Wait !',
      allowOutsideClick: false,
      showConfirmButton: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }

  clearAllPolygons() {
    this.plotted_polygons.forEach(function (value, key) {
      value.setMap(null);
    });
    this.plotted_polygons = [];
  }

  clearAreaPloygon() {
    this.plotted_area_polygons.forEach(function (value, key) {
      value.setMap(null);
    });
    this.plotted_area_polygons = [];
  }

  init_draw_tools() {
    if (this.drawingManager.getDrawingMode() === null) {
      this.drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
      this.drawingManager.setOptions({
        drawingControl: true,
      });
    } else {
      this.drawingManager.setMap(this.map.googleMap);
      const that = this;
      google.maps.event.addListener(this.drawingManager, 'polygoncomplete', function (polygon) {
        polygon.bob = Math.random().toString(36).substring(7);
        const contentString = '<div class="info-content">' +
          '<button type="button" id="delete-drown-' + polygon.bob + '" data-index="' +
          polygon.bob + '" >Remove</button>' +
          '</div>';
        const infowindow = new google.maps.InfoWindow({
          content: contentString,
        });
        google.maps.event.addListenerOnce(infowindow, 'domready', () => {
          document.getElementById('delete-drown-' + polygon.bob).addEventListener('click', () => {
            infowindow.close();
            polygon.setMap(null);
            that.removeCreatedPolygon(polygon.bob);
          });
        });
        that.created_polygons.push(polygon);
        google.maps.event.addListener(polygon, 'click', function (event) {
          infowindow.setPosition(event.latLng);
          infowindow.open(that.map.googleMap);
        });
        google.maps.event.addListener(polygon, 'dragend', function (event) {
        });

      });
      google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function (event) {
        that.drawingManager.setDrawingMode(null);
        that.drawingManager.setOptions({
          drawingControl: false,
        });
      });
    }

  }

  getPolygons(value) {
    this.http
      .get(this.url.replace('_placeholder_', value))
      .subscribe((data: any) => {
        const results = data.filter(function (ele, index) {
          return ele.osm_type === 'relation';
        });
        this.found_polygons = results;
        if (results.length === 0) {
          return;
        }
        this.found_polygons = results;
        this.cdr.detectChanges();
        this.plotPolygon(results[0], this.formatPolygon(results[0]));
        this.setBoundToMap(results[0]);
      });
  }

  formatPolygon(rawPoly) {
    const wktObj = new Wkt.Wkt();
    if (rawPoly.geojson !== undefined) {
      return wktObj.read(JSON.stringify(rawPoly.geojson));
    }
    return false;
  }

  removeCreatedPolygon(polyId) {
    this.created_polygons = this.created_polygons.filter(function (value) {
      return value.bob !== polyId;
    });
  }

  submitForm() {
    if (this.plotted_polygons.length > 0 || this.created_polygons.length > 0) {
      this.populatePoly();

      if (this.editMode) {
        this.http
          .put(
            this.admin.getFullUrl('location/' + this.editId),
            this.locationData,
            {headers: this.admin.getHeader()},
          )
          .subscribe((data: any) => {
            this.noty.success('Locations Updated Successfully');
            this.router.navigate(['areas/locations']);
          });
      } else {
        this.admin
          .post('location', this.locationData)
          .subscribe((data: any) => {
            this.noty.success('Locations Stored Successfully');
            this.router.navigate(['areas/locations']);
          });
      }

    } else {
      Swal.fire(
        'Polygon Issue',
        'No Polygon found in Map.. Please Draw or Search for a Polygon',
      );
    }
  }

  notValid(name) {
    return name.invalid && (name.dirty || name.touched);
  }
  valid(name) {
    return name.valid;
  }

  populatePoly() {
    if (this.plotted_polygons.length === 0 && this.created_polygons.length === 0) {
      this.admin.noty('No Polygon Found', true);
    }
    const wkt = new Wkt.Wkt();
    if (this.plotted_polygons.length > 0) {
      this.plotted_polygons.forEach(function (value, key) {
        if (key === 0) {
          wkt.fromObject(value);
        } else {
          const wkt2 = new Wkt.Wkt();
          wkt.merge(wkt2.fromObject(value));
        }
      });
    } else {
      if (this.created_polygons.length > 0) {
        this.created_polygons.forEach(function (value, key) {
          if (key === 0) {
            wkt.fromObject(value);
          } else {
            const wkt2 = new Wkt.Wkt();
            wkt.merge(wkt2.fromObject(value));
          }
        });
      }
    }

    this.locationData.routes = wkt.write();
    this.locationData.boundingbox = this.getMapBoundBox();
  }

  setBoundToMap(poly) {
    const southWest = new google.maps.LatLng(poly.boundingbox[0], poly.boundingbox[2]);
    const northEast = new google.maps.LatLng(poly.boundingbox[1], poly.boundingbox[3]);
    this.map.fitBounds(new google.maps.LatLngBounds(southWest, northEast));
  }

  plotToMap(poly) {
    if (this.current_plotted !== poly.key) {
      this.clearAllPolygons();
      this.current_plotted = poly.key;
      this.plotPolygon(poly.value, this.formatPolygon(poly.value));
      this.setBoundToMap(poly.value);
    }

  }

  getMapBoundBox() {
    this.map.googleMap.setZoom(this.map.googleMap.getZoom() + 1);
    const bound = this.map.googleMap.getBounds();
    this.map.googleMap.setZoom(this.map.googleMap.getZoom() - 1);
    const ne = bound.getNorthEast();
    const sw = bound.getSouthWest();
    return '["' + sw.lat() + '","' + ne.lat() + '","' + sw.lng() + '","' + ne.lng() + '"]';
  }

  plotPolygon(object, polygonWkt, area = false) {
    const that = this;
    if (object.geojson.type === 'Polygon') {
      const triangleCoords = [];
      object.geojson.coordinates[0].forEach(function (value5, key5) {
        triangleCoords.push({lng: value5[0], lat: value5[1]});
      });
      let filleColler;
      let strokeWeight;
      let editable;
      if (area) {
        filleColler = '#f7f9fc';
        strokeWeight = 1;
        editable = false;
      } else {
        filleColler = '#3b3862';
        strokeWeight = 2;
        editable = true;
      }
      const polygon = new google.maps.Polygon({
        paths: triangleCoords,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: strokeWeight,
        fillColor: filleColler,
        fillOpacity: 0.55,
        editable: editable,
      });
      if (area) {
        that.plotted_area_polygons.push(polygon);
      } else {
        that.plotted_polygons.push(polygon);
      }

      polygon.setMap(this.map.googleMap);
    } else {
      const polyArray = polygonWkt.toObject(this.polyOption);
      polyArray.forEach(function (value22, key) {
        if (area) {
          that.plotted_area_polygons.push(value22);
        } else {
          that.plotted_polygons.push(value22);
        }

        value22.setMap(that.map.googleMap);
      });
    }
  }

}
