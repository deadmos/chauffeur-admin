

export const MENUS = [
    {
        id: 'area',
        title: 'Coverage Areas',
        icon: 'fa fa-map',
        link: '/areas',
    },
    {
        id: 'location_type',
        title: 'Location Type Manager',
        icon: 'fa fa-list',
        link: '/areas/location-types',
    },
    {
        id: 'location',
        title: 'Location Manager',
        icon: 'fa fa-map-marker',
        link: '/areas/locations',
    },
    {
        id: 'zone',
        title: 'Driver Zone Manager',
        icon: 'fa fa-map-marker',
        link: '/areas/zones',
    },
];
