import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AreasComponent } from './areas.component';
import { LocationsComponent } from './locations/locations.component';
import { LocationTypesComponent } from './location-types/location-types.component';
import { CreateEditComponent } from './create-edit/create-edit.component';
import { CreateEditComponent as LocationTypeCreateEditComponent } from './location-types/create-edit/create-edit.component';
import { CreateEditComponent as LocationCreateEditComponent } from './locations/create-edit/create-edit.component';
import { TestComponent } from './test/test.component';
import {DriverZoneComponent} from './driver-zone/driver-zone.component';
import {ZoneCreateEditComponent} from './driver-zone/zone-create-edit/zone-create-edit.component';

const routes: Routes = [
    {
        path: '',
        component: AreasComponent,
    },
    {
        path: 'create',
        component: CreateEditComponent,
    },
    {
        path: 'edit/:id',
        component: CreateEditComponent,
    },
    {
        path: 'locations',
        component: LocationsComponent,
    },
    {
        path: 'locations/create',
        component: LocationCreateEditComponent,
    },
    {
        path: 'locations/edit/:id',
        component: LocationCreateEditComponent,
    },
    {
        path: 'zones',
        component: DriverZoneComponent,
    },
    {
        path: 'zones/create',
        component: ZoneCreateEditComponent,
    },
    {
        path: 'zones/edit/:id',
        component: ZoneCreateEditComponent,
    },
    {
        path: 'location-types',
        component: LocationTypesComponent,
    },
    {
        path: 'location-types/create',
        component: LocationTypeCreateEditComponent,
    },
    {
        path: 'location-types/edit/:id',
        component: LocationTypeCreateEditComponent,
    },
    {
        path: 'test',
        component: TestComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AreasRoutingModule { }
