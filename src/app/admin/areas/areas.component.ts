import { Component, OnInit } from '@angular/core';
import {MENUS} from './areas-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {Router} from '@angular/router';
import {ActionsComponent} from '../common/actions/actions.component';
import {AgDatatable} from '../admin.extends';
import {NbThemeService} from '@nebular/theme';

@Component({
  selector: 'ngx-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss'],
})
export class AreasComponent extends AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  menus = MENUS;
  url = 'areas';
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Area List',
      icon: 'fa fa-map',
    },
  ];
  headerData = {
    title: 'Area List',
    action: true,
    actionLink: '/areas/create',
    actionTitle: 'Add New Area',
  };

  constructor(
    protected http: HttpClient,
    protected admin: AdminService,
    protected router: Router,
    protected themeService: NbThemeService,
  ) {
    super();
    this.columnDefs = [
      {
        field: 'globalSearch',
        hide: true,
        initialHide: true,
        filter: 'agTextColumnFilter',
        filterParams: {
          newRowsAction: 'keep',
        },
      },
      {
        field: 'id',
        headerName: 'Area Id',
        sortable: true,
        filter: false,
        maxWidth: 160,
      },
      {
        field: 'name',
        headerName: 'Area',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
        minWidth: 260,
      },
      {
        field: 'locations_count',
        headerName: 'Location Count',
        sortable: true,
        filter: true,
        filterParams: this.filterParams,
        maxWidth: 160,
      },
      {
        field: 'action',
        sortable: true,
        filter: false,
        maxWidth: 150,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'Edit Area',
            href: '/areas/edit',
            type: 'edit',
          },
          {
            title: 'Delete Area',
            href: 'areas/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete?',
              successMessage: 'Area deleted successfully',
            },
          },
        ],
      },

    ];
    this.defaultColDef = { resizable: true };
    this.rowSelection = 'multiple';
  }

  ngOnInit(): void {
  }

}
