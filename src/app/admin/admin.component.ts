import { Component, OnInit } from '@angular/core';
import {NbMenuService} from '@nebular/theme';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {

  constructor(private menuService: NbMenuService, private router: Router) {  }

  onContecxtItemSelection(title) {
    if (title === 'Log out') {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('currentUserToken');
      this.router.navigateByUrl('login');
    }
}
  ngOnInit(): void {
    this.menuService.onItemClick()
      .subscribe((event) => {
        this.onContecxtItemSelection(event.item.title);
      });
  }

}
