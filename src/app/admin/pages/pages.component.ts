import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../admin.service';
import {ToggleComponent} from '../common/toggle/toggle.component';
import {ActionsComponent} from '../common/actions/actions.component';
import {Router} from '@angular/router';
import { MENUS } from './pages-menu';
import {NbThemeService} from '@nebular/theme';
import {AgDatatable} from '../admin.extends';
@Component({
  selector: 'ngx-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
})

export class PagesComponent extends AgDatatable implements OnInit {
  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  menus = MENUS;
  url = 'pages';
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Page List',
      icon: 'fa fa-file',
    },
  ];
  headerData = {
    title: 'Pages List',
    action: true,
    actionLink: '/pages/create',
    actionTitle: 'Add New Page',
  };
  constructor(
    protected http: HttpClient,
    protected admin: AdminService,
    protected router: Router,
    protected themeService: NbThemeService,
  ) {
    super();
    this.columnDefs = [
      {
        field: 'globalSearch',
        hide: true,
        initialHide: true,
        filter: 'agTextColumnFilter',
        filterParams: {
          newRowsAction: 'keep',
        },
      },
      { field: 'id', sortable: false, filter: false , maxWidth: 60},
      { field: 'name' , sortable: true, filter: true,  minWidth: 200,
        filterParams: {
          filterOptions: ['contains', 'equals'],
          defaultOption: 'contains',
          suppressAndOrCondition : true,
        },

      },
      { field: 'slug', sortable: true, filter: true,  minWidth: 150,
        filterParams: {
          filterOptions: ['contains', 'equals'],
          defaultOption: 'contains',
          suppressAndOrCondition : true,
        },
      },
      { field: 'status', sortable: true, filter: false ,  maxWidth: 120,
        cellRendererFramework: ToggleComponent,
        cellRendererParams: {name: 'status' , href: 'pages/__placeholder__/change-status'},
      },

      { field: 'menu_status', headerName: 'Menu Status' , sortable: true, filter: false,  maxWidth: 120,
        cellRendererFramework: ToggleComponent,
        cellRendererParams: {name: 'menu_status' , href: 'pages/__placeholder__/change-menu-status'},
      },
      { field: 'menu_group', headerName: 'Menu Group',  sortable: true, filter: true,  maxWidth: 150,
        filterParams: {
          filterOptions: ['contains', 'equals'],
          defaultOption: 'contains',
          suppressAndOrCondition : true,
        },
      },
      { field: 'action', sortable: true, filter: false,  maxWidth: 150,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'Edit Page',
            href: '/pages/edit',
            type: 'edit',
          },
          {
            title: 'Delete Page',
            href: 'pages/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete?',
              successMessage: 'Page deleted successfully',
            },
          },
        ],
      },
    ];
    this.defaultColDef = { resizable: true };
    this.rowSelection = 'multiple';
  }

  ngOnInit(): void {
  }

}
