import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PagesComponent} from './pages.component';
import {CreateEditComponent} from './create-edit/create-edit.component';
import {MenuComponent} from './menu/menu.component';
import {InfoComponent} from './info/info.component';
import {CommonListComponent} from './common-list/common-list.component';


const routes: Routes = [{

      path: '',
      component: PagesComponent,
    },
    {
        path: 'create',
        component: CreateEditComponent,
    },
    {
        path: 'edit/:id',
        component: CreateEditComponent,
    },
    {
        path: 'delete/:id',
        component: CreateEditComponent,
    },
    {
        path: 'menu',
        component: MenuComponent,
    },
    {
        path: 'common',
        component: CommonListComponent,
    },
    {
      path: 'info',
      component: InfoComponent,
    }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule { }
