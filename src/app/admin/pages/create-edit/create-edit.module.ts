import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NbCardModule} from '@nebular/theme';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NbCardModule,
  ],
})
export class CreateEditModule { }
