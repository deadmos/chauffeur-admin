import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from '@nebular/theme';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';
import {MENUS} from '../pages-menu';
import {ActivatedRoute, Router} from '@angular/router';
import {NotyService} from '../../services/noty.service';

@Component({
  selector: 'ngx-create-edit, ngx-form-inputs',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss'],
})

export class CreateEditComponent implements OnInit {

  page: any = {
    name: '',
    status: 1,
    menu_group: '',
    menu_status: 1,
  };
  sections;
  selects = {};
  dialogRef;
  cropperRef;
  current_sections = [];
  config = {
    aspectRatio: 1 / 1,
    viewMode: 2,
    scalable: false,
    background: false,
    zoomable: false,
  };
  files = [];
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Page List',
      link: '/pages',
      icon: 'fa fa-file',
    },
    {
      title: 'Page Create',
      icon: 'fa fa-plus',
    },
  ];
  headerData = {
    title: 'Pages create',
    action: false,
  };

  imageUrl: any = 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Bartholdi_Fountain_in_Washington%2C_D.C._2012.JPG/800px-Bartholdi_Fountain_in_Washington%2C_D.C._2012.JPG';

  constructor(
    private dialogService: NbDialogService,
    private http: HttpClient,
    private admin: AdminService,
    private router: Router,
    private route: ActivatedRoute,
    private noty: NotyService,
  ) {
  }

  open(dialog: TemplateRef<any>) {
    this.dialogRef = this.dialogService.open(dialog, {context: 'this is some additional data passed to dialog'});
  }

  openCropper(cropper: TemplateRef<any>, event) {
    this.readThis(event.target, cropper);

  }

  moveUpSection(section) {
    const curr = this.arraySearch(this.current_sections, section.id);
    if (curr !== 0) {
      this.current_sections = this.array_move(
        this.current_sections,
        curr, curr - 1);
    }

  }

  moveDownSection(section) {
    const curr = this.arraySearch(this.current_sections, section.id);
    if (curr !== (this.current_sections.length - 1)) {
      this.current_sections = this.array_move(
        this.current_sections,
        curr, curr + 1);
    }
  }

  removeSection(section) {
    this.current_sections = this.current_sections.filter(function (value) {
      return value !== section.id;
    });
  }

  array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
      let k = new_index - arr.length + 1;
      while (k--) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing
  }

  arraySearch(arr, val) {
    for (let i = 0; i < arr.length; i++)
      if (arr[i] === val)
        return i;
  }

  getSelectOptions(type) {
    let selects = [];
    if (type !== undefined && this.selects[type] !== undefined) {
      selects = this.selects[type];
    }
    return selects;

  }

  notValid(name) {
    return name.invalid && (name.dirty || name.touched);
  }
  valid(name) {
    return name.valid;
  }
  submitForm(form) {
    const data = this.admin.formatFormData(form);
    let postUrl;
    if (this.page.id === undefined) {
       postUrl = this.admin.getFullUrl('pages');
    } else {
      data.append('_method', 'PUT');
       postUrl = this.admin.getFullUrl('pages/' + this.route.snapshot.paramMap.get('id'));
    }
    this.http
      .post(
        postUrl,
        data,
        {headers: this.admin.getHeader(true)},
      )
      .subscribe((res) => {
        this.noty.success('Successfully' + (this.page.id === undefined ? 'Created Page' : 'Updated Page'));
        this.router.navigate(['pages']);
      });

  }

  layoutChanged(section) {
    let layout = section.layouts.filter(function (value) {
      return value.id === section.data.layout;
    });
    if (layout.length > 0) {
      layout = layout[0];
      if (layout.extra !== undefined && layout.extra.length > 0) {
        layout.extra.forEach(function (value) {
          if (section.fields.filter(function (nestedValue) {
            return nestedValue.id === value.id;
          }).length === 0) {
            value.field_type = 'extra';
            section.fields.push(value);
          }

        });
      } else {
        section.fields = section.fields.filter(function (value) {
          return value.field_type !== 'extra';
        });
      }
    }
  }

  getSectionByName(name) {

   return this.sections.find(obj => {
      return obj.id === name;
    });
  }
  setEditorValue(mce,section) {
    section.data.html = mce;
  }
  setEditorListValue(mce,section) {
    section.html = mce;
  }

  appendSectionHtml(section) {
    const result = this.sections.find(obj => {
      return obj.id === section;
    });
    if (result !== undefined) {
      this.current_sections.push(result.id);
      if (result.lists !== undefined && result.lists.default_count !== undefined) {
        result.listNumberArray = Array.from(Array(result.lists.default_count).keys());
        for (let i = 0; i < result.lists.default_count; i++) {
          result.lists.data[i] = {};
        }
      }
      if (this.dialogRef !== undefined) {
          this.dialogRef.close();
      }
    }

  }

  isInCurrent(sectionId) {
    return this.current_sections.indexOf(sectionId) === -1;
  }

  getSection(sectionId) {
    return this.sections.find(obj => {
      return obj.id === sectionId;
    });
  }

  closeCropper() {
    this.cropperRef.close();
  }
  addList(section) {
    section.lists.data.push({});
    section.listNumberArray.push(section.listNumberArray.length);

  }
  removeList(section) {
      section.listNumberArray.splice(-1,1)
      section.lists.data.splice(-1,1)
    // section.lists.data.push({});
    // section.listNumberArray.push(section.listNumberArray.length);

  }

  readThis(inputValue: any, cropper): void {


    const myReader: FileReader = new FileReader();
    const _this = this;
    myReader.onloadend = function (e) {
      _this.imageUrl = e.target.result;
      _this.cropperRef = _this.dialogService.open(cropper);
    };

    myReader.readAsDataURL(inputValue.files[0]);
  }

  ngOnInit(): void {

    this.http
      .get(
        this.admin.getFullUrl('pages/section-list'),
        {headers: this.admin.getHeader()},
      )
      .subscribe((data) => {
        this.sections = data['sections'];
        this.selects = data['select_list'];
        if (this.router.url !== '/pages/create') {
          this.http
            .get(
              this.admin.getFullUrl('pages/' + this.route.snapshot.paramMap.get('id') + '/edit' ),
              {headers: this.admin.getHeader()},
            )
            .subscribe((pageData: any) => {
              const _this = this;
              this.page = pageData;
              if (pageData.sections !== undefined && pageData.sections.length > 0) {
                pageData.sections.forEach(function (value, index) {
                  if (value.type === 'field') {
                    _this.appendSectionHtml(value.name);
                    const _section = _this.getSectionByName(value.name);
                    if (_section !== undefined) {
                      _section.data = value;
                      if(_section.lists !== undefined){
                        _section.lists.data = pageData.sections.filter(function(value2) {
                            return value2.name === value.name && value2.type === 'list';
                          });
                      }

                    }
                  }
                });
              }
            });
        }
      });
  }


}
