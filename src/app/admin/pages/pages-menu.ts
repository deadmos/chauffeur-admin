

export const MENUS = [
  {
    id: 'page',
    title: 'Page Manager',
    icon: 'shopping-cart-outline',
    link: '/pages',
  },
  {
    id: 'menu',
    title: 'Menu Manager',
    icon: 'shopping-cart-outline',
    link: '/pages/menu',
  },
  {
    id: 'info',
    title: 'Info Manager',
    icon: 'shopping-cart-outline',
    link: '/pages/info',
  },
  {
    id: 'list',
    title: 'Common List Manager',
    icon: 'shopping-cart-outline',
    link: '/pages/common',
  },
];
