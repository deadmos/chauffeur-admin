import { Component, OnInit } from '@angular/core';
import {MENUS} from '../pages-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';

@Component({
  selector: 'ngx-common-list',
  templateUrl: './common-list.component.html',
  styleUrls: ['./common-list.component.scss'],
})
export class CommonListComponent implements OnInit {
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Menu Manager',
      icon: 'fa fa-bars',
    },
  ];
  headerData = {
    title: 'Testimonial and Fleet Logos',
    action: false,
  };
  commonList: any = [{list: {fields: []}}];
  currentNode: any = {};
  constructor(private http: HttpClient, private admin: AdminService) {
  }
  image_url = 'http://localhost:4200/assets/images/nick.png';
  ngOnInit(): void {
    this.http
      .get(
        this.admin.getFullUrl('common-list'),
        {headers: this.admin.getHeader()},
      )
      .subscribe((data) => {
        this.commonList = data;
      });
  }
  showType(node): void {
    this.currentNode  = node;
  }
  submitForm(form) {
    const data = this.admin.formatFormData(form);
  }

}
