import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonListComponent } from './common-list.component';
import {AdminThemeModule} from '../../../@themes/admin/admin-theme.module';
import {NbButtonModule, NbCardModule, NbCheckboxModule, NbInputModule} from '@nebular/theme';
import {DndModule} from 'ng2-dnd';
import {AdminModule} from '../../admin.module';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [CommonListComponent],
  imports: [
    CommonModule,
    AdminThemeModule,
    NbCardModule,
    DndModule,
    AdminModule,
    NbCheckboxModule,
    NbButtonModule,
    NbInputModule,
    FormsModule,
  ],
})
export class CommonListModule { }
