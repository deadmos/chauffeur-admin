import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu.component';
import {AdminThemeModule} from '../../../@themes/admin/admin-theme.module';
import {DndModule} from 'ng2-dnd';
import { NbCardModule } from '@nebular/theme';
import {AdminModule} from '../../admin.module';


@NgModule({
  declarations: [MenuComponent],
  imports: [
    CommonModule,
    AdminThemeModule,
    DndModule,
    NbCardModule,
    AdminModule,
  ],
})
export class MenuModule { }
