import {Component, OnInit} from '@angular/core';
import {MENUS} from '../pages-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';

@Component({
  selector: 'ngx-admin-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent  implements OnInit {
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Menu Manager',
      icon: 'fa fa-bars',
    },
  ];
  menu_nodes: [];
  footer_nodes: [];


  constructor(private http: HttpClient, private admin: AdminService) {
  }

  ngOnInit(): void {
    this.http
      .get(
        this.admin.getFullUrl('pages/menus'),
        {headers: this.admin.getHeader()},
      )
      .subscribe((data) => {
        this.menu_nodes = data['menuData'];
        this.footer_nodes = data['footerMenuData'];
      });
  }
}
