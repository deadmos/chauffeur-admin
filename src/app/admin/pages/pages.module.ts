import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { AgGridModule } from 'ag-grid-angular';
import { CreateEditComponent } from './create-edit/create-edit.component';
import {PagesRoutingModule} from './pages-routing.module';
import {NbButtonModule, NbCardModule, NbInputModule} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import { NbSelectModule, NbToggleModule } from '@nebular/theme';
import {AdminModule} from '../admin.module';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [PagesComponent, CreateEditComponent],
  imports: [
    CommonModule,
    AgGridModule,
    PagesRoutingModule,
    NbCardModule,
    AdminThemeModule,
    NbSelectModule,
    NbToggleModule,
    AdminModule,
    NbInputModule,
    NbButtonModule,
    FormsModule,
  ],
})
export class PagesModule { }
