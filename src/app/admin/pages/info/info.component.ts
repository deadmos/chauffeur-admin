import {Component, OnInit} from '@angular/core';
import {MENUS} from '../pages-menu';
import {HttpClient} from '@angular/common/http';
import {AdminService} from '../../admin.service';

@Component({
  selector: 'ngx-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Menu Manager',
      icon: 'fa fa-bars',
    },
  ];
  headerData = {
    title: 'Pages Info',
    action: false,
  };
  info: any = {
    name: '',
    email: '',
    phone: '',
    image_url: '',
    footer_text: '',
    facebook: '',
    twitter: '',
    google: '',
    linkedin: '',
    youtube: '',
    instagram: '',
  };

  constructor(private http: HttpClient, private admin: AdminService) {
  }

  ngOnInit(): void {
    this.http
      .get(
        this.admin.getFullUrl('pages/info'),
        {headers: this.admin.getHeader()},
      )
      .subscribe((data) => {
      this.info = data;
      });
  }
}
