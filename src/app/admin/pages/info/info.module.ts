import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComponent } from './info.component';
import {AdminThemeModule} from '../../../@themes/admin/admin-theme.module';
import {NbCardModule} from '@nebular/theme';
import {AdminModule} from '../../admin.module';



@NgModule({
  declarations: [InfoComponent],
  imports: [
    CommonModule,
    AdminThemeModule,
    NbCardModule,
    AdminModule,
  ],
})
export class InfoModule { }
