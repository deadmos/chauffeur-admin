import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatementsComponent } from './statements.component';
import {CompanyStatementsComponent} from './company-statements/company-statements.component';

const routes: Routes = [
    {
        path: '',
        component: StatementsComponent,
    },
    {
        path: 'company',
        component: CompanyStatementsComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class StatementsRoutingModule { }
