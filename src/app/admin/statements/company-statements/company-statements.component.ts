import { Component, OnInit, TemplateRef } from '@angular/core';
import { MENUS } from '../statements-menu';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../../admin.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ActionsComponent } from '../../common/actions/actions.component';
import { NbDialogService, NbThemeService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NotyService } from 'app/admin/services/noty.service';
import { AgDatatable } from '../../admin.extends';

@Component({
  selector: 'ngx-company-statements',
  templateUrl: './company-statements.component.html',
  styleUrls: ['./company-statements.component.scss'],
})

export class CompanyStatementsComponent extends AgDatatable implements OnInit {

  public columnDefs;
  public defaultColDef;
  public rowData: {};
  public rowSelection;
  url = 'company-statements';
  statementFormData: any = {};
  dialogRef: any;
  partners: any;
  id: any;
  name: any;
  menus = MENUS;
  breadcrumbs = [
    {
      title: 'Home',
      link: '/dashboard',
      icon: 'fa fa-home',
    },
    {
      title: 'Company Statement List',
      icon: 'fas fa-file-alt',
    },
  ];
  constructor(
    protected dialogService: NbDialogService,
    protected noty: NotyService,
    protected http: HttpClient,
    protected admin: AdminService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected themeService: NbThemeService,
  ) {
    super();
    this.activatedRoute.params.subscribe((params: Params) => { this.id = params.id; });
    this.columnDefs = [
      { field: 'id', headerName: 'Id', sortable: true, filter: false, maxWidth: 100, minWidth: 60 },
      {
        field: 'partner.user.name',
        headerName: 'Company',
        sortable: true,
        filter: true,
        filterParams: {
          filterOptions: ['contains', 'equals'],
          defaultOption: 'contains',
          suppressAndOrCondition: true,
        },
      },
      { field: 'title', headerName: 'Title', sortable: true, filter: false },
      { field: 'from_date', headerName: 'From Date', sortable: true, filter: false },
      { field: 'to_date', headerName: 'To Date', sortable: true, filter: false },
      {
        field: 'currency',
        headerName: 'Total Credit',
        valueFormatter: params => params.data !== undefined ? this.currencyFormatter(params.data.credit, '$') : '',
        sortable: true,
        filter: false,
      },
      {
        field: 'action', sortable: true, filter: false, minWidth: 100,
        cellRendererFramework: ActionsComponent,
        cellRendererParams: [
          {
            title: 'View PDF',
            href: 'company-statements/',
            type: 'download',
          },
          {
            title: 'Delete',
            href: 'statements/',
            type: 'delete',
            confirm: {
              title: 'Are you sure want to delete the statement?',
              successMessage: 'Statement deleted successfully',
            },
          },
        ],
      },
    ];
    this.defaultColDef = { resizable: true };
    this.rowSelection = 'multiple';
  }

  showLoader() {
    Swal.fire({
      title: 'Please Wait !',
      allowOutsideClick: false,
      showConfirmButton: false,
      willOpen: () => {
        Swal.showLoading();
      },
    });
  }
  convertDate(date: any) {
    return (new DatePipe('en-Us').transform(new Date(date), 'yyyy-MM-dd'));
  }

  companyChange(company_id: any) {
    this.statementFormData.partner_id = company_id;
  }

  openDialog(dialog: TemplateRef<any>) {
    this.dialogRef = this.dialogService.open(dialog, { context: 'this is some additional data passed to dialog' });
  }

  submitStatementForm(data: any) {
    for (const index in data.value) {
      if (index === 'date_range') {
        this.statementFormData.from_date = this.convertDate(data.value[index].start);
        this.statementFormData.to_date = this.convertDate(data.value[index].end);
      } else {
        this.statementFormData[index] = data.value[index];
      }
    }
    this.showLoader();
    this.admin.post(`company-statements`, this.statementFormData).subscribe({
      next: (res) => {
        const response: any = res;
        this.noty.success(response.message);
        Swal.close();
        this.dialogRef.close();
        setTimeout(() => this.reloadCurrentRoute(`statements/company`), 2000);
      },
      error: (error) => {
        Swal.close();
        this.noty.error('Server Error Status:' + error.status + '<br>' + error.message);
      },
    });
  }

  reloadCurrentRoute(currentUrl) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  ngOnInit(): void {
    this.admin.get('networks-list').subscribe((res) => {
      this.partners = res;
    });
  }

}
