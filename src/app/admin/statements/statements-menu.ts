export const MENUS = [
    {
      id: 'driver',
      title: 'Driver Statments',
      icon: 'fa fa-user-secret',
      link: '/statements',
    },
    {
      id: 'COMPANY',
      title: 'Company Statments',
      icon: 'fa fa-building',
      link: '/statements/company',
    },
];
