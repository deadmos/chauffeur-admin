import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatementsRoutingModule } from './statements-routing.module';
import { StatementsComponent } from './statements.component';
import {NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbFormFieldModule,
  NbInputModule,
  NbListModule,
  NbSelectModule,
  NbToggleModule,
  NbDatepickerModule} from '@nebular/theme';
import {AdminThemeModule} from '../../@themes/admin/admin-theme.module';
import {AdminModule} from '../admin.module';
import {AgGridModule} from 'ag-grid-angular';

import {FormsModule} from '@angular/forms';
import {NbMomentDateModule } from '@nebular/moment';
import {CompanyStatementsComponent} from './company-statements/company-statements.component';

@NgModule({
  declarations: [StatementsComponent, CompanyStatementsComponent],
  imports: [
    CommonModule,
    StatementsRoutingModule,
    NbCardModule,
    AdminThemeModule,
    AdminModule,
    AgGridModule,
    NbSelectModule,
    NbToggleModule,
    NbDatepickerModule,
    NbMomentDateModule,
    NbCheckboxModule,
    NbButtonModule,
    NbFormFieldModule,
    NbInputModule,
    NbListModule,
    FormsModule,
  ],
})
export class StatementsModule { }
