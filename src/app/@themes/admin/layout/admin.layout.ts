import { Component } from '@angular/core';

@Component({
  selector: 'ngx-admin-layout',
  styleUrls: ['admin.layout.scss'],
  templateUrl: 'admin.layout.html',
})
export class AdminLayoutComponent {
  constructor()  {

  }
}
