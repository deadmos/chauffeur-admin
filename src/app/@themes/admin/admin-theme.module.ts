import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSidebarModule,
  NbUserModule,
  NbContextMenuModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbThemeModule, NbCheckboxModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NbSecurityModule } from '@nebular/security';

import {
  AdminTinyMCEComponent,
  FooterComponent,
  HeaderComponent,
} from './components';

import { AdminLayoutComponent } from './layout/admin.layout';

import { DARK_THEME } from './styles/theme.dark';
import {RouterModule} from '@angular/router';
import {MenuBoxComponent} from './components/menu-box/menu-box.component';
import { NotificationBoxComponent } from './components/notification-box/notification-box.component';
import { ContentHeaderComponent } from './components/content-header/content-header.component';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import {MyDatePickerModule} from 'mydatepicker';
const NB_MODULES = [
  NbLayoutModule,
  NbMenuModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbContextMenuModule,
  NbSecurityModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbEvaIconsModule,
  MyDatePickerModule,
];
const COMPONENTS = [
  HeaderComponent,
  FooterComponent,
  AdminTinyMCEComponent,
  AdminLayoutComponent,
  NotificationBoxComponent,
];

@NgModule({
  imports: [CommonModule, ...NB_MODULES, RouterModule, NbCheckboxModule],
  exports: [CommonModule, ...COMPONENTS, ContentHeaderComponent, PageHeaderComponent],
  declarations: [...COMPONENTS, MenuBoxComponent,
    ContentHeaderComponent, PageHeaderComponent],
})
export class AdminThemeModule {
  static forRoot(): ModuleWithProviders<AdminThemeModule> {
    return {
      ngModule: AdminThemeModule,
      providers: [
        ...NbThemeModule.forRoot(
          {
            name: 'dark',
          },
          [ DARK_THEME ],
        ).providers,
      ],
    };
  }
}
