import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent implements OnInit {
  @Input() headerData: any = {
    title: '',
    action: false,
  };
  constructor() { }
  ngOnInit(): void {
  }

}
