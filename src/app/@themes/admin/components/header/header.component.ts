import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { UserData } from '../../../../@core/data/users';
import { LayoutService } from '../../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { AdminService } from 'app/admin/admin.service';

@Component({
  selector: 'ngx-admin-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  currentTheme = 'default';
  userPictureOnly: boolean = false;
  user: any;
  show: boolean = false;
  show_notification: boolean = false;
  userMenu = [{ title: 'Log out' }];
  notifications = [];
  private destroy$: Subject<void> = new Subject<void>();

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private userService: UserData,
    private layoutService: LayoutService,
    public _router: Router,
    private breakpointService: NbMediaBreakpointsService,
    private router: Router,
    private admin: AdminService,
) {
  }


  isDark = true;
  // themes = [
  //   {
  //     value: 'dark',
  //     name: 'Dark',
  //   },
  //   {
  //     value: 'dark',
  //     name: 'Dark',
  //   }
  // ];


  toggleMenu(event) {
    event.stopPropagation();
    this.show = !this.show;
    if (this.show_notification) {
      this.show_notification = false;
    }
  }
  toggleNotification(event) {
    event.stopPropagation();
    this.show_notification = !this.show_notification;
    if (this.show) {
      this.show = false;
    }
  }

  changeTheme(checked: boolean) {
    if (checked) {
      this.themeService.changeTheme('dark');
    } else {
      this.themeService.changeTheme('default');
    }
  }


  ngOnInit() {
    let curUser = localStorage.getItem('currentUser');
      this.themeService.changeTheme('default');
    this.admin.get('notifications').subscribe((notifications: any) => {
        this.notifications = notifications;
      });
    this.userService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => {
        this.user = JSON.parse(curUser).data;
      });

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


  navigateHome() {
  }
}
