import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ngx-content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.scss'],
})
export class ContentHeaderComponent implements OnInit {

  constructor() { }
  @Input() menus = [];
  @Input() current = '';
  @Input() title = '';
  @Input() breadcrumbs = [];

  ngOnInit(): void {
  }

}
