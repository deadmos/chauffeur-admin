import {Component, HostListener, Input, EventEmitter, Output} from '@angular/core';


@Component({
  selector: 'ngx-admin-menu-box',
  styleUrls: ['./menu-box.component.scss'],
  templateUrl: './menu-box.component.html',
})
export class MenuBoxComponent {
  @Input() status: boolean = false;
  @Output() statusChange = new EventEmitter<boolean>();

    menus = [
      {
        title: 'Website Manger',
        link: '/pages',
        icon: '/assets/images/menu/website.png',
      },
      {
        title: 'Fleet Manager',
        link: '/fleets',
        icon: '/assets/images/menu/car.png',
      },
      {
        title: 'Network Accounts',
        link: '/networks',
        icon: '/assets/images/menu/website.png',
      },
      {
        title: ' Driver Manager',
        link: '/drivers',

        icon: '/assets/images/menu/driver.png',
      },
      {
        title: 'Bookings',
        link: '/bookings',
        icon: '/assets/images/menu/booking.png',
      },
      {
        title: 'Area / Location',
        link: '/areas',
        icon:  '/assets/images/menu/location.png',
      },

      {
        title: 'Invoices',
        link: '/invoices/driver',
        icon: '/assets/images/menu/invoice.png',
      },
      {
        title: 'Transactions',
        link: '/transactions',
        icon: '/assets/images/menu/transaction.png',
      },
      {
        title: 'Payouts',
        link: '/driver-payouts',
        icon: '/assets/images/menu/transaction.png',
      },

      {
        title: 'Reports',
        link: '/reports',
        icon: '/assets/images/menu/invoice.png',
      },
      {
        title: 'Statements',
        link: '/statements',
        icon: '/assets/images/menu/statement.png',
      },
      {
        title: 'Earnings',
        link: '/earnings',
        icon: '/assets/images/menu/transaction.png',
      },
      {
        title: 'News Letter',
        link: '/news-letters/driver',
        icon: '/assets/images/menu/statement.png',
      },
      {
        title: 'Settings',
        link: '/settings/admin',
        icon: '/assets/images/menu/settings.png',
      },
      {
        title: 'Notifications',
        link: '/notifications',
        icon: '/assets/images/menu/settings.png',
      },
      {
        title: 'SEO Tools',
        link: '/seo',
        icon: '/assets/images/menu/seo.png',
      },
      {
        title: 'Faq Manager',
        link: '/faqs',
        icon: '/assets/images/menu/faq.png',
      },
    ];
  @HostListener('document:click')
  clickout() {

    if (this.status) {
      this.status = false;
      this.statusChange.emit(false);
    }
  }

}
