import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ngx-notification-box',
  templateUrl: './notification-box.component.html',
  styleUrls: ['./notification-box.component.scss'],
})
export class NotificationBoxComponent  {
  @Input() status: boolean = false;
  @Input() notifications = [{
      created_at_formated:'',
      data:{
          notification_icon:'',
          notification_url:'',
      }
  }];
  @Output() statusChange = new EventEmitter<boolean>();
  @HostListener('document:click')
  clickout() {

    if (this.status) {
      this.status = false;
      this.statusChange.emit(this.status);
    }
  }

}
