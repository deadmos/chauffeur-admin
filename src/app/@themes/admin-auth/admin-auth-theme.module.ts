import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import {NbInputModule, NbLayoutModule, NbThemeModule} from '@nebular/theme';
import {AdminAuthLayoutComponent} from '../admin-auth/layout/admin-auth.layout';
import {FooterComponent, HeaderComponent} from '../admin-auth/components';
import {DARK_THEME} from '../admin-auth/styles/theme.dark';

@NgModule({
  declarations: [
    AdminAuthLayoutComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    NbLayoutModule,
    NbInputModule,
  ],
  exports: [
    AdminAuthLayoutComponent,
  ],
})
export class AdminAuthThemeModule {
  static forRoot(): ModuleWithProviders<AdminAuthThemeModule> {
    return {
      ngModule: AdminAuthThemeModule,
      providers: [
        ...NbThemeModule.forRoot(
          {
            name: 'dark',
          },
          [ DARK_THEME ],
        ).providers,
      ],
    };
  }
}
