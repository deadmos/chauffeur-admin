import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {AdminService} from './admin/admin.service';
import {DomSanitizer} from '@angular/platform-browser';


@Pipe({
  name: 'secure',
})
export class SecurePipe implements PipeTransform {

  constructor(private http: HttpClient, private admin: AdminService,  private sanitizer: DomSanitizer) { }

  transform(url: string) {

    return new Observable<string>((observer) => {
      observer.next('data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==');
      const {next, error} = observer;
      const headers = this.admin.getHeader();
      this.http.get(url, {
        headers: headers,
        responseType: 'blob',
      }).subscribe(response => {
        const reader: any = new FileReader();
        reader.readAsDataURL(response);
        reader.onloadend = function () {
          observer.next(reader.result);
        };
      });

      return {unsubscribe() {  }};
    });
  }

}
