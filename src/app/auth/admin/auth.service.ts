import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {Router} from '@angular/router';
@Injectable()
export class AuthService  {
  env = environment;
  baseUrl  = this.env.apiUrl;
  loginUrl = 'admin/login';
  constructor(private http: HttpClient, private router: Router) {}
  loginAttempt(request, obj) {
   this.http.post(this.baseUrl + this.loginUrl, request).subscribe({
     next: data => {

       localStorage.setItem('currentUser', JSON.stringify(data));
       localStorage.setItem('currentUserToken', data['data']['token']);
       obj.authFailed = false;
       obj.submitting = false;
       this.router.navigateByUrl('dashboard');
     },
     error: error => {
       obj.authFailed = true;
       obj.submitting = false;
       obj.authFailedError = 'These credentials do not match our records.';
     },
   });
  }
  isLoggedIn(){
    let curUser = localStorage.getItem('currentUser');

    const curToken = localStorage.getItem('currentUserToken');

    if (curUser) {
      curUser = JSON.parse(curUser).data;
      if (curUser !== undefined && curToken !== undefined && curUser['token'] !== undefined && curUser['role'] === 'admin') {
        return true;
      }
    }
    return false;
  }

}
