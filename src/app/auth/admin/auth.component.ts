import {  Router  } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {AuthService} from './auth.service';

@Component({
  selector: 'ngx-auth',
  templateUrl: './auth.component.html',
})
export class AuthComponent implements OnInit {

  constructor(private router: Router, private auth: AuthService) {

      if(this.auth.isLoggedIn() || this.router.url == '/'){
        this.router.navigateByUrl('dashboard');
      }
  }

  ngOnInit(): void {

  }

}
