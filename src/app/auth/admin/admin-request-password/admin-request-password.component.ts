import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'ngx-admin-request-password',
  templateUrl: './admin-request-password.component.html',
  styleUrls: ['./admin-request-password.component.scss']
})
export class AdminRequestPasswordComponent implements OnInit {
    env = environment;
    baseUrl  = this.env.apiUrl;
    submitting = false;
    failed = false;
    email ='';
    reset_code ='';
    password ='';
    error ='';
    reset_error ='';
    password_error ='';
   

    firstForm: FormGroup;
    secondForm: FormGroup;
    thirdForm: FormGroup;
  
    constructor(private fb: FormBuilder, private http:HttpClient, private router:Router) {
    }
  
    ngOnInit() {

    }
  
    onFirstSubmit(stepper) {
        this.submitting = true;

        this.http.post(this.baseUrl + 'admin/request-password', {email:this.email}).subscribe({
            next: data => {
                console.log('inside');
                this.submitting = false;
                stepper.next();
            },
            error: error => {
                this.submitting = false;
                this.failed = true;
                this.error = 'These credentials do not match our records.';
            },
          });
 
    }
  
    onSecondSubmit(stepper) {
        this.submitting = true;
    
        this.http.post(this.baseUrl + 'admin/check-reset-code', {reset_code:this.reset_code,email:this.email}).subscribe({
            next: data => {
                console.log(data['status']);
                if(data['status']){
                    this.submitting = false;
                    stepper.next();
                }else{
                    this.submitting = false;
                    this.reset_error = 'Code Is Invalid';
           
                }
            },
            error: error => {
                this.submitting = false;
                this.failed = true;
                this.reset_error = 'Error While Submiting';
            },
          });
    }
  
    onThirdSubmit(stepper) {
        this.submitting = true;
    
        this.http.post(this.baseUrl + 'admin/reset-password', {reset_code:this.reset_code,email:this.email,password:this.password}).subscribe({
            next: data => {
                if(data['status']){
                    this.router.navigate(['login']);
                }else{
                    this.submitting = false;
                    this.password_error = 'Password Is Invalid';
           
                }
            },
            error: error => {
                this.submitting = false;
                this.failed = true;
                this.password_error = 'Server Error';
            },
          });
    }
    onSubmit() {

  }

}
