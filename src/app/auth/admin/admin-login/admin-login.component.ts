import { Component } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
@Component({
  selector: 'ngx-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss'],
})
export class AdminLoginComponent {
  model = {
    email: '',
    password: '',
  };
  authFailedError = '';
  authFailed = false;
  submitting = false;

  constructor(private auth: AuthService, private route: Router) {

  }

  onSubmit() {
    this.submitting = true;
    this.auth.loginAttempt(this.model, this);
    // console.log(status);
    // if (status == true) {
    //   this.route.navigateByUrl('admin/dashboard');
    // } else {
    //   this.authFailed = true;
    //   this.authFailedError = 'These credentials do not match our records.';
    // }
  }

}
