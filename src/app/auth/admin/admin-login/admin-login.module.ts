import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminAuthThemeModule} from '../../../@themes/admin-auth/admin-auth-theme.module';
import {AdminLoginComponent} from './admin-login.component';
import {NbCardModule, NbFormFieldModule, NbIconModule, NbInputModule} from '@nebular/theme';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '../auth.service';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [AdminLoginComponent],
  imports: [
    AdminAuthThemeModule,
    CommonModule,
    NbInputModule,
    NbFormFieldModule,
    NbCardModule,
    NbIconModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],

})
export class AdminLoginModule {
}
