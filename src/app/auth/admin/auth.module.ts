import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {AuthComponent} from './auth.component';
import { AuthRoutingModule } from './auth-routing.module';
import {AdminAuthThemeModule} from '../../@themes/admin-auth/admin-auth-theme.module';
import {NbButtonModule, NbCardModule, NbInputModule, NbStepperModule} from '@nebular/theme';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './auth.service';
import { RouterModule } from '@angular/router';
import { AdminRequestPasswordComponent } from './admin-request-password/admin-request-password.component';


@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    AdminAuthThemeModule,
    NbInputModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule
  ],
  declarations: [
    AuthComponent,
    AdminRequestPasswordComponent
  ],
  providers: [AuthService],
})
export class AuthModule {

}
