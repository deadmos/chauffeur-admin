import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NbLayoutModule} from '@nebular/theme';
import {AdminLoginComponent} from './admin-login/admin-login.component';
import {AdminLogoutComponent} from './admin-logout/admin-logout.component';
import {AdminRequestPasswordComponent} from './admin-request-password/admin-request-password.component';
import {AdminResetPasswordComponent} from './admin-reset-password/admin-reset-password.component';
import {AuthComponent} from './auth.component';

const routes: Routes = [
  {
  path: '',
  component: AuthComponent,
  children: [
  {
    path: 'login',
    component: AdminLoginComponent,
  },
  {
    path: 'logout',
    component: AdminLogoutComponent,
  },
  {
    path: 'request-password',
    component: AdminRequestPasswordComponent,
  },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes), NbLayoutModule],
  exports: [RouterModule],
})
export class AuthRoutingModule { }

