import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor( public router: Router) {}
  canActivate(): boolean {
    let curUser = localStorage.getItem('currentUser');

    const curToken = localStorage.getItem('currentUserToken');

    if (curUser) {
      curUser = JSON.parse(curUser).data;
      if (curUser !== undefined && curToken !== undefined && curUser['token'] !== undefined && curUser['role'] === 'admin') {
        return true;
      } else {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentUserToken');
      }
    }
    this.router.navigateByUrl('login');
    return false;


  }
}
