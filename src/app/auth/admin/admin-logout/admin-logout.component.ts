import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-admin-logout',
  templateUrl: './admin-logout.component.html',
  styleUrls: ['./admin-logout.component.scss'],
})
export class AdminLogoutComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
