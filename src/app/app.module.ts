/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { AdminThemeModule } from './@themes/admin/admin-theme.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AgGridModule } from 'ag-grid-angular';

import {
  NbCardModule,
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
  NbStepperModule,
} from '@nebular/theme';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthGuardService} from './auth/admin/auth-guard.service';
import {AdminService} from './admin/admin.service';
import { ToggleComponent } from './admin/common/toggle/toggle.component';
import {ActionsComponent} from './admin/common/actions/actions.component';
import {TreeModule} from '@circlon/angular-tree-component';
import {DndModule} from 'ng2-dnd';
import {MapService} from './admin/areas/map.service';
import { WebsiteManagerComponent } from './svg/website-manager/website-manager.component';

@NgModule({
  declarations: [
    AppComponent,
    WebsiteManagerComponent,
  ],
  providers: [
    AuthGuardService,
    AdminService,
    MapService,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    TreeModule,
    NbCardModule,
    NbStepperModule,
    ReactiveFormsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    DndModule.forRoot(),
    AgGridModule.withComponents([
      ToggleComponent,
      ActionsComponent,
    ]),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    CoreModule.forRoot(),
    AdminThemeModule.forRoot(),
  ],
  bootstrap: [AppComponent],
})

export class AppModule {
}
