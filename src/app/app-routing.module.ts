import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./auth/admin/auth.module')
      .then(m => m.AuthModule),
  },
  {
    path: '',
    loadChildren: () => import('./admin/admin.module')
      .then(m => m.AdminModule),
  },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})


export class AppRoutingModule {
}
